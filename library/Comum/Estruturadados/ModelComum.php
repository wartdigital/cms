<?php 

/**
 * Classe que representa um model comum
 * 
 * @author Jo�o Pedro Grasselli
 * @version 17/08/2012
 */
class Comum_Estruturadados_ModelComum{
	
	private $fgativo;
	
	/**
	 * M�todo construtor
	 * 
	 * @author Jo�o Pedro Grasselli
	 * @version 17/08/2012
	 */
	public function __construct(){}
	
	/**
	 * Fun��o set gen�rica para qualquer atributo
	 * 
	 * @author Jo�o Pedro Grasselli
	 * @param string $pName Nome do atributo a ser inserido
	 * @param mixed $pValue Valor a ser inserido para o atributo
	 * @return void
	 */
	public function __set($pName, $pValue){
		$method = 'set' . $pName;
        if (!method_exists($this, $method)) {
            throw new Exception('Invalid property');
        }
        $this->$method($value);	
	}
	
	/**
	 * Fun��o get gen�rica para qualquer atributo
	 * 
	 * @author Jo�o Pedro Grasselli
	 * @return mixed
	 */
	public function __get($pName)
    {
        $method = 'get' . $pName;
        if (!method_exists($this, $method)) {
            throw new Exception('Invalid property');
        }
        return $this->$method();
    }

    /**
	 * Fun��o set para o fgativo
	 * 
	 * @author Jo�o Pedro Grasselli
	 * @param mixed $pFgativo
	 */
    public function setFgativo($pFgativo){
    	$this -> fgativo = $pFgativo;
    }
    
    /**
	 * Fun��o get para o fgativo
	 * 
	 * @author Jo�o Pedro Grasselli
	 * @return mixed
	 */
    public function getFgativo(){
    	return $this -> fgativo;
    }
    
	/**
	 * Fun��o que converte o objeto para um array
	 * 
	 * @author Jo�o Pedro Grasselli
	 * @version 14/01/2012
	 * @throws Zend_Exception
	 */
	public function toArray(){
		try{
			$lArr = get_class_vars(get_class($this));
			
			foreach($lArr as $lAtribute => $lNULL){
				if($lAtribute != NULL){
					if(is_object ($this -> $lAtribute)){
						if(method_exists($this -> $lAtribute, 'toArray')){
							$lArrRetorno[$lAtribute] = $this -> $lAtribute -> toArray();
						}else{
							if($this -> $lAtribute instanceof W3_Estruturadados_ListaComum){
								for($li = 0; $li < $this -> $lAtribute -> getTam(); $li++){
									$lRow = $this -> $lAtribute -> get($li);
									if(method_exists($lRow, 'toArray')){	
										$lArrRetorno[$lAtribute][$li] = $lRow -> toArray();
									}
								}
							}
						}
					}else{
						$lArrRetorno[$lAtribute] = $this -> $lAtribute;
					}
				}
			}
			return $lArrRetorno;
		}catch(Zend_Exception $ze){
			throw $ze;
		}
	}
	
	/**
	 * Fun��o que converte o objeto para uma stdClass
	 * 
	 * @author Jo�o Pedro Grasselli
	 * @version 14/01/2012
	 * @throws Zend_Exception
	 */
	public function toStdClass(){
		try{
			$lArr = self::toArray();
			return self::arrayToStdClass($lArr);
		}catch(Zend_Exception $ze){
			throw $ze;
		}
	}
	
	/**
	 * Fun��o que converte um array para uma stdClass
	 * 
	 * @author Jo�o Pedro Grasselli
	 * @version 14/01/2012
	 * @throws Zend_Exception
	 */
	private function arrayToStdClass($pArray){
		try{
			$lReturnObject = new stdClass;
			
			foreach($pArray as $lIndex => $lValue){
				if(is_array($lValue)){
					$lValue = self::arrayToStdClass($lValue);
				}
				$lReturnObject -> $lIndex = $lValue;
			}
			
			return $lReturnObject;
		}catch(Zend_Exception $ze){
			throw $ze;
		}
	}
}

?>