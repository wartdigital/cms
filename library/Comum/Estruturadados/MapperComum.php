<?php

/**
 * Classe que representa um Mapper comum
 *
 * @author Jo�o Pedro Grasselli
 * @version 17/08/2012
 */
class Comum_Estruturadados_MapperComum extends Comum_Estruturadados_ObjetoComum{

    protected $dbTable;
    
	public function __construct($pDbTable){
		$this-> dbTable = $pDbTable;
	}

	public function getDbTable(){
		return $this->dbTable;
	}

	public function lastID(){
		return $this->dbTable->lastID();
	}
	
	public function data($pData){
		return $this->dbTable->data($pData);
	}

	public function getPaginatorInstance(){
		return $this->dbTable->getPaginatorInstance();
	}

}

