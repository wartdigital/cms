<?php

/**
 * Classe que representa um objeto do tipo lista comum.
 *
 * @author Jo�o Pedro Grasselli
 * @access public
 */
class Comum_Estruturadados_ListaComum extends Comum_Estruturadados_ObjetoComum {

	/**
	 * @author Jo�o Pedro Grasselli
	 * @var Array Representa a lista de itens do vetor.
	 */
	protected $lLista;		 	
	
	/**
	 * @author Jo�o Pedro Grasselli	
	 * @access public
	 * @param void
	 */	
	public function ListaComum($pArrayConversao = NULL) {
		if ($pArrayConversao != NULL) {
			$this->lLista = $pArrayConversao;
		}
	}

	/**
	 * Captura o n�mero de itens que a lista possui.
	 *
	 * @author Jo�o Pedro Grasselli
	 * @access public
	 * @param void
	 * @return Integer N�mero de itens da lista.
	 */	
	public function getTam() {
		if ($this->lLista != NULL) {
			return count($this->lLista);					
		} else {
			return 0;
		}
	}
		
	/**
	 * Captura um item da lista.
	 *
	 * @author Jo�o Pedro Grasselli
	 * @access public
	 * @param void
	 * @return Object Item da lista.
	 */		
	public function get($pPosicao) {
		return $this->lLista[$pPosicao];
	}
		
	/**
	 * Adiciona itens a lista.
	 *
	 * @author Jo�o Pedro Grasselli
	 * @access public
	 * @param Object Item para a lista.
	 * @return void
	 */		
	public function add($pItem) {
		$this->lLista[$this->getTam()] = $pItem;
	}
		
		
	/**
	 * Retorna o vetor da lista.
	 *
	 * @author Jo�o Pedro Grasselli
	 * @access public
	 * @param Object Item para a lista.
	 * @return void
	 */	
	public function lista2Vetor() {
		return $this->lLista;
	}

		
	/**
	 * M�todo respons�vel pelo retorno de um array, para a montagem das options de um elemento select
	 * 
	 * @author Jo�o Pedro Grasselli
	 * @param $pValorDefault - Valor que ir� na primeira op��o do combo de sele��o, sendo o seu valor Null
	 * @param $pFuncaoGetCodigo - nome da fun��o que deve retornar o c�digo para o value, sendo que no par�metro deve ser passado os sinais "()" ao final do nome da fun��o  
	 * @param $pFuncaoGetDescricao - nome da fun��o que deve retornar o nome que ir� aparecer no combo, sendo que no par�metro deve ser passado os sinais "()" ao final do nome da fun��o
	 * @param $isMultiple - par�metro booleano que solicita o retorno de um array para montagem de um select m�ltiplo
	 * @return array com os options montados, sendo a posi��o default com c�digo NULL ("selecione...")
	 * @version 29/08/2012
	 * @version 24/09/2012 - acrescentado par�metro $isMultiple - Wiliam Passaglia Castilhos
	 */
	public function getOptionsArray($pValorDefault,$pFuncaoGetCodigo,$pFuncaoGetDescricao,$isMultiple=FALSE) {
		try{
			//caso seja select multiplo, n�o deve vir o primeiro campo preenchido com o identificador
			if(!$isMultiple){
				if(isset($pValorDefault)){
					$options = array(''=>$pValorDefault);
				}else{
					$options = array(''=>"Selecione");
				}
			}
			if(isset($pFuncaoGetDescricao) && isset($pFuncaoGetCodigo)){
				for($i = 0; $i < $this->getTam(); $i++){
					eval('$options[$this->lLista[$i]->'.$pFuncaoGetCodigo.'] = $this->lLista[$i]->'.$pFuncaoGetDescricao.';');
				}
			}else{
				throw new Zend_Exception("Par�metros incorretos na fun��o getOptionsArray ",0,NULL);
			}
			return $options;
		}catch(Zend_Exception $e){
			die($e);
		}
	}
	
} ?>
