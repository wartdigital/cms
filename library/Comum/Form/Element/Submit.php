<?php
require_once 'Zend/Form/Element/Xhtml.php';
/**
 * @author Gabriel Stringari
 * @version 12/05/2017
 * @Time: 14:31
 */
class Comum_Form_Element_Submit extends Zend_Form_Element_Button
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'formButton';

    public function Comum_Form_Element_Button($pName){
        parent::__construct($pName);

        $this -> setDecorators(array(
            array('ViewHelper'),
            array('Errors'),
            array('HtmlTag', array('tag' => 'span'))
        ))
            -> setAttrib('class', 'botao-padrao');
            //-> setAttrib('type','submit');

    }
}