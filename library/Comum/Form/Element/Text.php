<?php

require_once 'Zend/Form/Element/Xhtml.php';

/**
 * Elemento de formulário para campos texto
 * 
 * @author João Pedro Grasselli
 * @version 02/05/2014
 */
class Comum_Form_Element_Text extends Zend_Form_Element_Text
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'formText';
    
    public function Comum_Form_Element_Text($pName){
    	parent::__construct($pName);

    	$this -> setDecorators(array(
							   		array('ViewHelper'),
							   		array('Errors'),
							   		array('Label')
					   				 )
					   		   )
			  -> setAttrib ('class', 'campo-padrao');
    }
    
    public function setDescription($pDescricao){
    	$this -> setAttrib('alt', $pDescricao)
			  -> setAttrib('title', $pDescricao)
              -> setAttrib('placeholder', $pDescricao);
			  
		return $this;
    }
    
}