<?php

require_once 'Zend/Form/Element/Xhtml.php';

/**
 * Elemento de formulário para campos texto
 * 
 * @author João Pedro Grasselli
 * @version 02/05/2014
 */
class Comum_Form_Element_Multiselect extends Zend_Form_Element_Select
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'formSelect';
    
    public function Comum_Form_Element_Multiselect($pName){
    	parent::__construct($pName);

    	$this -> setDecorators(array(
							   		array('ViewHelper'),
							   		array('Errors'),
							   		array('Label')
							   		)
					   		  )
              -> setAttrib('multiple', 'multiple')
			  -> setAttrib('class', 'campo-padrao');
    }
    
    public function setMultiOptions($pArrayOptions){
           if($pArrayOptions != NULL){
              parent::setMultiOptions($pArrayOptions);
           }
           return $this;
    }
    
	public function setDescription($pDescricao){
    	$this -> setAttrib('title', $pDescricao);
    	
    	return $this;
    }
}
