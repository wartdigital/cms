<?php

require_once 'Zend/Form/Element/Xhtml.php';

/**
 * Elemento de formulário para botões
 * 
 * @author João Pedro Grasselli
 * @version 02/05/2014
 */
class Comum_Form_Element_Button extends Zend_Form_Element_Button
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'formButton';
    
    public function Comum_Form_Element_Button($pName){
    	parent::__construct($pName);

    	$this -> setDecorators(array(
								   	array('ViewHelper'),
								   	array('Errors'),
								   	array('HtmlTag', array('tag' => 'span'))
								   	))
			  -> setAttrib('class', 'botao-padrao');
    }
}