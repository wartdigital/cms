<?php

require_once 'Zend/Form/Element/Xhtml.php';

/**
 * Elemento de formulário para campos texto
 * 
 * @author João Pedro Grasselli
 * @version 02/05/2014
 */
class Comum_Form_Element_Telefone extends Comum_Form_Element_Text
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'formText';
    
    public function Comum_Form_Element_Date($pName){
    	parent::__construct($pName);

    	$this -> setAttrib('data', 'data')
    		  -> setAttrib('mask', 'fone')
    		  -> setAttrib('maxlength', '14');
    }
}
