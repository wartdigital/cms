<?php

require_once 'Zend/Form/Element/Xhtml.php';

/**
 * Elemento de formulário para campos texto
 * 
 * @author João Pedro Grasselli
 * @version 02/05/2014
 */
class Comum_Form_Element_Hidden extends Zend_Form_Element_Hidden
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'formHidden';
    
    public function Comum_Form_Element_Hidden($pName){
    	parent::__construct($pName);

    	$this -> setDecorators(array(
							   		array('ViewHelper'),
							   		array('Errors'),
							   		array('Label')
					   				 )
					   		   );
    }
    
}