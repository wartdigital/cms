<?php

require_once 'Zend/Form/Element/Xhtml.php';

/**
 * Elemento de formulário para campos texto
 *
 * @author João Pedro Grasselli
 * @version 02/05/2014
 */
class Comum_Form_Element_Ano extends Comum_Form_Element_Text
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'formText';

    public function Comum_Form_Element_Ano($pName){
    	parent::__construct($pName);

    	$this -> setDecorators(array(
							   		array('ViewHelper'),
							   		array('Errors'),
							   		array('Label')
					   				 )
					   		   )
			  -> setAttrib ('class', 'campo-padrao')
              -> setAttrib ('mask', 'int')
              -> setAttrib ('maxlength', '4');
    }

    public function setDescription($pDescricao){
    	$this -> setAttrib('alt', $pDescricao)
			  -> setAttrib('title', $pDescricao);

		return $this;
    }

}
