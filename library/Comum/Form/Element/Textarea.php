<?php

require_once 'Zend/Form/Element/Xhtml.php';

/**
 * Elemento de formulário para campos texto
 * 
 * @author João Pedro Grasselli
 * @version 02/05/2014
 */
class Comum_Form_Element_Textarea extends Zend_Form_Element_Textarea
{
    /**
     * Default form view helper to use for rendering
     * @var string
     */
    public $helper = 'formTextarea';
    
    public function Comum_Form_Element_Textarea($pName){
    	parent::__construct($pName);

    	$this -> setDecorators(array(
					   		array('ViewHelper'),
					   		array('Errors'),
					   		array('Label'),
					   		))
			  -> setAttrib('class', 'campo-padrao');
    }
    
    public function setDescription($pDescricao){
    	$this -> setAttrib('alt', $pDescricao)
			  -> setAttrib('title', $pDescricao);
			  
		return $this;
    }
    
}