<?php

class Comum_Cookie{

	protected$lCookie;
	
	public function Comum_Cookie(){}
	
	public function setCookie($pName, $pValue, $pExpire = NULL){
		//Padrão de 30 dias
		$lExpire = ($pExpire != NULL)?$pExpire:time()+60*60*24*30;
		setCookie($pName, $pValue, $lExpire);	
	}
	
	public function getCookie($pName){
		return $_COOKIE[$pName];	
	}
	
}