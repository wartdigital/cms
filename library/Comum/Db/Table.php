<?php
class Comum_Db_table extends Zend_Db_Table{
	
public function data($data,$sep='/'){
	if($data == '0000-00-00'){
		return '';
	}elseif($data["4"]=="-"){
		return substr($data,8,2).$sep.substr($data,5,2).$sep.substr($data,0,4);
	}elseif($data["2"]=="/"){
		return substr($data,6,4).$sep.substr($data,3,2).$sep.substr($data,0,2);
	}else{
		return false;
	}
}

	/*
	 * Configurações de paginação
	 */

	public function setPaginationConfig(Comum_Paginator_Config $pConfig){
		$this -> paginationConfig = $pConfig;
	}

	public function getPaginationConfig(){
		return $this -> paginationConfig;
	}

	public function setPaginatorInstance(Comum_Paginator $pPaginator){
		$this -> paginator = $pPaginator;
	}

	public function getPaginatorInstance(){
		return $this -> paginator;
	}

	/*
	 * Fim das configurações de paginação
	 */

    /**
	 * Metodo que retorna a última chave primaria inserida
	 * @param
	 * @return string
	 * @throws Zend_Db_Exception
	 */
	public function lastID(){
           return $this->getAdapter()->lastInsertId();
	}

}
?>
