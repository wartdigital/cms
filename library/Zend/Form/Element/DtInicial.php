<?php 
class Zend_Form_Element_DtInicial extends Zend_Form_Element_Text{
	
	public function __contruct(){
		parent::__construct;
	}
	
	public function _init(){
	$this->addValidator(new Zend_Validate_StringLength(10,10))
			     ->setRequired(false)
	        	 ->setAttrib('OnKeyPress', 'MascaraData(form.dtfinal);')
	        	 ->setAttrib('OnBlur', 'ValidaData(form.dtfinal);')
	        	 ->setAttrib('maxlength', '10')
	        	 ->setAttrib('style', 'width:15%;')
	        	 ->setDecorators(array(
						   		 array('ViewHelper'),
						   		 array('Errors'),
						   		 array('Label'),
						   		 )) 
		    	 ->class ='campomenu';
		    	 
	return $this;
	}
}