<?php 
/** Zend_Form_Element_Xhtml */
require_once 'Zend/Form/Element/Multi.php';

/**
 * Text form element
 *
 * @category   Zend
 * @package    Zend_Form
 * @subpackage Element
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: Text.php 23775 2011-03-01 17:25:24Z ralph $
 */

class Zend_Form_Element_FgAtivo extends Zend_Form_Element_Multi
{
	
	public $helper = 'formSelect';
	
	public function __construct(){
		parent::__construct('fgativo');
		
		$this->setdecorators(array(
						   	 array('ViewHelper'),
						   	 array('Errors'),
						   	 array('Label'),
						   	));
		
		$this->setAttrib('class', 'campo-padrao');
						   	
		$this->addMultiOptions(array(
	 	     '1'=>'Ativo',
	         '0'=>'Inativo'
	         ));
	}
}