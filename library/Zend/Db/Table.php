<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Db
 * @subpackage Table
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: Table.php 23775 2011-03-01 17:25:24Z ralph $
 */

/**
 * @see Zend_Db_Table_Abstract
 */
require_once 'Zend/Db/Table/Abstract.php';

/**
 * @see Zend_Db_Table_Definition
 */
require_once 'Zend/Db/Table/Definition.php';

/**
 * Class for SQL table interface.
 *
 * @category   Zend
 * @package    Zend_Db
 * @subpackage Table
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Zend_Db_Table extends Zend_Db_Table_Abstract
{

    /**
     * __construct() - For concrete implementation of Zend_Db_Table
     *
     * @param string|array $config string can reference a Zend_Registry key for a db adapter
     *                             OR it can reference the name of a table
     * @param array|Zend_Db_Table_Definition $definition
     */
    public function __construct($config = array(), $definition = null)
    {
        if ($definition !== null && is_array($definition)) {
            $definition = new Zend_Db_Table_Definition($definition);
        }

        if (is_string($config)) {
            if (Zend_Registry::isRegistered($config)) {
                trigger_error(__CLASS__ . '::' . __METHOD__ . '(\'registryName\') is not valid usage of Zend_Db_Table, '
                    . 'try extending Zend_Db_Table_Abstract in your extending classes.',
                    E_USER_NOTICE
                    );
                $config = array(self::ADAPTER => $config);
            } else {
                // process this as table with or without a definition
                if ($definition instanceof Zend_Db_Table_Definition
                    && $definition->hasTableConfig($config)) {
                    // this will have DEFINITION_CONFIG_NAME & DEFINITION
                    $config = $definition->getTableConfig($config);
                } else {
                    $config = array(self::NAME => $config);
                }
            }
        }

        parent::__construct($config);
    }
    
    /**
	 * Metodo que retorna a �ltima chave primaria inserida 
	 * @param 
	 * @return string
	 */
	public function lastID(){			
		return $this->getAdapter()->lastInsertId();
	}
	
	/**
	 * Helper para colocar a quantidade indicada por par�metro de zeros � esquerda
	 * @author Wiliam Passaglia Castilhos
	 * @param $valor int o valor a ser transformado
	 * @param $quantZeros a quantidade de zeros que ser� colocada � esquerda
	 * @version 09/03/2012
	 */
	public function Bz($valor, $quantZeros){
	   while (strlen($valor)<$quantZeros) 
	  	$valor="0".$valor;
	  return $valor;
	}
	
	/**
	 * Metodo que coloca apenas os dados referente � tabela do banco
	 * @param array $dados os dados a serem verificados
	 * @return array
	 */
	protected function colunas(Array $dados){
		$ret = array();
		foreach($dados as $coluna=>$valor){
			if(in_array($coluna,$this->_getCols())){
				$ret[$coluna] = $valor;
			}
		}
		return $ret;
	}	
	
	/**
	 * Metodo que transforma a data para formato de banco
	 * @param string $data a data a ser modificada
	 * @return array
	 */	
	 public function data($data, $sep="/") {
	 	//var_dump($data,$data["4"]);
		  if($data["4"]=="-"){
		  	//var_dump("<hr>",$data,substr($data,8,2).$sep.substr($data,5,2).$sep.substr($data,0,4));
		  	return substr($data,8,2).$sep.substr($data,5,2).$sep.substr($data,0,4);
		  } elseif($data["2"] == "/") {
		    return substr($data,6,4).$sep.substr($data,3,2).$sep.substr($data,0,2);
		  }
		  else return false;
	}

	
	/**
	 * M�todo que abre uma transa��o
	 */
	public function beginTransaction(){
		if(Zend_Registry::get('transactionStarted') == 0){
			Zend_Registry::set('transactionStarted', '1');
			$lAdapter = $this -> getAdapter();
			$lAdapter -> beginTransaction();
		}
	} 
	
	/**
	 * M�todo que fecha uma transa��o e da commit
	 */
	public function commit(){
		if(Zend_Registry::get('transactionStarted') == 1){
			Zend_Registry::set('transactionStarted', '0');
			$lAdapter = $this -> getAdapter();
			$lAdapter -> commit();
		}else{
			throw new Zend_Db_Exception('Transa��o n�o iniciada!');
		}
	} 
	
	/**
	 * M�todo que fecha da rollback na transa��o aberta
	 */
	public function rollBack(){
		if(Zend_Registry::get('transactionStarted') == 1){
			Zend_Registry::set('transactionStarted', '0');
			$lAdapter = $this -> getAdapter();
			$lAdapter -> rollBack();
		}else{
			throw new Zend_Db_Exception('Transa��o n�o iniciada!');
		}
	} 
}
