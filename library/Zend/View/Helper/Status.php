<?php class Zend_View_Helper_Status{

	public $strstatus;

	/*Modificado em 10/05/2012 por Wiliam Passaglia Castilhos, acrescentados os status Reprovado e Aprovado*/
	public function status($cdstatus){
		
		$strstatus = NULL;
		//var_dump($cdstatus);
		switch($cdstatus){
			case 3: $strstatus = "Reprovado";break;
			
			case 2: $strstatus = "Aprovado";break;
			
			case 1: $strstatus = "Matriculado";break;
			
			case 4 : $strstatus = (($cdstatus!=NULL)?"Cancelado":"");break;
						 
		}
 		return $strstatus;
	}
} 
