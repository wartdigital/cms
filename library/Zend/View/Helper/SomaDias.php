<?php class Zend_View_Helper_SomaDias{
	
	public function SomaDias($data, $dias){
		$thisyear = substr ( $data, 6, 4 );
		$thismonth = substr ( $data, 3, 2 );
		$thisday =  substr ( $data, 0, 2 );
		
		$nextdate = mktime ( 0, 0, 0, $thismonth, $thisday + $dias, $thisyear );
		
		return $nextdate;
	}
	
}