<?php 
class Zend_View_Helper_SaldoHoras extends Zend_View_Helper_Abstract {
	/**
	 * Helper para gera��o da tabela com o saldo de horas de uma pessoa
	 * @param string $cdpessoa c�digo da pessoa
	 * @param boolean $home se � a p�gina inicial
	 * @param boolean $relatorio para�metro que indica se o helper est� sendo chamado de um relat�rio
	 * @author Jo�o Pedro Grasselli
	 * @version 04/04/2012
	 * @return string
	 * */
	public function SaldoHoras($cdpessoa,$home=NULL,$relatorio=NULL) {
		try{
			$banco_horas = new Horas_Model_SaldoHoras();
			$saldo = $banco_horas -> BuscaHorasPorAluno($cdpessoa);
			if($relatorio){
				return $saldo;
			}
			if($home){
				$lHTML ='
				<p class="box_titulo titulo_home">saldo de horas</p>
				<div class="box">
				<table class="tabelapadrao">';
			}else{
				$lHTML ='
				<div class="box">
				<p class="box_titulo">saldo de horas</p>
				<table class="tabelapadrao">';
			}
			
			foreach($saldo as $grupo){	
				$lHTML .='
				<tr>
				<td align="left" width="80%">'.$grupo[nmgrupo].'</td>
				<td>'.(($grupo[nrsaldo]==NULL)?"0":$grupo[nrsaldo]).'</td>
				</tr>';
			}
			
			$lHTML .='
			</table>
			</div>';
			
			return $lHTML;
			
		}catch(Zend_Exception $e){
			die($e);
		}
	}
}