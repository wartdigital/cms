<?php 
 /** 
 * Class inserts neccery code for initialize file manager ElFinder 
 * 
 * @author Dimitry Dushkin 
 * 
 */ 
 class Zend_View_Helper_EnableElFinder extends Zend_View_Helper_Abstract{ 
 
	 public function enableElFinder() { 
		 $elfinder_base_uri = "/js"; 
		 
		 $this->view->headLink()->appendStylesheet("/css/elfinder.css"); 
		 $this->view->headScript()->appendFile("/js/elfinder.min.js"); 
		 
		 $this->view->headScript()->captureStart() ?> 
			 var opts = { 
			 lang : 'ru', 
			 styleWithCss : false, 
			 width : 800, 
			 height : 200, 
			 toolbar : 'normal', 
			 fmAllow : true, 
			 fmOpen : function(callback) { 
			 $('<div id="myelfinder">').elfinder({ 
				 url : '/elfinder/connectors/php/connector.php',  
			 }) 
		 } 
		 }; 
		 <?php $this->view->headScript()->captureEnd(); 
	 } 
 } 