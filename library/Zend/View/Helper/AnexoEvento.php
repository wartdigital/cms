<?php 
class Zend_View_Helper_AnexoEvento extends Zend_View_Helper_Abstract {
	/**
	 * Fun��o retorna informacoes referentes as arquivos guardado na tabela de anexos de eventos, conforme o c�digo do procolo passado por par�metro
	 * @param int $cdprotocolo c�digo do protocolo em que o(s) anexos est�o associados 
	 * @author Wiliam Passaglia Castilhos
	 * @version 07/04/2012
	 * @return string html com a tabela montada com todos os anexos correspondentes a esse protocolo
	 * */
	public function AnexoEvento($cdprotocolo) {
		try{
			$banco_anexos_eventos = new Protocolo_Model_Eventos();
			$anexos = $banco_anexos_eventos-> BuscarAnexo($cdprotocolo);
			
			$lhtml = '<table class="tabelapadrao" cellpadding="0" cellspacing="0" border="0">';
			foreach($anexos as $anexo){
				$lhtml .= '<tr>';
					$lhtml .= '<td>';
						$lhtml .= '<a href="/default/index/download/nmarquivo/'.$anexo[nmarquivo].'/extensao/'.$anexo[nmextensao].'/nmfisico/'.$anexo[nmfisico].'/nmpasta/'.$anexo[nmpasta].'" class="linkpreto"> '.$anexo[nmtipoanexo].' </a>';
					$lhtml .= '</td>';
				$lhtml .= '</tr>';
			}
			
			$lhtml .= '</table>';
			
			return $lhtml; 
		}catch(Zend_Exception $e){
			die("Erro no view helper AnexoEvento". $e);
		}
	}
}