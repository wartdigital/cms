<?php
	/**
	 * Helper para conferir se as miss�es de uma disciplina podem ser alteradas
	 * @param int $cddisciplina c�digo da disciplina 
	 * @author Jo�o Pedro Grasselli
	 * @version 14/06/2012
	 * @return boolean
	 * */
class Zend_View_Helper_AlteracaoMissao extends Zend_View_Helper_Abstract {

	public function AlteracaoMissao($cddisciplina) {
		try{
			$banco_disciplina = new Disciplina_Model_Disciplina();
			return $banco_disciplina -> liberaAlteracao($cddisciplina);
		}catch(Zend_Exception $e){
			die($e);
		}
	}
}