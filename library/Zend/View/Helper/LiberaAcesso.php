<?php 

/**
 * View helper respons�vel por retornar se o usu�rio logado possui permiss�o para acessar determinado recurso
 * 
 * @author Jo�o Pedro Grasselli
 */
class Zend_View_Helper_LiberaAcesso{
	
	/**
	 * Fun��o respons�vel por retornar se o usu�rio logado possui permiss�o para acessar determinado recurso
	 * 
	 * @author Jo�o Pedro Grasselli
	 * @return bool
	 */
	public function LiberaAcesso($pResource){
		if(!empty($pResource)){
			$lAcessControlPlugin = new Login_Plugin_AccessControl();
			return $lAcessControlPlugin -> LiberaAcesso($pResource);
		}
	    return false;
	}
}