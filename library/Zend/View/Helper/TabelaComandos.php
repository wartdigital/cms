<?php class Zend_View_Helper_TabelaComandos{
	
	/**
	 * Helper que desenha a tabela de comandos que fica na parte superior e inferior das listagens
	 * Tamb�m testa se o usu�rio tem permiss�o para executar o comando representado no bot�o, por consequ�ncia, se pode visualizar este 
	 * @param $opcoes
 	 *					string - [url] - url que ser� acessada.
 	 *					string - [title] - o title caso o mouse passe por cima do link.
 	 *					string - [descricao] - a descri��o que ir� aparecer no link.
 	 *					string - [href] - uma chamada de �ncora - opcional.
 	 *@author Wiliam Passaglia Castilhos
 	 *@return strinh - HTML
 	 *@version 14/06/2012 
	 */
	public function TabelaComandos($opcoes, $pPaginator = NULL){
		try{
			$lHTML = '';
			
			$lHTML .= '<table border="0" cellpadding="0" cellspacing="0" class="tabela_listagem_aux">
						<tr>
							<td width="25" align="left">
						       	<a href="javascript:void(0)" title="Excluir itens selecionados"></a>
						    </td>
						    <td align="left">';
			
			//contador permite a partir do segundo bot�o colocar um espa�o entre eles   
			$i=0;
			foreach($opcoes as $opcao){
				$i++;
				if(!empty($opcao['url'])){
					$url = $opcao['url'];
					$url = explode('/', $url);
					$url = "/".$url[1]."/".$url[2]."/".$url[3];
				
					$url = str_replace("-","",$url);
				}
				
				$helper = new Zend_View_Helper_LiberaAcesso();
				if($i>1){
						$lHTML .= '<span class="esp_paginacao">';
					}
				if($helper->LiberaAcesso($url)){
					
					$lHTML .= '<a href="'.$opcao['url'].'" title="'.$opcao['title'].'">'.$opcao['descricao'].'</a>';
					
					
					if($i>1){
						$lHTML .= '</span>';
					}
				
				}elseif(!empty($opcao['href'])){
					$lHTML .= $opcao['href'];
				}
			}
			
			$lHTML.='</td>';
			
			if($pPaginator != NULL){
				$lHTML.='<td align="right">';
				$lHTML.= $pPaginator -> renderControls();
				$lHTML.='</td><td width="25"></td>';
			}
			
				$lHTML.='
					</tr>
			</table>';
			
			return $lHTML;
			
		}catch(Zend_Exception $error){
			die($error);	
		}
	}
}