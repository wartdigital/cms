<?php 
/**
 * View helper respons�vel pelo c�digo para a impressao de uma view
 * 
 * @author Jo�o Pedro Grasselli
 * @return string
 * @version 05/04/2013 
 */
class Zend_View_Helper_Impressao{

	public function Impressao(){
		$lStringImpressao = '		
		<script type="text/javascript">
			$(window).load(function(){
				self.print();
				self.close();
			});
		</script>';
		
 		return $lStringImpressao;
	}
} 
