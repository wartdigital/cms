<?php class Zend_View_Helper_NenhumRegistro{
	
	public function NenhumRegistro($altura=null){
		if ($altura!= null){
			return '
			<tr>
				<td height="'.$altura.'" colspan="12" align="center">
					Nenhum registro encontrado
				</td>
			</tr>';
		}else{
			return 
			'<tr>
				<td height="150" colspan="12" align="center">
					Nenhum registro encontrado
				</td>
			</tr>';
		}
	}
}