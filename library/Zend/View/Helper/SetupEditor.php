<?php
class Zend_View_Helper_SetupEditor {

	function SetupEditor( $pTextareaId,
						  $pToolbar = NULL,
						  $pWidth = NULL
						 ){
		//var_dump($toolbar);
		//exit;
		$lConfig = null;
		
		if ($pToolbar){
			$lConfig .="{toolbar : '".$pToolbar."'";	
			if ($pWidth){
				$lConfig .=", width: '".$pWidth."'}";
			}else{
				$lConfig .= " } ";
			}
		}else{
			if ($pWidth){
				$lConfig .="{width: '".$pWidth."'}";	
			}
		}
			
		if ($lConfig){
			return 
			"<script type=\"text/javascript\">
			CKEDITOR.replace( '". $pTextareaId ."', ".$lConfig.");
			</script>";
		}else{
			return 
			"<script type=\"text/javascript\">
			CKEDITOR.replace( '". $pTextareaId ."' );
			</script>";
		}
	}
}
