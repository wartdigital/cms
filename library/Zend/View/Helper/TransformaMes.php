<?php 
class Zend_View_Helper_TransformaMes extends Zend_View_Helper_Abstract {
	
	/**
	 * View Helper que retorna a string com o nome do mes conforme o seu n�mero
	 * @author Wiliam Passaglia Castilhos
	 * @param $cdmes int o c�digo do m�s
	 * @version 29/05/2012
	 * @return array
	 * */
	public function TransformaMes($cdmes) {
		switch($cdmes){
			case 1:
				return "Janeiro";
				break;
			case 2:
				return "Fevereiro";
				break;
			case 3:
				return "Mar�o";
				break;
			case 4:
				return "Abril";
				break;
			case 5:
				return "Maio";
				break;
			case 6:
				return "Junho";
				break;
			case 7:
				return "Julho";
				break;
			case 8:
				return "Agosto";
				break;
			case 9:
				return "Setembro";
				break;
			case 10:
				return "Outubro";
				break;
			case 11:
				return "Novembro";
				break;
			case 12:
				return "Dezembro";
				break;
		}
	}
}