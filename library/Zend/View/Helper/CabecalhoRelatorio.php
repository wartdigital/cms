<?php class Zend_View_Helper_CabecalhoRelatorio{
    protected $_separador = "<br>";

	public function CabecalhoRelatorio($nmrelatorio, array $filtros){
		switch(W3_System_Register::getSystem()){				case("w3i_acropolis"):
										$caminhoimagem = "/layouts/acropolis/img/logo.gif";break;
				case("w3i_icarus"):
										$caminhoimagem = "/layouts/aero/img/logo.gif";break;
		}
    	$lHTML = '<table width="90%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td width="15%" rowspan="2"><img src="'.$caminhoimagem.'" width="200" height="85"></td>
				<td align="center" style="font-size:16px;">'.$nmrelatorio.'</td>
			</tr>';
    	
    	$lHTML .= '<tr><td>';
    	/* EASTER EGG
    	 * echo '<img src="http://i0.kym-cdn.com/photos/images/original/000/096/044/trollface.jpg?1296494117">
    	<img src="http://25.media.tumblr.com/tumblr_lx5lqyG19e1qaa7gwo1_400.jpg">';
    	exit; */
    	foreach($filtros as $key =>$filtro){
    		$descricao = array();
	    	if(is_array($filtro[dsfiltro])){
	   
	    		if(count($filtro[dsfiltro])>3){
	    			$this->_separador = " , ";
	    		}
	    		$lHTML .= '<table class="tabelacadastro cab_relatorio" cellpadding="0" cellspacing="3" border="0" width="100%">';
		    	$lHTML .= '<tr><td width="20%" align="right"><b>';
		    	$lHTML .= $filtro[nmfiltro];
		    	$lHTML .= '</b></td><td>';
		    	
		    	$i=0;
		    	
	    		foreach($filtro[dsfiltro] as $key=>$desc){
	    			
	    			$descricao[] = $desc;
	    			if($i==9){
	    				$descricaolonga = true;
	    				break;
	    			}
	    			$i++;
	    		}
	    		unset($desc);
	    		
	    		if($descricaolonga){
	    			$lHTML .= wordwrap(implode($this->_separador,$descricao))." ...";
	    		}else{
	    			$lHTML .= implode($this->_separador,$descricao);
	    		}
	    		
	    		$lHTML .= '</td></tr></table>';
	    	}else{
	    		$lHTML .= '<table class="tabelacadastro cab_relatorio" cellpadding="0" cellspacing="3" border="0" width="100%">';
	    		$lHTML .= '<tr><td width="20%" align="right"><b>';
	    		$lHTML .= $filtro[nmfiltro];
	    		$lHTML .= '</b></td><td>';
	    		$lHTML .= $filtro[dsfiltro];//$descricao;//$filtro[dsfiltro];
	    		$lHTML .= '</td></tr></table>';
	    	}
    		
    		//$lHTML .= '<br>';
    	}	   
    	$lHTML .= '</tr></td><tr><td colspan="2">
    				<font size="1">Gerado em '.date("d/m/Y H:i:s").' por '. 
					  $auth= Zend_Auth::getInstance()->getStorage()->read()->nmpessoa.'  
        		    </font></td>';
    	
    	$lHTML .= '</tr>
		           </table>';

		$lHTML .= '<br>';
					  
		return $lHTML;
    }

}