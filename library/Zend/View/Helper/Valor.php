<?php 
class Zend_View_Helper_Valor{
	/**
	 * Helper que recebe como par�metro o valor vindo do arquivo de retorno 
	 * do banco no formato 000000000000000(15 d�gitos), sendo os dois �ltimos caracteres os centavos.
	 * Essa fun��o retira os "0" da esquerda, e caso haja centavos coloca um ponto como divisor
	 * @param $valor string - o valor a ser formatado
	 * @author Wiliam Passaglia Castilhos
	 * @version 02/04/2012
	 * @return o valor formatado =>0 ou 0.01 
	 * */
	public function Valor($valor) {
		$valor = ltrim($valor,"0");
		
		$ultimaPosicaoDentroString = strlen($valor);
		if($ultimaPosicaoDentroString == 2){
			//foi passado somente centavos
			return "0.".$valor;
		}elseif($ultimaPosicaoDentroString ==1){
			return "0.0".$valor;
		}elseif($ultimaPosicaoDentroString ==0){
			//passado como par�metro 00000...
			return "0.00";
		}
		
		if(substr($valor,($ultimaPosicaoDentroString-2))!="0" || substr($valor,($ultimaPosicaoDentroString-1)) !="0"){
			return substr($valor,0,($ultimaPosicaoDentroString-2)).".".substr($valor,($ultimaPosicaoDentroString-2),2);
		}else{
			return substr($valor,0,($ultimaPosicaoDentroString-2)).".00";
		}
		return false;
	}

}