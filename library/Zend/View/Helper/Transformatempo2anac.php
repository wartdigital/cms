<?php class Zend_View_Helper_Transformatempo2anac{



function Transformatempo2anac($pHoraSistema) {
   $lVetHoraSistema = explode(":", $pHoraSistema);
   $lHora   = $lVetHoraSistema[0];
   $lMinuto = $lVetHoraSistema[1];

   // N�o fecha com tabela ANAC quando 00:20 => � 0,4 e n�o 0,3 como calculado
   // return ($lVetHoraSistema[0] * 1) + round(($lVetHoraSistema[1] / 60),1);
  // $lValor = 0;
   if ($lMinuto >= 1 AND $lMinuto <= 6) {
      $lValor = 0.1;
   }
   elseif ($lMinuto >= 7 AND $lMinuto <= 12) {
      $lValor = 0.2;
   }
   elseif ($lMinuto >= 13 AND $lMinuto <= 18) {
      $lValor = 0.3;
   }
   elseif ($lMinuto >= 19 AND $lMinuto <= 24) {
      $lValor = 0.4;
   }
   elseif ($lMinuto >= 25 AND $lMinuto <= 30) {
      $lValor = 0.5;
   }
   elseif ($lMinuto >= 31 AND $lMinuto <= 36) {
      $lValor = 0.6;
   }
   elseif ($lMinuto >= 37 AND $lMinuto <= 42) {
      $lValor = 0.7;
   }
   elseif ($lMinuto >= 43 AND $lMinuto <= 48) {
      $lValor = 0.8;
   }
   elseif ($lMinuto >= 49 AND $lMinuto <= 54) {
      $lValor = 0.9;
   }
   elseif ($lMinuto >= 55 AND $lMinuto <= 60) {
      $lValor = 1.0;
   }
   
   if((!empty($lHora) && $lHora != "00" )|| ($lValor != NULL)){
   		$retorno = ($lHora * 1) + $lValor;
   }else{
   		$retorno = "";
   } 
return $retorno;
/*
Convers�o normal
0,1 = 01 a 06 min
0,2 = 07 a 12 min
0,3 = 13 a 18 min
0,4 = 19 a 24 min
0,5 = 25 a 30 min
0,6 = 31 a 36 min
0,7 = 37 a 42 min
0,8 = 43 a 48 min
0,9 = 49 a 54 min
1,0 = 55 a 60 min

Convers�o inversa para sistema anac
0,1 =  06 min
0,2 =  12 min
0,3 =  18 min
0,4 =  24 min
0,5 =  30 min
0,6 =  36 min
0,7 =  42 min
0,8 =  48 min
0,9 =  54 min
1,0 =  60 min
*/
}
}