<?php 

class Zend_View_Helper_BuscaAvancada extends Zend_View_Helper_Abstract {
	
	/**
	 * View Helper que retorna o HTML contendo o link para expandir a busca
	 * @author Jo�o Pedro Grasselli
	 * @param 
	 * @return string HTML contendo o link para expandir a busca
	 * @version 01/03/2013
	 */
	public function BuscaAvancada(){
		return '
			<script type="text/javascript">
				function buscaAvancada(pLink){
					$(\'*[avancado^="true"]\').each(function(){
						$(this).animate(
							  {
							    opacity: "1",
							    display: "show"
							  }, 800, function() {
								  $(this).toggle();
							  }
						);  
					});
			
					//$(pLink).text("busca simplificada");
					$(pLink).addClass("avancada-ativa");
					$(pLink).attr("onclick", "simplificaBusca(this)");
				}
		
				function simplificaBusca(pLink){
					$(\'*[avancado^="true"]\').each(function(){
						$(this).animate(
							  {
							    opacity: "0"
							  }, 800, function() {
								  $(this).toggle();
							  }
						);  
					});
					
					//$(pLink).text("busca avan�ada");
					$(pLink).removeClass("avancada-ativa");
					$(pLink).attr("onclick", "buscaAvancada(this)");
				}
			</script>
	
			<div class="box_filtro">
				<p class="box_titulo">
					&nbsp;
				</p>
				
				<p class="box_titulo" style="float:right">
					<a href="javascript:void(0)" onclick="buscaAvancada(this)" class="linkBuscaAvancada">busca avan�ada</a>
				</p>
			</div>';
	}
}
