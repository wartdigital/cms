<?php 
class Zend_View_Helper_Getnumeros{

	/**
	 * Function que retorna a string passada por par�metro sem formata��o, apenas com n�meros 
	 *@param string $pValueString
	 *@author Wiliam Passaglia Castilhos
	 *@version 01/10/2012
	 *@return String somente com numeros
	 */
	public function Getnumeros($pValueString) {
   		$retorno = "";
   		$length = strlen($pValueString);
			for($i = 0; $i < $length; $i++){
				$char = substr($pValueString,$i,1);
				if(is_numeric($char))
					$retorno = $retorno.$char;
			}
		return $retorno;
	}
}
