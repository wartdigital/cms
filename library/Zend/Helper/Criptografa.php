<?php
// classe de criptografia de senha
class Zend_Controller_Action_Helper_Criptografa extends Zend_Controller_Action_Helper_Abstract {
	
	public $pluginLoader;
	
	public function __construct() {
	       $this->pluginLoader = new Zend_Loader_PluginLoader();
	}

    public function criptografa($pPassword) {
	       $lPassword = sha1(md5($pPassword));
	       return $lPassword;
	}
}
