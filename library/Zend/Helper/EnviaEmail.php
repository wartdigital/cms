<?php
/** Classe de envio de emails
 * @author Gustavo Pistore
 */
class Zend_Controller_Action_Helper_EnviaEmail extends Zend_Controller_Action_Helper_Abstract {
		
		/**
		 * EnviaEmail
		 *
		 * @desc Pega o valor de um par�metro do banco
		 * @param int param
		 * @return string $valor
		 */
	
		public $pluginLoader;
		public $mail;
		
		protected $config;
		protected $transport;
		protected $tipoDeSegurancaSMTP;
		protected $portaSMTP;	
		
		protected $gTexto;
		
		/**
		 * Flag que indica se ser� utilizado algum css com a mensagem
		 */
		protected $useSMTP = false;
		
		/**
		 * Flag que indica se ser� utilizado algum css com a mensagem
		 */
		protected $useCss;
		/**
		 * Css que ser� utilizado na mensagem
		 */
		protected $gCss;
		
		public function __construct() {
			$this->pluginLoader = new Zend_Loader_PluginLoader();

			//Testamos se devemos utilizar o envio via STMP
			$gConfig = Zend_Registry::get('gConfig');
			if($gConfig['mail']['smtp']){
				//Configura��es para envio via SMTP
				$lBdParam = new Parametros_Model_Parametros();
				
	    		$smtp 			= $lBdParam->getparametro("smtp");
				$smtpusuario 	= $lBdParam->getparametro("smtpusuario");
				$smtpsenha 		= $lBdParam->getparametro("smtpsenha");
				$smtp_porta 	= $lBdParam->getparametro("smtp_porta");
				$smtp_seguranca = $lBdParam->getparametro("smtp_seguranca");
				
				$config = array('auth' 		=> 'login',
	    		            	'username'	=> $smtpusuario,
	    		           	 	'password'	=> $smtpsenha,
								'ssl' 		=> $smtp_seguranca,
								'port'		=> $smtp_porta);
				
				$this -> useSMTP = true;
				$this -> transport = new Zend_Mail_Transport_Smtp($smtp, $config);
			}else{
				$this -> useSMTP = false;
			}
			
			$this->mail = new Zend_Mail('iso-8859-1');
			
			self::resetCss();
	 		$this -> useCss = false;
		}
	
	   /** Action para configurar mensagem
	 	* @author Gustavo Pistore
	 	* @param $texto
	 	*/
		public function settexto($texto){
			$this -> gTexto = $texto;
			//$this->mail->setBodyHtml($texto);
		}
		
	  /** Fun��o que indica se ser� utilizado css ou n�o
	   * @author Jo�o Pedro Grasselli
	   * @param boolean $pUse
	   * @return void
	   * @version 09/10/2012
	   */
		public function useCss($pUse){
			if($pUse){
				$this -> useCss = true;
			}else{
				$this -> useCss = false;
			}
		}
		
	  /** Fun��o que seta o css que sera utilizado
	   * @author Jo�o Pedro Grasselli
	   * @param $pCss
	   * @return void
	   * @version 09/10/2012
	   */
		public function setCss($pCss){
			$this -> gCss = $pCss;
		}
		
	  /** Fun��o que retorna o css atual
	   * @author Jo�o Pedro Grasselli
	   * @param 
	   * @return string
	   * @version 09/10/2012
	   */
		public function getCss(){
			return $this -> gCss;
		}
		
	  /** Fun��o que reseta o css, passando a utilizar o padr�o
	   * @author Jo�o Pedro Grasselli
	   * @param 
	   * @return void
	   * @version 09/10/2012
	   */
		public function resetCss(){
			$this -> gCss = 'body {
								background-color: #FFFFFF;
								margin-left: 20px;
								margin-top: 0px;
								margin-right: 0px;
								margin-bottom: 0px;
								text-align:left;
							}
							body,td,th {
								font-family: Tahoma, Verdana, Arial, sans-serif;
								font-size: 11px;
							}
							.tabelapadrao {
								font-family: Tahoma, Arial, Verdana, sans-serif;
								font-size: 11px;
								border: 1px solid #CCCCCC;
								padding: 3px;
								background-color:#FFFFFF;
							}
							.tabelapadraocabecalho {
								font-family: Tahoma, Arial, Verdana, sans-serif;
								font-size: 11px;
								font-weight: bold;
								color: #000000;
								background-attachment: fixed;
								background-color: #D8DDEC;		
								background-repeat: repeat-x;
								background-position: left top;
								height: 20px;
							}
							.linhapadrao{
								background-color:#EBEBEB;
								font-family: Tahoma, Arial, Verdana, sans-serif;
								color: #000000;
								font-weight:bold;
								font-size: 11px;
								padding: 5px;
							}
							.corlinhaclara{
								background-color:#EEEEEE;
							}
							.corlinhaescura{
								background-color:#FFFFFF;
							}
							.linkbranco {
								color: #000000;
								text-decoration: none;
							}
							.linkpreto {
								color: #000000;
								text-decoration: none;';
		}
		
		/** Action para configurar remetente
		 * @author Gustavo Pistore
		 * @param string $email
		 * @param string $nome
		 */
		
		public function setrem($email,$nome = NULL){
			$this->mail->setFrom($email,$nome);
		 	$this->mail->setReturnPath($email);
		}
	
		/** Action para configurar destinat�rio
		 * @author Gustavo Pistore
		 * @param string $email
		 * @param string $nome
		 */
		public function setdest($email, $nome = NULL){
			$this->mail->addTo($email, $nome);
		}
		
		/** Action para configurar assunto
		 * @author Gustavo Pistore
		 * @param $texto
		 */
		public function setassunto($texto){
			$this->mail->setSubject($texto);
		}

		/** Action para adicionar anexo
		 * @author Gustavo Pistore
		 * @param $caminho
		 * @param $nome
		 * @param $tipo
		 */
		public function addanexo($caminho, $nome, $tipo){
			$this->mail->createAttachment(file_get_contents($caminho), 
										  $tipo, 
										  Zend_Mime::DISPOSITION_INLINE, 
										  Zend_Mime::ENCODING_BASE64, 
										  $nome);
		}
		
		/**
		 * 
		 * @param mixed $pAttachment
		 */
		public function addAnexoFile($pAttachment){
			$this->mail->addAttachment($pAttachment);
		}
		
		/** Fun��o para adicionar c�pia carbono
		 * @author Jo�o Pedro Grasselli
		 * @param string $pEmail
		 * @param string $pNome
		 */
		public function addCc($pEmail, $pNome){
			$this -> mail -> addCc($pEmail, $pNome);
		}
		
		/** Fun��o para adicionar c�pia carbono oculta
		 * @author Jo�o Pedro Grasselli
		 * @param string $pEmail
		 * @param string $pNome
		 */
		public function addBcc($pEmail, $pNome){
			$this -> mail -> addBcc($pEmail, $pNome);
		}
		
		/** Fun��o para enviar email
		 * @author Gustavo Pistore
		 */
		public function enviar(){
			$lMsg = '';
			if($this -> useCss){
				$lMsg .= '<style type="text/css"><!--'
							.$this -> gCss.
						'--></style>';
			}
			$lMsg .= $this -> gTexto;
			$this->mail->setBodyHtml($lMsg);
			
			if($this -> useSMTP){
				$this->mail->send($this->transport);
			}else{
				$this->mail->send();
			}
		}

}