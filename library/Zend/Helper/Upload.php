<?php 
	/** Helper utilizado no upload de arquivos 
	 * @author Jo�o Pedro Grasselli
	 */
class Zend_Controller_Action_Helper_Upload extends Zend_Controller_Action_Helper_Abstract {
	
	private $location;
	private $target; 

	public function Zend_Controller_Action_Helper_Upload() {
		$this->pluginLoader = new Zend_Loader_PluginLoader ();
		$this->location = $_SERVER['DOCUMENT_ROOT']."/uploads/";
	}

    /** Gera um nome aleat�rio e �nico para renomear arquivos no servidor
	 * @author Jo�o Pedro Grasselli
	 * @param 
	 * @return string com um nome para o arquivo f�sico
	 * @version 07/05/2012
	 */
	private function GeraNome(){
		$fisic_name = session_id().microtime();
		$fisic_name = str_replace(' ', "", $fisic_name);
		$fisic_name = str_replace('.', "", $fisic_name);
		
		return $fisic_name;
	}

    /** Set para indicar qual a pasta em que ser� realizado o upload
	 * @author Jo�o Pedro Grasselli
	 * @param string $target nome da pasta dentro de /uploads/
	 * @return 
	 * @version 07/05/2012
	 */
	public function setTarget($target){
		if(!empty($target)){
			$this->target = $target;
		}
	}

	/** Retorna a pasta na qual ser� feito o upload
	 * @author Jo�o Pedro Grasselli
	 * @param 
	 * @return string com o nome da pasta
	 * @version 07/05/2012
	 */
	public function getTarget(){
		return $this->target;
	}

    /** Realiza o upload dos arquivos para o servidor e retorna os dados para posterior grava��o dos dados
	 * @author Jo�o Pedro Grasselli
	 * @param Zend_File_Transfer $upload objeto contendo todos os arquivos do upload
	 * @param array $pOptions array com as op��es de configura��o do upload
	 * 				KEEP_EXTENSION salva o arquivo com a sua extens�o
	 * @return array com os dados de todos os arquivos em que o upload foi realizado com sucesso
	 * @version 07/05/2012
	 */
    public function upload(Zend_File_Transfer $upload, array $pOptions = NULL){
    	
    	$files = $upload->getFileInfo();
    	
    	if(is_array($files)){
			    $anexos = array();
			
			    foreach($files as $cdtipoanexo=>$file){
			    	
			    	$lTempName = $file["tmp_name"];
			    	$file = $file["name"];
			    	if(!empty($file)){
					    //separa a extens�o do arquivo
					    $file_extension = strtolower(end(explode(".", $file)));
					    
					    //pega o nome do arquivo
					    $lenght_extension = strlen($file_extension)+1;
					    $file_name = substr($file, 0, strlen($file)-$lenght_extension);
					    $file_name = end(explode("/", $file_name));	
			
					    //criando um nome fisico para o arquivo
					    $fisic_name = $this->GeraNome();
					    $fisic_name = $cdtipoanexo.$fisic_name;
						
					    $pasta = $this->location.$this->target."/";

					    if($pOptions['KEEP_EXTENSION']){
					    	$fisic_name = $fisic_name.".".$file_extension;
					    }

					    $upload->addFilter('Rename', array('target' => $pasta.$fisic_name,'overwrite' => false));
					   	
					   	try {
					   		$upload->receive($file);
							
					    	$anexos[$cdtipoanexo] = array('nmarquivo'=>$file_name,
					    					  			  'nmfisico'=>$fisic_name,
					    					              'nmextensao'=>$file_extension,
					    					              'nmpasta'=>$this->target,
					    								  'cdtipoanexo'=>$cdtipoanexo);
					    	
			          	} catch (Zend_File_Transfer_Exception $e) {
			          	    die($e->getMessage());
			       	    } catch (Zend_Exception $ze) {
			          	    die($ze->getMessage());
			       	    }
			    	}	
	    		}
	    		return $anexos;
		    }
    }
}