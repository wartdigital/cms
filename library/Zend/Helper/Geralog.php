<?php 
class Zend_Controller_Action_Helper_Geralog extends Zend_Controller_Action_Helper_Abstract {
	
 public function __construct() {
       $this->pluginLoader = new Zend_Loader_PluginLoader ();
    }

    /** Registra o log das a��es realizadas no sistema
    * @author Jo�o Pedro Grasselli
    * @param string $nmmodule nome domodule
    * @param string $nmcontroller nome do controller
    * @param string $nmaction nome da action
    * @param array $vlvalor valor que identifique a opera��o realizada
    * @return void
    */
    
 public function Geralog($nmmodule, $nmcontroller, $nmaction, $vlvalor){
 	//C�digo do usu�rio
	$cdusuario = Zend_Auth::getInstance()->getStorage()->read()->cdpessoa;
	//IP do usu�rio
	$ip = $_SERVER[REMOTE_ADDR];
 	//Data e hora atual
	$data = date("YmdHis");
 	//Nome do Controller
 	//$nmcontroller = "";
	//Nome da Action
 	//$nmaction = "";
	//Valores
	//$valores = "";
 	
	$dados = array("cdusuario"=>$cdusuario,
				   "IP"=>$ip,
				   "dtlog"=>$data,
				   "nmmodule"=>$nmmodule,
 			       "nmcontroller"=>$nmcontroller,
				   "nmaction"=>	$nmaction,
				   "vlvalor"=>$vlvalor,
	);
	$banco = new Default_Model_Log();
	$banco ->addLog($dados);
 }				
}