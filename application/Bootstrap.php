<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap{

	protected $_view        = null;
	
	//Doctype
	protected function _initDoctype(){
 		$this->bootstrap('view');
		$view = $this->getResource('view');
		
		//Seta Zend_Locale para Brasil
		$lLocale = new Zend_Locale('pt_BR');
		Zend_Registry::set('Zend_Locale', $lLocale);
		
		//Titulo do Projeto	
		$view->headTitle('CarSis') -> setSeparator(' | ');

        $view->doctype('HTML5');
        $view->setEncoding('UTF-8');
	}
	
	//Zend_Session
	protected function _initSession(){
		Zend_Session::start();
	}
	
	//TRADUÇÃO DOS ERROS PARA PT-BR
	protected function _initTranslate() {
  		try{
  			$translator = new Zend_Translate ( array (
                        'adapter' => 'array', 
                        'content' => APPLICATION_PATH.'/configs/languages/pt_BR/',
                        'locale' => 'pt_BR', 
                        'scan' => Zend_Translate::LOCALE_DIRECTORY 
            ) );
            Zend_Validate_Abstract::setDefaultTranslator ( $translator );
  		}catch(Exception $e){
  			echo  $e->getMessage(); 
  			print_r($e->getTrace());
  		}
  }

	//Autenticação
	protected function _initAuth(){
      	$this->bootstrap('FrontController');
      	$auth = Zend_Auth::getInstance();
	}	

   	protected function _initHelpers(){
   		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
    	$viewRenderer->initView();
 
    	//add zend action helper path
    	Zend_Controller_Action_HelperBroker::addPath('Zend/Helper/');
    	//Zend_Controller_Action_HelperBroker::addPath('Comum/Helper/');
	}

}


