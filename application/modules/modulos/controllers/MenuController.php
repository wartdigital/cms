<?php

class Modulos_MenuController extends Zend_Controller_Action{

    public function init(){}
	
	/**
	 *  Listagem de todos os menus
	 */
    public function indexAction(){
		$lBdMenu = new Modulos_Model_Menu();
		$lRows = $lBdMenu->listar();
		$this->view->menu = $lRows;
    }
    
	/**
	 *  Adi��o de novo menu
	 */
    public function addAction(){
        $this->getHelper('viewRenderer')->setRender('cadastro');
		$lForm = new Modulos_Form_InsereMenu();
		$this->view->form = $lForm;
		
		$this -> view -> pCabecalho = 'Inclus�o';

		if ($this->getRequest ()->isPost ()) {
			$lPost = $this->getRequest()->getPost();
			$lBdMenu = new Modulos_Model_Menu();
			
			$lMenu['nmmenu'] 	= $lPost['nmmenu'];
			$lMenu['fgativo']	= '1';
			
			$lBdMenu->inserir ($lMenu);
			return $this->_redirect ( '/modulos/menu/index' );
		}
    }
    
	/**
	 *  Edi��o de menu
	 */
    public function editAction(){
		$this->getHelper('viewRenderer')->setRender('cadastro');
	   
		$lForm = new Modulos_Form_InsereMenu();

		$lCdMenu = $this->getRequest()->cdmenu;
		$lBdMenu = new Modulos_Model_Menu();
		$lMenu = $lBdMenu->buscar ($lCdMenu);
		
		$this -> view -> pCabecalho = 'Altera��o';
		
		$lForm->populate($lMenu);
		$this->view->form = $lForm;
		
		if ($this->getRequest()->isPost() ) {
			$lPost = $this->getRequest()->getPost();
			
			$lMenu['nmmenu'] 	= $lPost['nmmenu'];
			$lMenu['fgativo']	= '1';
			
			$lBdMenu->editar($lMenu, $lCdMenu);
			
			return $this->_redirect('/modulos/menu/index');
		}
    }

    /**
	 *  Exclus�o de menu
	 */
    public function deleteAction(){
    	$lBdMenu = new Modulos_Model_Menu ();
    	$lCdMenu = $this->getRequest()->cdmenu;
		
    	if($lCdMenu != ""){
    		$lBdMenu->excluir($lCdMenu);
    		return $this->_redirect('/modulos/menu/index');
    	}
    }

}
