<?php
class Modulos_ModulosController extends Zend_Controller_Action{

    public function init(){}
	
	/**
	 *  Filtro de m�dulos
	 */
	public function indexAction(){
		$lFormModulo = new Modulos_Form_FiltroModulo();
	    $this-> view-> pFormModulo = $lFormModulo;
    	
		if($this->getRequest()->isPost()){
			$dados = $this->getRequest()->getPost();
			
			$session = new Zend_Session_Namespace('modulo', true);
			$session->filtro = $dados;
	    
			return $this->_redirect('/modulos/modulos/listagem');
		}
    }
    
	/**
	 *  Listagem de m�dulos
	 */
	public function listagemAction(){
	    $lBdModulos = new Modulos_Model_Modulos();
				
		$session = new Zend_Session_Namespace('modulo', true);
		$dados = $session->filtro;
			
		$lBdModulos->setFiltro($dados[busca],
							   $dados[cdmenu]
							  );
		$modulos = $lBdModulos->BuscaComFiltro();
			
		$this->view->modulos = $modulos;
	}
		
		
	/**
	 *  Adi��o de novos m�dulos
	 * @author Wiliam Passaglia Castilhos
	 * @param void
	 * @return void
	 * @version 19/09/2011
	 */
    public function addAction(){
		$this->getHelper ( 'viewRenderer' )->setRender('cadastro');
		
		$lForm = new Modulos_Form_InsereModulos(array('cdmodulo' => ''));
		$this->view->formulario = $lForm;
		
		$this->view->cabecalho = "Inclus�o";
		
		if ($this->getRequest()->isPost()) {
			$lPost = $this->getRequest()->getPost();
			
			$lCdModulo			= $lPost['cdmodulo'];
			$lValues[nmsigla]	= $lPost['nmsigla'];
			$lValues[dsmodulo]	= $lPost['dsmodulo'];
			$lValues[caminho]	= $lPost['caminho'];
			$lValues[nmmodulo]	= $lPost['nmmodulo'];
			$lValues[fgativo]	= $lPost['fgativo'];
			
			if($lPost['cdmenu'] != NULL){
				$lValues[cdmenu] = $lPost['cdmenu'];
			}else{
				$lValues[cdmenu] = NULL;
			}
			
			$lBDModulos = new Modulos_Model_Modulos();
			$lBDModulos -> inserir ($lValues);
			$lCdModulo = $lBDModulos -> lastID();
	
			$lCount = 0;
			$lBDActions = new Modulos_Model_Actions();
			foreach($lPost[nmaction] as $action){
				$values_action = array(
										'fgindex'		=>$lPost[fgindex][$lCount],
										'nmaction'		=>$lPost[nmaction][$lCount],
										'nmtitulo'		=>$lPost[nmtitulo][$lCount],
										'cdnivel'		=>$lPost[cdnivel][$lCount],
										'cdmodulo'		=>$lCdModulo
										);
				$lCount++;
				
				$lBDActions -> inserir($values_action);
			}
			
			return $this->_redirect ( '/modulos/modulos/index' );
		}
    }
	/**
	 * Edi��o de m�dulos
	 */
    public function editAction(){
        $this->getHelper ('viewRenderer')->setRender('cadastro');
		
        $lCdModulo = $this->getRequest()->cdmodulo;
		$lBdModulos = new Modulos_Model_Modulos();
		$lBdActions = new Modulos_Model_Actions;
		
		$lModulo = $lBdModulos->buscar($lCdModulo);
		
		$lForm = new Modulos_Form_InsereModulos(array('cdmodulo' => $lCdModulo));
		$lForm->populate($lModulo);
		$this->view->formulario = $lForm;
		
		$this->view->cabecalho = "Altera��o";
		
		$this->view->cdmodulo = $lCdModulo;

		$lActions = $lBdActions->listar($lCdModulo);
		$this->view->actions = $lActions;
		
		if ($this->getRequest ()->isPost ()) {
			$lPost = $this->getRequest()->getPost();

			$values[nmsigla]	= $lPost['nmsigla'];
			$values[dsmodulo]	= $lPost['dsmodulo'];
			$values[caminho]	= $lPost['caminho'];
			$values[nmmodulo]	= $lPost['nmmodulo'];
			$values[fgativo]	= $lPost['fgativo'];

			if($lPost['cdmenu'] != NULL){
				$values[cdmenu] = $lPost['cdmenu'];
			}else{
				$values[cdmenu] = NULL;
			}

			$lActions = $lBdActions->listar($lCdModulo);
			foreach($lActions as $lKey => $action){
				$inserir =array('fgindex'	=> $lPost['fgindex'.$action[cdaction]],
								'nmtitulo'	=> $lPost['nmtitulo'.$action[cdaction]],
								'nmaction'	=> $lPost['nmaction'.$action[cdaction]],
								'cdnivel'	=> $lPost['cdnivel'.$action[cdaction]],
								'cdmodulo'	=> $lCdModulo
								);
				$lBdActions-> editar($inserir, $action[cdaction]);
			}
			
			$count=0;
			foreach($lPost[nmaction] as $action){
				if(!empty($action)){
					$inserir =array('fgindex'	=> $lPost[fgindex][$count],
									'nmaction'	=> $action,
									'nmtitulo'	=> $lPost[nmtitulo][$count],
									'cdnivel'	=> $lPost[cdnivel][$count],
									'cdmodulo'	=> $lCdModulo
									);
					$count++;
					$lBdActions -> inserir($inserir);
				}
			}
			
			$lBdModulos->editar($values,$lCdModulo);
			
			$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
			$manager = $bootstrap->getResource('cachemanager');
			$cache = $manager->getCache('acl');
			$cache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
			return $this->_redirect('/modulos/modulos/index');
	}
 }
    
	/**
	 * Fun��o que seta com exclu�do um m�dulo no banco
	 * @author Wiliam Passaglia Castilhos
	 * @param void
	 * @return void
	 * @version 19/09/2011
	 */
    public function deleteAction()
    {
		$this->getHelper ( 'viewRenderer' )->setRender(index);
        $banco = new Modulos_Model_Modulos();
    	$cod = $this->getRequest();
    	if($cod->cdmodulo!=""){
	   		$banco->excluir($cod->cdmodulo);
	   		//$helper = $this->_helper->getHelper('Geralog');
			//$helper -> Geralog($this->_request->getControllerName(), $this->_request->getActionName(), $cod->cdmodulo);
	    	return $this->_redirect('/modulos/modulos/index');
    	}
    }
}







