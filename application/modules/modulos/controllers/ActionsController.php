<?php

class Modulos_ActionsController extends Zend_Controller_Action{

    public function init(){}

    public function indexAction(){}
	
	/**
	 *  Fun��o que exclui uma action do banco
	 */
    public function deleteAction(){
    	$lCdAction = $this->getRequest()->cdaction;
		$lCdModulo = $this->getRequest()->cdmodulo;
		
    	if($lCdAction != ""){
			$lBdAction = new Modulos_Model_Actions();
	   		$lBdAction->excluir($lCdAction);
			
	   		$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
			$manager = $bootstrap->getResource('cachemanager');
			$cache = $manager->getCache('acl');
			$cache->clean(Zend_Cache::CLEANING_MODE_ALL);
	   		
	    	return $this->_redirect('/modulos/modulos/edit/cdmodulo/'.$lCdModulo);
    	}
    }
}