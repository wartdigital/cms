<?php

class Modulos_Model_Modulos extends Comum_Db_Table{
	protected $_name = 'itr_modulos';
	protected $_primary = 'cdmodulo';
	
	private $_busca;
	
	private $_menu;
	
	/** Lista os modulos cadastrados
	  * @author Wiliam Passaglia Castilhos
	  * @param void
	  * @return consulta banco
	  * @version 19/09/2011
	  */
	public function listar(){
		try {
			$where = $this->getAdapter()->quoteInto("fgativo <> ?", '9');
			return $this->fetchAll($where,"nmmodulo");
			}catch (Zend_Db_Exception $e){
				echo "Erro na consulta ao banco de dados: ".$e;
		}
	}
	/** Adiciona novo modulo
	  * @author Wiliam Passaglia Castilhos
	  * @param string $modulo nome do modulo a ser adicionado 
	  * @return void
	  * @version 19/09/2011
	  */
	public function inserir(Array $modulo){
		try{
			$modulo = self::colunas($modulo);
			return parent::insert($modulo);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}

	/** Busca um modulo cadastrado
	  * @author Wiliam Passaglia Castilhos 
	  * @param string $cert 
	  * @return array contendo o resultado da busca
	  * @version 19/09/2011
	  */
	public function buscar($cert = null){
		try {
			$where = '1';
			if(!empty($cert)){
				$where = $this->getAdapter()->quoteInto("cdmodulo = ?", $cert);					
			}
			return $this->fetchRow($where)->toArray();
		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados";
		}
	}
		
	/** Edita um modulo
	  * @author Wiliam Passaglia Castilhos
	  * @param string docs nome a ser adicionado, string $id valor da chave prim�ria da linha modificada no banco 
	  * @return -
	  * @version 19/09/2011
	  */
	public function editar(Array $docs,$id){
		try{
			$where = $this->getAdapter()->quoteinto("cdmodulo=?",$id);
			$docs = self::colunas($docs);
			return  parent::update($docs,$where);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}
		
		
	/** Exclui uma action no m�dulo
	 * @author Wiliam Passaglia Castilhos
	 * @param string $docs codigo do tipo a ser excluido
	 * @return void
	 * @version 19/09/2011
	 */
	public function delete($docs){
		try{
			$tabela = new Zend_Db_Table('itr_action');
			$where = $tabela->getAdapter()->quoteInto("cdaction= ?", $docs);
			$tabela->delete($where);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}
	/** Seta com exclu�do o m�dulo passado por par�metro 
	 * @author Wiliam Passaglia Castilhos
	 * @param string $docs codigo do tipo a ser excluido
	 * @return void
	 * @version 19/09/2011
	 */
	public function excluir($docs){
		try{
			$where = $this->getAdapter()->quoteInto("cdmodulo= ?", $docs);
			$dados = array ("fgativo" => '9');
			parent::update($dados, $where);
			
			$tabela = new Zend_Db_Table('itr_action');
			$tabela->delete($where);
			
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}
	
	/** Busca o nome do m�dulo 
	  * @param string $docs nome a ser resgatado do banco
	  * @return void
	  * @version 19/09/2011
	  */
	public function buscanome($sigla = null){
		try {
			$where = '1';
			if(!empty($sigla)){
				$where = $this->getAdapter()->quoteInto("nmsigla = ?", $sigla);					
			 }
			return $this->fetchRow($where);
		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados";
		}
	}
	
	public function BuscaComFiltro(){
		try {
			$select = $this ->getAdapter()->select()
			                ->from(array('m'=> 'itr_modulos'))
		         	        ->where('m.fgativo <> 9')
		         	        ->order('nmmodulo ASC');
		         		
			if (!empty($this->_busca)){
					$select->where("m.nmmodulo LIKE '%".$this->_busca . "%' OR m.nmsigla LIKE '%".$this->_busca . "%' OR m.dsmodulo LIKE '%".$this->_busca . "%'");	
			}
			
			if($this->_menu == "#"){
				$select->where("m.cdmenu IS NULL");					
			 }
			else{
				if(!empty($this->_menu)){
					$select->where("m.cdmenu =".  $this ->_menu);					
				 }
			 }
			 $stmt = $this->getAdapter()->query($select);
			 $result = $stmt->fetchAll();	
			return $result;
		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados";
		}
	}
	
	public function setFiltro($pBusca=null, $pMenu=null){
		if(!empty($pBusca)){
			$this->_busca=$pBusca;
		}
		if(!empty($pMenu)){
			$this->_menu=$pMenu;
		}

	}

}