<?php

class Modulos_Model_Menu extends Comum_Db_Table
{
        protected $_schema = 'security';
		protected $_name = 'security.zapp_menu';
		protected $_primary = 'cdmenu';
	
	/** Lista os menus cadastrados
	  */
		public function listar(){
			try {

				$where = $this->getAdapter()->quoteInto("fgativo <> ?", '9');
				return $this->fetchAll($where,"nmmenu");
				}catch (Zend_Db_Exception $e){
					echo "Erro na consulta ao banco de dados ".$e;
					
			}
		}
	
	/** Adiciona novo menu
	  */
		public function inserir(Array $docs){
			try{
				parent::insert($docs);
			}catch(Zend_Db_Adapter_Exception $e){
				echo $e->getMessage();
			}
		
		}

	 /** Busca um menu cadastrado
	  */
		public function buscar($cert = null){
			try {
				if(!empty($cert)){
					$where = $this->getAdapter()->quoteInto("cdmenu = ?", $cert);					
				 }
				return $this->fetchRow($where)->toArray();
			}catch (Zend_Db_Exception $e){
					echo "Erro na consulta ao banco de dados: ".$e;
			}
		}
		
	/** Editar menu
	  */	
		public function editar($pMenu, $pCdMenu){
			try{
				$lWhere = $this->getAdapter()->quoteinto("cdmenu = ?",$pCdMenu);
				return  parent::update($pMenu,$lWhere);
			}catch(Zend_Db_Adapter_Exception $e){
				echo $e->getMessage();
			}
		}
		
	/** Seta com exclu�do o menu passado por par�metro 
	 */
		public function excluir($pCdMenu){
			try{
				$lWhere = $this->getAdapter()->quoteInto("cdmenu= ?", $pCdMenu);
				$lDados = array (
					"fgativo" => '9'
				);
				parent::update($lDados, $lWhere);
			}catch(Zend_Db_Adapter_Exception $e){
				echo $e->getMessage();
			}
		}
		
	/** Monta menu
	 * @author Jo�o Pedro Grasselli
	 * @param string $pCdPerfil codigo do perfil
	 * @return void
	 * @version 01/05/2014
	 */		
    public function montaMenu($pCdPerfil){
           $select = $this->getAdapter()->select()
		   		   ->from(array('p' => 'security.zapp_perfil'), array())
				   ->joinInner(array('pr' => 'security.zapp_perfilrelacao'), 'p.cdperfil = pr.cdperfil', array())
                   ->joinInner(array('m' => 'security.zapp_modulos'), 'pr.cdmodulo = m.cdmodulo', array())
				   ->joinInner(array('mn' => 'security.zapp_menu'), 'm.cdmenu = mn.cdmenu', array('cdmenu', 'nmmenu'))
				   ->joinInner(array('a' => 'security.zapp_action'), 'm.cdmodulo = a.cdmodulo', array('cdaction', 'nmaction', 'nmtitulo'))
                   ->where('p.cdperfil = ?', $pCdPerfil)
                   ->where('m.fgativo <> 9')
                   ->order('mn.nmmenu ASC');
           $stmt = $this->getAdapter()->query($select);
           return $menu = $stmt ->fetchAll();
	}
}

