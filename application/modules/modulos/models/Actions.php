<?php

class Modulos_Model_Actions extends Zend_Db_Table{
		
	protected $_name = 'itr_action';
	protected $_primary = 'cdaction';

	/** 
	 * Lista as actions cadastrados
	 */
	public function listar($pCdmodulo = NULL){
		try {
			if($pCdmodulo != NULL){
				$where = $this->getAdapter()->quoteInto("cdmodulo = ?", $pCdmodulo);
				return $this->fetchAll($where, "nmaction") -> toArray();
			}else{
				return NULL; 
			}
		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados ".$e;
		}
	}
	
	/** Busca uma Action cadastrada
	  */	
	public function buscar($act = null){
		try {
			$where = '1';
			if(!empty($act)){
				$where = $this->getAdapter()->quoteInto("cdaction = ?", $act);					
			 }
			return $this->fetchRow($where)->toArray();
		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados: ".$e;
		}
	}

	/** 
	 * Adiciona nova action
	 */
	public function inserir(array $pAction){
		try{
			parent::insert($pAction);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}

	/** Editar Action
	 */		
	public function editar(Array $docs, $id){
		try{
			$where = $this->getAdapter()->quoteinto("cdaction=?",$id);
			$docs = self::colunas($docs);
			return  parent::update($docs,$where);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}
	
	/** Seta com exclu�do a action passada por par�metro 
	 */	
	public function excluir($pCdAction){
		try{
			$lWhere = $this->getAdapter()->quoteInto("cdaction = ?", $pCdAction);
			$this->delete($lWhere);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}

}

