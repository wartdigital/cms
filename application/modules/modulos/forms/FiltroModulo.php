<?php 

class Modulos_Form_FiltroModulo extends Zend_Form{
	
	public function init(){
		
		$busca = new Comum_Form_Element_Text('busca');
		$busca ->setRequired(false)
	           ->setDescription('Busca');
      	$this->addElement($busca);
      	
      	$Menus = new Modulos_Model_Menu();
      	$lListaMenus = $Menus -> listar();
      	$options[""] = "Menu"; 
      	$options["#"] = "Nenhum Menu"; 
      	foreach ($lListaMenus as $cdMenu => $conteudo){
      		$options[$conteudo[cdmenu]] = $conteudo[nmmenu];
      	}

      	$menu  = new Comum_Form_Element_Select('cdmenu');
      	$menu ->setRequired(false)
      				   ->setAttrib('title', 'Menu')
      				   ->setMultiOptions($options);
      	$this->addElement($menu);
      	
      	//Bot�o de Envio
		$submit = new Comum_Form_Element_Gravar('Buscar');
		$this->addElement($submit);
  }
}
?>