<?php
/** 
 	 * Formul�rio para adi��o e edi��o de m�dulos
	 * @author Wiliam Passaglia Castilhos 
	 * @version 30/09/2011
	 */
class Modulos_Form_InsereModulos extends Zend_Form{
	
	public function init(){

    	$this->setMethod('post');
	  	//Sigla
	    $siglamodulo = new Comum_Form_Element_Text('nmsigla');
		$siglamodulo->setRequired(true)
					->setDescription('Sigla');	
					
		//Modulo			 				
		$nomemodulo = new Comum_Form_Element_Text('nmmodulo');
		$nomemodulo	->setRequired(true)
					->setDescription('Nome'); 
					
		//Descri��o do m�dulo
		$descricaomodulo = new Comum_Form_Element_Text('dsmodulo');
		$descricaomodulo->setRequired(true)
						->setDescription('Descricao'); 

		$caminhomodulo = new Comum_Form_Element_Text('caminho');
		$caminhomodulo	->setRequired(true)
						->setDescription('Caminho');
	
    	$banco = new Modulos_Model_Menu();
    	$menu = $banco->listar();
    	
	    $option = array();
	    $options[''] = "Nenhum menu";
    	foreach($menu as $tipo){
    		$cd = $tipo['cdmenu'];
    		$nome = $tipo['nmmenu'];
    		$options[$cd]=$nome;
	    }	
				
  		//Menu do m�dulo
		$cdmenu = new Comum_Form_Element_Select('cdmenu');
	  	$cdmenu->setRequired(false)
			   ->setDescription('Menu do m�dulo')
	           ->setMultiOptions($options);

		$this->addElement($cdmenu);
		$this->addElement($nomemodulo);
		$this->addElement($siglamodulo);
	  	$this->addElement($descricaomodulo);
	  	$this->addElement($caminhomodulo);
	  	
	  	$ativo = new Zend_Form_Element_FgAtivo();
	 	$this->addElement($ativo);
	 	
	 	$submit = new Comum_Form_Element_Gravar('Gravar');
	 	$this->addElement($submit);

		//Bot�o Voltar
		$voltar = new Comum_Form_Element_Button('Voltar');
		$voltar -> setAttrib('OnClick', "window.location.href='/modulos/modulos/index'");
		$this->addElement($voltar);

		
		//CADASTRO DE ACTIONS
		$lCdmodulo = $this->getAttrib('cdmodulo');
		if($lCdmodulo != NULL){
			$lBdActions = new Modulos_Model_Actions;
			$actions = $lBdActions->listar($lCdmodulo);
		}else{
			$actions = NULL;
		}
		
		if(!empty($actions)){	
			
			foreach($actions as $action){
				//var_dump($action);
			//FGindex
			$fgindex = new Zend_Form_Element_Checkbox('fgindex'.$action[cdaction]);
			$fgindex	->setRequired(true)
						->setDecorators(array(
										   	array('ViewHelper'),
										   	array('Errors'),
										   	))
						->setCheckedValue('1')
						//->setValue('1')
						->class='campomenu';
			if($action[fgindex]=='1'){
				$fgindex ->setAttrib('checked','checked');
			}
			$this->addElement($fgindex);
			
			//Titulo
			$tituloaction = new Comum_Form_Element_Text('nmtitulo'.$action[cdaction]);
			$tituloaction	->setRequired(true)
		            		->setValue($action[nmtitulo]);
			$this->addElement($tituloaction);
				
			//Caminho
			$caminhoaction = new Comum_Form_Element_Text('nmaction'.$action[cdaction]);
			$caminhoaction	->setRequired(true)
		            		->setValue($action[nmaction]);
							
			$this->addElement($caminhoaction);
			
			$options = array(
			'1'=>'1',
			'2'=>'2',
			'3'=>'3',
			'4'=>'4'
			);
			
			$cdnivel = new Comum_Form_Element_Select('cdnivel'.$action[cdaction]);
		  	$cdnivel->setRequired(true)
		            ->setMultiOptions($options)
				    ->setValue($action[cdnivel]);
				   
			$this->addElement($cdnivel);
			}
		}
	}	
}
