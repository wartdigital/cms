<?php
/** 
 * Formul�rio para adi��o e edi��o de menus
 */
class Modulos_Form_InsereMenu extends Zend_Form{

    public function init(){
        $this->setMethod('post');
	 
		$nomemenu = new Comum_Form_Element_Text('nmmenu');
		$nomemenu ->setRequired(true)
				->setDescription('Nome do menu');	           
		$this->addElement($nomemenu);

		$hdcodmenu= new Comum_Form_Element_Hidden('cdmenu');
		$this->addElement($hdcodmenu);
		
		$submit = new Comum_Form_Element_Gravar('Gravar');;
		$this->addElement($submit);
		
		$voltar = new Comum_Form_Element_Button('Voltar');
		$voltar->setAttrib('OnClick',"window.location.href='/modulos/menu'");
		$this->addElement($voltar);

    }

}

