<?php

class Perfil_Form_InserePerfil extends Zend_Form{
	
	public  function init(){
		$this->setMethod('post');
		
		//Busca por todos os acessos
		$lBdAcessos = new Perfil_Model_Acessos;
    	$lListaAcessos = $lBdAcessos -> listar();

    	$lOptions = array();
    	$lOptions[''] = 'Acesso';
    	foreach($lListaAcessos as $lKey => $lAcesso){
	    	$lOptions[$lAcesso['cdacesso']] = $lAcesso['nmacesso'];
   		}
    	
		//Nome do Perfil
		$nomecert = new Comum_Form_Element_Text('nmperfil');
		$nomecert->setRequired(true)
				 ->setDescription('Nome');
      	$this->addElement($nomecert);
      	
      	//Acesso do Perfil
		$acesso = new Comum_Form_Element_Select('cdacesso');
		$acesso ->setRequired(true)
				->addMultiOptions($lOptions);
      	$this->addElement($acesso);

      	$ativo = new Zend_Form_Element_FgAtivo();
      	$this->addElement($ativo);
		
		//Bot�o gravar
		$submit = new Comum_Form_Element_Gravar('Gravar');
		$this->addElement($submit);

		//Bot�o Votar
		$voltar = new Comum_Form_Element_Button('Voltar');
		$voltar->setAttrib('OnClick',"window.location.href='/perfil/perfil'");
		$this->addElement($voltar);
	}
}

