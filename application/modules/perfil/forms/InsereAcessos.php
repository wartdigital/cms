<?php

class Perfil_Form_InsereAcessos extends Zend_Form
{
public  function init(){

	 $this->setMethod('post');
	 
	 $banco = new Perfil_Model_Acessos();
	 $acessos = $banco->listar();
	 
	 foreach($acessos as $cod){
	 	$perfis=$banco->buscar($cod[cdacesso]);
	 	$perfil = new Zend_Form_Element_Select('cdperfil'.$cod[cdacesso]);
	    $perfil	->addValidator(new Zend_Validate_StringLength(1,100))
				->setRequired(true)
	          	->setAttrib('title', "Perfil de acesso - Acesso ".$cod[nmacesso])
	         	->setDecorators(array(
				   	   		    array('ViewHelper'),
						   		array('Errors'),
						   		array('Label'),
						   		));
		$options[''] = "Perfil de acesso - Acesso ".$cod[nmacesso];
	 	$options[0] = "Sem Acesso";
		foreach($perfis as $dadosperfil){
	 		$cdperfil = $cod[cdacesso]."_".$dadosperfil['cdperfil'];
			$nmperfil = $dadosperfil['nmperfil'];
			$options[$cdperfil] = $nmperfil;
	 	} 
	 	$perfil->setMultiOptions($options)
			   ->class ='campomenu';

		$this->addElement($perfil);
	 	$options = "";
	 }
	 $submit = new Zend_Form_Element_Submit('Gravar');
	 $submit->class='botaopadrao';
	 
	 $submit->setDecorators(array(
						   	array('ViewHelper'),
						   	array('Errors'),
						   	array('HtmlTag', array('tag' => '&nbsp')),
						   	));
	$this->addElement($submit);

	$voltar = new Zend_Form_Element_Button('Voltar');
	$voltar->class='botaopadrao';
	$voltar->setAttrib('OnClick',"window.location.href='/pessoa/pessoa'");
	$voltar->setDecorators(array(
						   	array('ViewHelper'),
						   	array('Errors'),
						   	array('HtmlTag', array('tag' => '&nbsp')),
						   	));
	$this->addElement($voltar);
}
}

