<?php

class Perfil_Form_InserePerfilModulo extends Zend_Form{
	
 	public function init(){
 		$lCdPerfil = $this->getAttrib('cdperf');
 		$nm = $this->getAttrib('nmperf');
 		$fgedit = $this->getAttrib('fgedit');
	
		$lBdPerfilModulo = new Perfil_Model_PerfilModulo();
	
 		$nmsigla = new Comum_Form_Element_Select('cdmodulo');
 		$nmsigla->setRequired(true);
 		$submit = new Comum_Form_Element_Gravar('Gravar');
 		$nivel = new Comum_Form_Element_Select('cdnivel');
					 
		if (isset($fgedit)){
			$lCdModulo = $this->getAttrib('cdmod');	
			$nmsigla->setAttrib('readonly',true);
			$nivel->setRequired(true);
			
    		$lModulo = $lBdPerfilModulo->buscar($lCdPerfil, $lCdModulo);
		 	$options[$lCdModulo] = $lModulo['nmsigla'];
		}else{
	   		$perfilmodulo = $lBdPerfilModulo->naopossui($lCdPerfil);
			
	   		if ($perfilmodulo){
	   			$options = array();
	   			$options[''] = 'M�dulo';
	   			foreach($perfilmodulo as $tipo){
	   				$options[$tipo['codigo']]= $tipo['nmmodulo'];
				}
	   		}else{
	   			$options[''] = "N�o h� novos m�dulos para adicionar a este perfil";
				$nivel->setOptions(array('disable' => array('1','2','3','4')));
				$nmsigla->setAttrib('disabled', true);
	        	$nmsigla->setAttrib('readonly',true);
	        	$submit->setAttrib('disabled', true);
	        	$submit->setAttrib('readonly',true);	
	   		}
		}
		
   		// Selectbox dos Modulos
		$nmsigla ->setMultiOptions($options)
	        	 ->setAttrib('title', 'M�dulo');
		
	 	// Select dos  N�veis de acesso		
		$nivel	->setAttrib('title', 'Nivel de Permiss�o')
			 	->setRequired(true)
				->addMultiOptions(array(
					''=>'Nivel de Permiss�o',
 	     			'1'=>'1',
         			'2'=>'2',
    	 			'3'=>'3',
         			'4'=>'4'	     
				));

		$voltar = new Comum_Form_Element_Button('Voltar');
		$voltar->setAttrib("OnClick","window.location.href='/perfil/perfil-modulo/index/cdperf/".$lCdPerfil."'");
					
		$this->addElement($nmsigla);
		$this->addElement($nivel);
		$this->addElement($submit);
		$this->addElement($voltar);	
	}
}

