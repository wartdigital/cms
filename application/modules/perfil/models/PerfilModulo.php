<?php

class Perfil_Model_PerfilModulo extends Zend_Db_Table
{
    protected $_schema = 'security';
	protected $_name = "zapp_perfilrelacao";
	protected $_primary =array('cdperfil','cdmodulo');
	 	
	/** Lista os módulos de um Perfil
	  * @author Gustavo Pistore 
	  * @param -
	  * @return consulta banco
	  * @version 13/09/2011
	  */
	public function listar($cod){
		try { 
			$select = $this->getAdapter()->select()
				->from(array('a'=>'security.zapp_perfilrelacao'))
				->joinleft(array('b'=>'security.zapp_modulos'),'a.cdmodulo=b.cdmodulo')
				->joinleft(array('c'=>'security.zapp_menu'),'c.cdmenu=b.cdmenu')
				->where("b.fgativo <> '9' AND cdperfil=".$cod)
				->order("nmmenu ASC")
				->order("nmmodulo ASC");
			$stmt = $this->getAdapter()->query($select);
			$result = $stmt->fetchAll();	
			return $result;
		}catch (Zend_Db_Exception $e){
				echo "Erro na consulta ao banco de dados";
		}
	}
	 
	 /** Lista todos os m�dulos que determinado perfil nao possui
	  * @author Gustavo Pistore
	  * @param string $cod c�digo do perfil pesquisado, 
	  * @return array retorna todos os m�dulos que o perfil nao possui
	  * @version 13/09/2011
	  */	
	public function naopossui($cod){
		try { 
			$select = $this->getAdapter()->select()
				->from(array('a'=>'security.zapp_modulos'),array('a.cdmodulo as codigo','a.nmsigla','a.nmmodulo'))
			 	->joinLeft (array('b'=>new Zend_Db_Expr("(SELECT * FROM security.zapp_perfilrelacao WHERE cdperfil =".$cod.")")),'a.cdmodulo=b.cdmodulo')
				->where("fgativo <> '9' AND cdperfil is null ")
				->order("nmsigla");;
			$stmt = $this->getAdapter()->query($select);
			$result = $stmt->fetchAll();	
			return $result;
		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados: ".$e;
		}
	}

     /** Adiciona novo m�dulo a determinado Perfil
	  * @author Gustavo Pistore
	  * @param string docs dados a serem adicionado, 
	  * @return -
	  * @version 13/09/2011
	  */
	public function inserir(Array $docs){
		try{
			parent::insert($docs);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}	
		
	public function buscar($pCdPerfil, $pCdModulo){
		try {
			$select = $this->getAdapter()->select()
					->from(array('m'=>'security.zapp_modulos'))
					->joinInner(array('p'=>'security.zapp_perfilrelacao'), 'm.cdmodulo = p.cdmodulo AND p.cdperfil = '.$pCdPerfil)
					->where("m.fgativo <> '9' AND m.cdmodulo = ".$pCdModulo);
			$result = $this->getAdapter()->fetchrow($select);	
			return $result;
		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados:". $e;
		}
	}
	
	/** Edita nivel de permissao em determinado perfil
	  */
	public function editar($pCdNivel, $pCdModulo, $pCdPerfil){
		try{
			$lWhere = array('cdmodulo= ?' => $pCdModulo, 'cdperfil = ?' => $pCdPerfil);
			$this->getAdapter()->update('security.zapp_perfilrelacao',array('cdnivel' => $pCdNivel), $lWhere);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}
	
	/** Exclui um modulo de um Perfil
	 */
	public function excluir($pCdModulo, $pCdPerfil){
		$lWhere = array('cdmodulo= ?' => $pCdModulo, 'cdperfil = ?' => $pCdPerfil);
		$this->getAdapter()->delete('security.zapp_perfilrelacao', $lWhere);
	}

}

