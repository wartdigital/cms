<?php

class Perfil_Model_Acessos extends Zend_Db_Table
{
    protected $_schema = 'security';
	protected $_name = 'zapp_perfilusuario';
	protected $_primary =array('cdacesso','cdusuario');
	
	/** Lista todos os acessos cadastrados
	 * @author Jo�o Pedro Grasselli 
	 * @param 
	 * @return array
	 * @version 06/10/2011
	 */
		public function listar($todos = false)
		{
			try {
				$select = $this ->getAdapter()->select()
			             		->from(array('a'=> 'security.zapp_acesso'))
			            		->order(array ('nmacesso ASC'));
			    if($todos){
			    	$select->where('fgativo = 1');
			    }else{
			    	$select->where('fgativo <> 9');
			    }   		
				$stmt = $this->getAdapter()->query($select);
				$result = $stmt->fetchAll();	
				return $result;
			}catch (Zend_Db_Exception $e){
				die($e);
			}
		}
		
	/** Busca todos os perfis para um acesso
	 * @author Jo�o Pedro Grasselli 
	 * @param string $cdAcesso codigo do acesso
	 * @return array
	 * @version 06/10/2011
	 */
		public function buscar($cdAcesso)
		{
			try {
				$select = $this ->getAdapter()->select()
			             		->from(array('p'=> 'security.zapp_perfil'))
			             		->where('cdacesso = ?', $cdAcesso)
			            		->where('fgativo <> 9');
			            		//->order(array ('nmacesso ASC'));
				$stmt = $this->getAdapter()->query($select);
				$result = $stmt->fetchAll();	
				return $result;
			}catch (Zend_Db_Exception $e){
					echo "Erro na consulta ao banco de dados";
			}
		}
	
	/** Busca dados do acesso
	 * @author Jo�o Pedro Grasselli 
	 * @param string $cdAcesso codigo do acesso
	 * @return array
	 * @version 06/10/2011
	 */
		public function buscarAcesso($cdAcesso)
		{
			try {
				$select = $this ->getAdapter()->select()
			             		->from(array('p'=> 'security.zapp_acesso'))
			             		->where('cdacesso = ?', $cdAcesso)
			            		->where('fgativo <> 9');
			            		//->order(array ('nmacesso ASC'));
				$stmt = $this->getAdapter()->query($select);
				$result = $stmt->fetchAll();	
				return $result;
			}catch (Zend_Db_Exception $e){
					echo "Erro na consulta ao banco de dados";
			}
		}
		
	/** Busca todos os perfis de uma pessoa
	 * @author Jo�o Pedro Grasselli 
	 * @param string $cdusuario codigo do acesso
	 * @param string $cdAcesso -opcional- se informado, ir� buscar os perfis somente daquele acesso
	 * @return array
	 * @version 07/10/2011
	 */
		public function buscarPerfisPessoa($cdusuario, $cdAcesso=null)
		{
			try {
				$select = $this ->getAdapter()->select()
			             		->from(array('p'=> 'security.zapp_perfilusuario'))
			             		->joininner(array('perfil' => 'security.zapp_perfil'),
			                 		   			'p.cdperfil = perfil.cdperfil')
			             		->where('cdusuario = ?', $cdusuario);
			            		//->order(array ('nmacesso ASC'));
				if($cdAcesso){
		        	$select->where('perfil.cdacesso = ?', $cdAcesso);
		        }
			             		
				$stmt = $this->getAdapter()->query($select);
				$result = $stmt->fetchAll();	
				return $result;
			}catch (Zend_Db_Exception $e){
					echo "Erro na consulta ao banco de dados";
			}
		}
		
	/** Insere um perfil de acesso para uma pessoa
	 * @author Jo�o Pedro Grasselli 
	 * @param string $cdPerfil codigo do perfil
	 * @param string $cdusuario codigo da pessoa
	 * @param string $cdAcesso codigo da acesso
	 * @return array
	 * @version 06/10/2011
	 */
		public function inserir($cdPerfil, $cdusuario, $cdAcesso)
		{
		  if($cdPerfil!="0"){
			try {
				$dados = array(
				'cdperfil'=>$cdPerfil,
				'cdusuario'=>$cdusuario,
				'cdacesso'=>$cdAcesso,
				);
				parent::insert($dados);	
			}catch (Zend_Db_Exception $e){
					echo "Erro na função inserir do model de Acessos".$e;
			}
		  }
		}
		
	/** Atualiza os dados de um perfil para uma pessoa
	 * @author Jo�o Pedro Grasselli 
	 * @param array [cdusuario] codigo da pessoa 
	 * 		  array [acessos] acessos para a pessoa, onde:
	 * 						chave � o c�digo do acesso
	 * 						valor � o c�digo do perfil 
	 *			Ex: array(2) { ["cdusuario"]=> string(1) "4" ["acessos"]=> array(1) { [1]=> string(1) "4" } }
	 *			Caso o c�digo do perfil seja "" (vazio) o perfil naquele acesso ser� exclu�do
	 * @return array
	 * @version 07/10/2011
	 * 			modificado em 17/04/2012 - Wiliam Passaglia Castilhos - Deletando os acessos da pessoa antes de inserir
	 * 			modificado em 19/04/2012 - Jo�o Pedro Grasselli - A ultima altera��o causou erros na grava��o de perfil da matr�cula, portanto o script foi alterado
	 */
		public function atualiza($infoAcesso)
		{
			$cdusuario = $infoAcesso['cdusuario'];
			$acessos_novos = $infoAcesso['acessos'];
			
			//busco todos os perfis que a pessoa ja possui
			$select = $this ->getAdapter()->select()
			             		->from(array('p'=> 'security.zapp_perfilusuario'))
			             		->where('cdusuario = ?', $cdusuario);
			$stmt = $this->getAdapter()->query($select);
			$result = $stmt->fetchAll();
			
			//organizo os acesso que a pessoa possui
			$acessos = array();
			foreach($result as $acesso){
					$acessos[$acesso[cdacesso]] = $acesso[cdperfil];  
			}
			
			$cdacessos = array_keys($acessos_novos);
			for($i=0; $i<sizeof($acessos_novos); $i++){	
				$acesso = current($acessos_novos);
				next($acessos_novos);

				$cdacesso = $cdacessos[$i];
				
				if($acesso != ""){
					$acessos_gravar[$cdacesso] = $acesso;
				}else{
					$acessos_excluidos[] = $cdacesso; 
				}
			}
			

			
			//separo todos os acessos que a pessoa possui e que não foram dados agora
			$acessos_antigos = array_diff_key($acessos, $acessos_gravar);
			

			
			$cdacessos = array_keys($acessos_antigos);
			for($i=0; $i<sizeof($acessos_antigos); $i++){	
				$perfil = current($acessos_antigos);
				next($acessos_antigos);
				
				$cdacesso = $cdacessos[$i];
				
				if(!in_array($cdacesso, $acessos_excluidos)){
					$acessos_gravar[$cdacesso] = $perfil;

				}
			}
			
			//deletando todos os perfis da pessoa
			$where = array();
			$tabela = new Zend_Db_Table('security.zapp_perfilusuario');
			$where[] = $tabela->getAdapter()->quoteInto('security.zapp_perfilusuario.cdusuario = ?',$cdusuario);
			$tabela->delete($where);
		
			$cdacessos = array_keys($acessos_gravar);
			for($i=0; $i<sizeof($acessos_gravar); $i++){	
				$perfil = current($acessos_gravar);
				next($acessos_gravar);
				
				$cdacesso = $cdacessos[$i];
				
				$insert = array('cdusuario'=>$cdusuario,
								'cdperfil'=>$perfil,
								'cdacesso'=>$cdacesso
								);
								
				$tabela -> insert($insert);
			}
	}
}