<?php

class Perfil_Model_Perfil extends Zend_Db_Table {

    protected $_schema = 'security';
	protected $_name = 'zapp_perfil';
	protected $_primary = 'cdperfil';
	
	
	/** Lista os tipos de Perfil
	  */
	public function listar(){
		try {
			$where = "fgativo <>  9";	
			return $this->fetchAll($where,"nmperfil")->toArray();
		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados!";	
		}
	}
	
	
	/** Busca um Perfil
	*/
	public function buscar($lCdPerfil){
		try {
			if(!empty($lCdPerfil)){
				$lWhere = $this->getAdapter()->quoteInto("cdperfil = ?", $lCdPerfil);					
			 }
			return $this->fetchRow($lWhere)->toArray();
		}catch (Zend_Db_Exception $e){
				echo "Erro na consulta ao banco de dados";
		}
	}
	
	/** Adiciona novo Perfil
	  */
	public function inserir($lPerfil){
		try{
			parent::insert($lPerfil);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}
		
	/** Adiciona novo Perfil
	  */
	public function editar($lPerfil, $lCdPerfil){
		try{
			$lWhere = $this->getAdapter()->quoteinto("cdperfil = ?", $lCdPerfil);
			return  parent::update($lPerfil, $lWhere);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}
	
	/** Exclui os dados de um Perfil
	 */
	public function excluir($lCdPerfil){
		try{
			$lWhere = $this->getAdapter()->quoteInto("cdperfil= ?", $lCdPerfil);
			$lUpdate = array ("fgativo" => '9');
			parent::update($lUpdate, $lWhere);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}

}

