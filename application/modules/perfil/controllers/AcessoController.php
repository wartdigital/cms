<?php

class Perfil_AcessoController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }
	/** Lista todas os perfis de uma pessoa 
	 * @author Jo�o Pedro Grasselli 
	 * @param void
	 * @return void
	 * @version 07/10/2011
	 */
	public function indexAction() {
		$helper = $this->_helper->getHelper('Nomemodulo');
        $controller = $helper->Nomemodulo();
	    $this->view->titulo = $controller;
	    $cdpessoa = $this->getRequest();
		$bancopessoa = new Pessoa_Model_Pessoa();
		$pessoa = $bancopessoa->buscar($cdpessoa->cdpessoa);
		$this->view->subtitulo = $pessoa[nmpessoa];
	    
	    $cdpessoa = $cdpessoa->cdpessoa;
	    $form = new Perfil_Form_InsereAcessos();
	    $banco = new Perfil_Model_Acessos();
		$pessoa = $banco ->buscarPerfisPessoa($cdpessoa);
		foreach($pessoa as $perfil){
			$popular = array(
			'cdperfil'.$perfil[cdacesso]=>$perfil[cdperfil]
			);
			$form->populate($popular);
		}
		
		//$form->populate($pessoa);
		$this->view->form = $form;
	}
	/** Action que adiciona um novo perfil para uma pessoa  
	 * @author Jo�o Pedro Grasselli
	 * @param void
	 * @return void
	 * @version 07/10/2011
	 */
	public function addAction() {
	}
	
	/** Action que edita um perfil de uma pessoa  
	 * @author Jo�o Pedro Grasselli
	 * @param void
	 * @return void
	 * @version 07/10/2011
	 */
	public function editAction() {
		$this->getHelper ( 'viewRenderer' )->setRender(cadastro);
		$helper = $this->_helper->getHelper('Nomemodulo');
        $controller = $helper->Nomemodulo();
        
	    $this->view->titulo = $controller;
	    $this->view->cabecalho = "Altera��o";
	    
	    $cdpessoa = $this->getRequest();
	   	$cdpessoa = $cdpessoa->cdpessoa;
	   	
	   	$bancopessoa = new Pessoa_Model_Pessoa();
		$pessoa = $bancopessoa->buscar($cdpessoa);
		
		$this->view->subtitulo = $pessoa[nmpessoa];
	    
		$form = new Perfil_Form_InsereAcessos();
	    
		$banco = new Perfil_Model_Acessos();
		
		$pessoa = $banco ->buscarPerfisPessoa($cdpessoa);
		//var_dump($pessoa);
		foreach($pessoa as $perfil){
			$populate = array(
			'cdperfil'.$perfil[cdacesso]=>$perfil[cdacesso]."_".$perfil[cdperfil]
			);
			$acesso[$perfil[cdacesso]]=$perfil[cdperfil];
			$form->populate($populate);
		}
		$this->view->form = $form;
	
		if ($this->getRequest()->isPost()) {
				$values = $this->getRequest()->getPost();
				foreach($values as $valor){
					if($valor AND $valor!="Gravar"){
						$cdacesso = substr($valor, 0, strpos($valor, '_'));
						$cdperfil = substr($valor, strpos($valor, '_')+1, strlen($valor));

						$banco->atualiza($cdperfil, $cdpessoa, $cdacesso);
					}
				}

		//$helper = $this->_helper->getHelper('Geralog');
		//$helper -> Geralog($this->_request->getControllerName(), $this->_request->getActionName(), $cdpessoa);
		return $this->_redirect('/pessoa/pessoa/index');
		}
	}
}





