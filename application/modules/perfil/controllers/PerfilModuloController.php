<?php

class Perfil_PerfilModuloController extends Zend_Controller_Action{

    public function init(){}
	
	 /** Action da Listagem dos m�dulos de um Perfil
	 */
    public function indexAction(){
    	$lCdPerfil = $this->getRequest()->cdperf;
		
		$lBdPerfil = new Perfil_Model_Perfil();
		$lPerfil = $lBdPerfil->buscar($lCdPerfil);	
		
		$lBdPerfilModulo = new Perfil_Model_PerfilModulo();
		$lListaModulos = $lBdPerfilModulo->listar($lCdPerfil);

		$this->view->pPerfil = $lPerfil;
    	$this->view->pModulos = $lListaModulos;	
    }
	
	/** Action da Adi��o dos m�dulos de um Perfil
	 */
    public function addAction(){
		$this->getHelper('viewRenderer')->setRender(cadastro);
		
    	$lCdPerfil = $this->getRequest()->cdperf;

    	$lBdPerfil = new Perfil_Model_Perfil();
    	$lPerfil = $lBdPerfil->buscar($lCdPerfil);
    	$this->view->pPerfil = $lPerfil;

		$lForm = new Perfil_Form_InserePerfilModulo(array('cdperf' => $lCdPerfil));
    	$this->view->form = $lForm;
		
    	$this->view->cabecalho = "Inclus�o";
    	
    	if($this->getRequest()->isPost()){
			$lPost = $this -> getRequest() -> getPost();
			
			$lPerfilModulo['cdperfil'] = $lCdPerfil;
			$lPerfilModulo['cdmodulo'] = $lPost['cdmodulo'];
			$lPerfilModulo['cdnivel'] = $lPost['cdnivel'];
			
			$lBdPerfilModulo = new Perfil_Model_PerfilModulo();
			$lBdPerfilModulo -> inserir($lPerfilModulo);
			
			$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
			$manager = $bootstrap->getResource('cachemanager');
			$cache = $manager->getCache('acl');
			$cache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
		 	return $this->_redirect('/perfil/perfil-modulo/index/cdperf/'.$lCdPerfil);
    	}
    }
	
    /** Action da Edi��o dos m�dulos de um Perfil
	 */
    public function editAction(){
 		$this->getHelper('viewRenderer')->setRender('cadastro');
		
  		$lCdPerfil = $this->getRequest()->cdperf;
		$lCdModulo = $this->getRequest()->cdmodulo;
		
    	$lBdPerfil = new Perfil_Model_Perfil();
    	$lPerfil = $lBdPerfil->buscar($lCdPerfil);
    	$this->view->pPerfil = $lPerfil;

		$lBdPerfilModulo = new Perfil_Model_PerfilModulo();
    	$lPerfilModulo = $lBdPerfilModulo -> buscar($lCdPerfil, $lCdModulo);
		
		$lForm= new Perfil_Form_InserePerfilModulo(array('cdperf' => $lCdPerfil,'cdmod'=>$lCdModulo, 'fgedit'=>'1'));
		$lForm->populate($lPerfilModulo);
    	$this -> view -> form = $lForm;
		
    	$this->view->cabecalho = "Altera��o";
    	
	    if($this->getRequest()->isPost()){
			$lPost = $this -> getRequest() -> getPost();
			
			$lBdPerfilModulo->editar($lPost['cdnivel'], $lCdModulo, $lCdPerfil);
			
			$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
			$manager = $bootstrap->getResource('cachemanager');
			$cache = $manager->getCache('acl');
			$cache->clean(Zend_Cache::CLEANING_MODE_ALL);
			
	    	return $this->_redirect('/perfil/perfil-modulo/index/cdperf/'.$lCdPerfil);	
	    }
    }
	
	/** Action da Exclus�o dos m�dulos de um Perfil
	 */
    public function deleteAction(){
		$lBdPerfilModulo = new Perfil_Model_PerfilModulo();
		
    	$lCdPerfil = $this->getRequest()->cdperf;
		$lCdModulo = $this->getRequest()->cdmodulo;
		
    	if($lCdPerfil != "" AND $lCdModulo != ""){
    		$lBdPerfilModulo -> excluir($lCdModulo, $lCdPerfil);
    	}
  		return $this->_redirect('/perfil/perfil-modulo/index/cdperf/'.$lCdPerfil);  

}
}