<?php

class Perfil_PerfilController extends Zend_Controller_Action{

    public function init(){}
	
     /** 
	  * Action da Listagem do Perfil
	  */
    public function indexAction(){
    	$lBdPerfil = new Perfil_Model_Perfil();
    	$lListaPerfis = $lBdPerfil->listar();
    	$this->view->perfil = $lListaPerfis;
    }

     /** Action de Adi��o de Perfil
	 */
    public function addAction(){
    	$this->getHelper('viewRenderer')->setRender('cadastro');
		
    	$lForm = new Perfil_Form_InserePerfil();
		$this->view->formulario = $lForm;
		
    	$this->view->cabecalho = "Inclus�o";
    	
    	if($this->getRequest()->isPost()){
			$lPost = $this->getRequest()->getPost();
			
			$lBdPerfil = new Perfil_Model_Perfil();
			
			$lPerfil['cdacesso']	= $lPost['cdacesso'];
			$lPerfil['fgativo']		= $lPost['fgativo'];
			$lPerfil['nmperfil']	= $lPost['nmperfil'];
			
			$lBdPerfil->inserir($lPerfil);
			
			return $this->_redirect('/perfil/perfil/index');		
    	}
    }

     /** Action da Edi��o de Perfil
	 */
    public function editAction(){
    	$this->getHelper('viewRenderer')->setRender('cadastro');

		$lCdPerfil = $this->getRequest()->cd;
    	$lBdPerfil = new Perfil_Model_Perfil();
		$lPerfil = $lBdPerfil->buscar($lCdPerfil);
		
    	$lForm = new Perfil_Form_InserePerfil();
		$lForm->populate($lPerfil);
		
    	$this->view->formulario = $lForm;
    	$this->view->cabecalho = "Altera��o";
    	
	    if($this->getRequest()->isPost()){
			$lPost = $this->getRequest()->getPost();
			
			$lPerfil['cdacesso']	= $lPost['cdacesso'];
			$lPerfil['fgativo']		= $lPost['fgativo'];
			$lPerfil['nmperfil']	= $lPost['nmperfil'];
			
			$lBdPerfil->editar($lPerfil, $lCdPerfil);
			return $this->_redirect('/perfil/perfil/index');
	    }
    }

     /** 
	  *  Action de exclus�o de Perfil 
	  */
    public function deleteAction(){
        $lBdPerfil = new Perfil_Model_Perfil();
    	$lCdPerfil = $this->getRequest()->cd;
		
    	if($lCdPerfil != ""){
    		$lBdPerfil->excluir($lCdPerfil);
	    	return $this->_redirect('/perfil/perfil/index');
    	}
	}


}







