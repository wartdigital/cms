<?php
/** Formulario de Estado
 * @author Jo�o Pedro Grasselli
 * @param 
 * @return 
 * @version 10/06/2013
 */
class Default_Form_InsereEstado extends Zend_Form{

	public  function init(){
		$this->setMethod('post');
		
		//Nome
		$nmcomb = new Comum_Form_Element_Text('nmestado');
		$nmcomb  ->setRequired(true)
	             ->setDescription('Nome');
      	$this->addElement($nmcomb);
      	
      	//Nome
		$nmcomb = new Comum_Form_Element_Text('nmsigla');
		$nmcomb  ->setRequired(true)
				 ->setAttrib('maxlength', '2')
	             ->setDescription('Sigla');
      	$this->addElement($nmcomb);

	 	//FgAtivo
		$fgativo = new Zend_Form_Element_FgAtivo('fgativo');
      	$this->addElement($fgativo);

	 	
		// Bot�o de grava��o
		$submit = new Comum_Form_Element_Gravar('Gravar');
		$this->addElement($submit);
		
		//Bot�o Voltar
		$voltar = new Comum_Form_Element_Button('Voltar');
		$voltar->setAttrib('OnClick',"window.location.href='/default/estados/index'");
		$this->addElement($voltar);
	}
	
	public function popFromModel(Default_Model_Estado $pEstado){
		try{
			if($pEstado != NULL){
				$this->nmestado	-> setValue($pEstado -> getNmestado());
				$this->nmsigla 	-> setValue($pEstado -> getNmsigla());
				$this->fgativo	-> setValue($pEstado -> getFgativo());
			}
		}catch(Zend_Exception $ze){
			die($ze);
		}
	}
}
?>