<?php 

class Default_Form_Suporte extends Zend_Form{

	public function init(){
		$this->setMethod('post');
		
		if(Zend_Auth::getInstance()->getIdentity()){
			$nmpessoa = new W3_Form_Element_Hidden('nmpessoa');
			$nmpessoa -> setvalue(Zend_Auth::getInstance()->getIdentity()->nmpessoa);
			$this->addElement($nmpessoa);
					  
			$nmemail = new W3_Form_Element_Hidden('nmemail');
			$nmemail -> setvalue(Zend_Auth::getInstance()->getIdentity()->nmemail);
			$this->addElement($nmemail);	
		}else{
			$nmpessoa = new W3_Form_Element_Text('nmpessoa');
			$nmpessoa ->setRequired(true)
		 			  ->setDescription('Nome');
	    	$this->addElement($nmpessoa);
	    	
	    	$nmemail = new W3_Form_Element_Text('nmemail');
			$nmemail ->setRequired(true)
		 			 ->setDescription('Email');
	    	$this->addElement($nmemail);
		}
		
		//T�tulo da mensagem
		$titulo = new W3_Form_Element_Text('nmtitulo');
		$titulo ->setRequired(true)
		 	    ->setDescription('T�tulo');
	    $this->addElement($titulo);
	    
	   	//Mensagem
	    $mensagem = new W3_Form_Element_Textarea('dsmensagem');
		$mensagem ->setRequired(true)
				  ->setAttrib('rows', '5')		
				  ->setDescription("Mensagem");
	    $this->addElement($mensagem);
	    
	     //Bot�o Gravar
		$submit = new W3_Form_Element_Submit('Enviar');
		$this->addElement($submit);
		
		//Bot�o Voltar
		$voltar = new W3_Form_Element_RedButton('Fechar');
		$voltar->setAttrib('OnClick',"self.close()");
		$this->addElement($voltar);
	}
}

?>