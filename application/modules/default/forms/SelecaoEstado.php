<?php
/** Formulario de sele��o de estados
 * @author Jo�o Pedro Grasselli
 * @param 
 * @return 
 * @version 10/06/2013
 */
class Default_Form_SelecaoEstado extends Zend_Form{
	
	protected $lListaEstado;
	
	public function Default_Form_SelecaoEstado(Default_Model_ListaEstado $pListaEstado){
		$this -> lListaEstado = $pListaEstado;
		self::init();
	}

	public  function init(){
		$this->setMethod('post');
		
		if($this -> lListaEstado != NULL){
			$lListaEstados = $this -> lListaEstado;
		}else{
			$lBdEstado = new Default_Model_EstadosMapper();
			$lListaEstados = $lBdEstado -> listar();
		}
		
		$lOptionsEstado = array('' => 'Selecione');
		for($li =0; $li < $lListaEstados -> getTam(); $li++){
			$lEstado = $lListaEstados -> get($li);
			if($lEstado -> getFgativo() == 1){
				$lOptionsEstado[$lEstado->getCdestado()] = $lEstado->getNmestado();
			}
		}

      	//Nome
		$cdestado = new Comum_Form_Element_Select('cdestado');
		$cdestado->setRequired(true)
				 ->setMultiOptions($lOptionsEstado)
	             ->setDescription('Estado');
      	$this->addElement($cdestado);
	}
	
	public function popFromModel(Default_Model_Estado $pEstado){
		try{
			if($pEstado != NULL){
				$this->cdestado	-> setValue($pEstado -> getCdestado());
			}
		}catch(Zend_Exception $ze){
			die($ze);
		}
	}
}
?>