<?php
/** Formulario de sele��o de estados
 * @author Jo�o Pedro Grasselli
 * @param 
 * @return 
 * @version 10/06/2013
 */
class Default_Form_SelecaoPais extends Zend_Form{

	public  function init(){
		$this->setMethod('post');
		
		$lBdPais = new Default_Model_PaisesMapper();
		$lListaPaises = $lBdPais -> listar();
		
		$lOptionsPaises = array('' => 'Selecione');
		for($li =0; $li < $lListaPaises -> getTam(); $li++){
			$lPais = $lListaPaises -> get($li);
			if($lPais -> getFgativo() == 1){
				$lOptionsPaises[$lPais->getCdPais()] = $lPais->getNmPais();
			}
		}

      	//Nome
		$cdpais = new Comum_Form_Element_Select('cdpais');
		$cdpais  ->setRequired(true)
				 ->setMultiOptions($lOptionsPaises)
	             ->setDescription('Pa�s');
      	$this->addElement($cdpais);
	}
	
	public function popFromModel(Default_Model_Pais $pPais){
		try{
			if($pPais != NULL){
				$this->cdpais	-> setValue($pPais -> getCdPais());
			}
		}catch(Zend_Exception $ze){
			die($ze);
		}
	}
}
?>