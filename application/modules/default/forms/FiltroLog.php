<?php
	/**
 	 * Listagem de log
	 * @author Cristiano Felippetti
	 * @version 26/01/2011
	 */
class Default_Form_FiltroLog extends Zend_Form{

	    public function init()
	    {	
		
        $this->setMethod('post');

        $pessoa = new Pessoa_Model_Pessoa();
		$listapessoa = $pessoa->listar();
		
		$options = array();
		$options[] = "Pessoa";
		/**
	 	 * La�o que coloca todas as pessoas em uma vari�vel
	 	 * que ser� usada para compor o combo se sele��o
	 	 */
		foreach($listapessoa as $pessoaArray){
				$cdpessoa = $pessoaArray['cdpessoa'];
				$nmpessoa = $pessoaArray['nmpessoa'];
				$options[$cdpessoa] = $nmpessoa;
		}
		// Criar campo de sele��o de pessoas
		$campoNome = new Zend_Form_Element_Select("cdpessoa");
		$campoNome->addValidator(new Zend_Validate_StringLength(1,100))
       				->setRequired(false)
       				->setAttrib('title', 'Pessoa')        				
       				->setMultiOptions($options)
					->setDecorators(array(
						   		    array('ViewHelper'),
						   		    array('Errors'),						   		
						   		    ))
       				->class='campomenu';		
		$this->addElement($campoNome);
		
		// Montagem da lista de modules
		$default_log = new Default_Model_Log();
		$listamodules = $default_log->listarModules(); 		
		$options = array();
		$options[] = "Module";
		/**
	 	 * La�o que coloca todos os models em uma vari�vel
	 	 * que ser� usada para compor o combo se sele��o
	 	 */
		foreach($listamodules as $moduleArray){
				$nmmodule = $moduleArray['nmmodule'];
				$options[$nmmodule] = $nmmodule;
		}
		
		// Criar campo de sele��o de modules
		$campoModule = new Zend_Form_Element_Select("nmmodule");
        $campoModule->addValidator(new Zend_Validate_StringLength(1,100))
       				->setRequired(false)
       				->setAttrib('title', 'Module')
       				->setMultiOptions($options)
					->setDecorators(array(
						   		    array('ViewHelper'),
						   		    array('Errors'),
						   		    ))
       				->class='campomenu';		
		$this->addElement($campoModule);
		
		// Montagem da lista de controller
		$listacontrollers = $default_log->listarController(); 		
		$options = array();
		$options[] = "Controller";
		//La�o que coloca todos os controllers em uma vari�vel
	 	//que ser� usada para compor o combo se sele��o

		foreach($listacontrollers as $controllerArray){
				$nmcontroller = $controllerArray['nmcontroller'];
				$options[$nmcontroller] = $nmcontroller;
		}
		// Criar campo de sele��o de modules
		$campoController = new Zend_Form_Element_Select("nmcontroller");
        $campoController->addValidator(new Zend_Validate_StringLength(1,100))
       				->setRequired(false)
       				->setAttrib('title', 'Controller')
       				->setMultiOptions($options)
					->setDecorators(array(
						   		    array('ViewHelper'),
						   		    array('Errors'),
						   		    ))
       				->class='campomenu';		
		$this->addElement($campoController);
		
		// Montagem da lista de actions
		$listaactions = $default_log->listarAction(); 		
		$options = array();
		$options[] = "Action";
	 //La�o que coloca todas as actions em uma vari�vel
	 //que ser� usada para compor o combo se sele��o
	
		foreach($listaactions as $actionArray){
				$nmaction = $actionArray['nmaction'];
				$options[$nmaction] = $nmaction;
		}
		// Criar campo de sele��o de action
		$campoAction = new Zend_Form_Element_Select("nmaction");
		$campoAction->addValidator(new Zend_Validate_StringLength(1,100))
       				->setRequired(false)
       				->setAttrib('title', 'Action')
       				->setMultiOptions($options)
					->setDecorators(array(
						   		    array('ViewHelper'),
						   		    array('Errors'),					
						   		    ))
       				->class='campomenu';
        $this->addElement($campoAction);      	

         
        $datainicio = new Zend_Form_Element_Text('dtinicio');
        $datainicio->addValidator(new Zend_Validate_StringLength(1,10))
       				->setRequired(false)
       				->setLabel('Data inicial:')
       				->setAttrib('data','data')
       				->setAttrib('alt', 'Data inicial')
       				->setAttrib('title', 'Data inicial')
       				->setValue('Data inicial')
       				->setAttrib('style', 'width:75%')
					->setDecorators(array(
						   		    array('ViewHelper'),
						   		    array('Errors'),						   		
						   		    ))
       				->class='campomenu';
       	$this->addElement($datainicio);
       	
       	$datafim = new Zend_Form_Element_Text('dtfim');
        $datafim->addValidator(new Zend_Validate_StringLength(1,10))
       				->setRequired(false)
       				->setLabel('Data final:')
       				->setAttrib('data','data')
       				->setAttrib('alt', 'Data final')
       				->setAttrib('title', 'Data final')
       				->setValue('Data final')
       				->setAttrib('style', 'width:76.5%')
					->setDecorators(array(
						   		    array('ViewHelper'),
						   		    array('Errors'),						   		
						   		    ))
       				->class='campomenu';
       	$this->addElement($datafim);

	
	$submit = new Zend_Form_Element_Submit('Buscar');
	$submit->class='botaopadrao';	
	$submit->setDecorators(	array(
						   	array('ViewHelper'),
						   	array('Errors'),
						   	array('HtmlTag', array('tag' => '&nbsp')),
						   ));
	$this->addElement($submit);

    }
}
