<?php
/** Formulario de sele��o de cidades
 * @author Jo�o Pedro Grasselli
 * @param 
 * @return 
 * @version 10/06/2013
 */
class Default_Form_SelecaoCidade extends Zend_Form{
	
	protected $lListaCidade;
	
	public function Default_Form_SelecaoCidade(Default_Model_ListaCidade $pListaCidade){
		$this -> lListaCidade = $pListaCidade;
		self::init();
	}

	public  function init(){
		$this->setMethod('post');
		
		if($this -> lListaCidade != NULL){
			$lListaCidades = $this -> lListaCidade;
		}else{
			$lBdCidade = new Default_Model_CidadesMapper();
			$lListaCidades = $lBdCidade -> listar();
		}
		
		$lOptionsCidade = array('' => 'Selecione');
		for($li =0; $li < $lListaCidades -> getTam(); $li++){
			$lCidade = $lListaCidades -> get($li);
			if($lCidade -> getFgativo() == 1){
				$lOptionsCidade[$lCidade->getCdcidade()] = $lCidade->getNmcidade();
			}
		}

      	//Nome
		$cdestado = new Comum_Form_Element_Select('cdcidade');
		$cdestado->setRequired(true)
				 ->setMultiOptions($lOptionsCidade)
	             ->setDescription('Cidade');
      	$this->addElement($cdestado);
	}
	
	public function popFromModel(Default_Model_Cidade $pCidade){
		try{
			if($pCidade != NULL){
				$this->cdcidade	-> setValue($pCidade -> getCdcidade());
			}
		}catch(Zend_Exception $ze){
			die($ze);
		}
	}
}
?>