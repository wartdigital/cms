<?php
/** Formulario de Cidade
 * @author Jo�o Pedro Grasselli
 * @param 
 * @return 
 * @version 10/06/2013
 */
class Default_Form_InsereCidade extends Zend_Form{

	public  function init(){
		$this->setMethod('post');
		
		//Nome
		$nmcomb = new Comum_Form_Element_Text('nmcidade');
		$nmcomb  ->setRequired(true)
	             ->setDescription('Nome');
      	$this->addElement($nmcomb);

	 	//FgAtivo
		$fgativo = new Zend_Form_Element_FgAtivo('fgativo');
      	$this->addElement($fgativo);

	 	
		// Bot�o de grava��o
		$submit = new Comum_Form_Element_Gravar('Gravar');
		$this->addElement($submit);
		
		//Bot�o Voltar
		$voltar = new Comum_Form_Element_Button('Voltar');
		$voltar->setAttrib('OnClick',"window.location.href='/default/cidades/index'");
		$this->addElement($voltar);
	}
	
	public function popFromModel(Default_Model_Cidade $pCidade){
		try{
			if($pCidade != NULL){
				$this->nmcidade	-> setValue($pCidade -> getNmCidade());
				$this->fgativo	-> setValue($pCidade -> getFgativo());
			}
		}catch(Zend_Exception $ze){
			die($ze);
		}
	}
}
?>