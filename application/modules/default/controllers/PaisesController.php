<?php 

class Default_PaisesController extends Zend_Controller_Action{
	
	/** Filtro para busca de um combust�vel
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function indexAction(){	    
		$lBdPaises = new Default_Model_PaisesMapper();
		$lListaPaises = $lBdPaises -> listar();
		$this->view -> pListaPaises = $lListaPaises;
	}
	
	/** Adi��o de um combust�vel
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function addAction(){
		$this->getHelper('viewRenderer')->setRender(cadastro);
		
		$lForm = new Default_Form_InserePais();
		$this->view -> pForm = $lForm;
		
		$this -> view -> cabecalho = 'Inserir';
		
		if($this->getRequest()->isPost()){
			$lPost = $this->getRequest()->getPost();
			
			$lMdPais = new Default_Model_Pais();
 			$lMdPais -> setNmPais($lPost['nmpais']);
 			$lMdPais -> setNmSigla($lPost['nmsigla']);
 			$lMdPais -> setFgativo($lPost['fgativo']);
		
			$lBdPais = new Default_Model_PaisesMapper();
			$lBdPais -> inserir($lMdPais);
			return $this->_redirect('/default/paises/index');
		}
		
	}
	
 	/** Edi��o de um combust�vel
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function editAction(){
		$this->getHelper('viewRenderer')->setRender(cadastro);

		$this -> view -> cabecalho = 'Alterar';
		
		$lBdPais = new Default_Model_PaisesMapper();
		$lMdPais = new Default_Model_Pais();
		$lMdPais -> setCdPais($this->getRequest()->cd);
		
		if($this->getRequest()->isPost()){
			$lPost = $this->getRequest()->getPost();
			
 			$lMdPais -> setNmPais($lPost['nmpais']);
 			$lMdPais -> setNmSigla($lPost['nmsigla']);
 			$lMdPais -> setFgativo($lPost['fgativo']);
			
			$lBdPais -> alterar($lMdPais);
			
			return $this->_redirect('/default/paises/index');
		}else{
			$lMdPais = $lBdPais -> buscar($lMdPais);
		}
		
		$lForm = new Default_Form_InserePais();
		$lForm -> popFromModel($lMdPais);
		$this->view -> pForm = $lForm;
		
	}
	
	/** Exclus�o de um pa�s
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function deleteAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

    	$lMdPais = new Default_Model_Pais();
		$lMdPais -> setCdPais($this->getRequest()->cd);
		
		$lBdPais = new Default_Model_PaisesMapper();
		$lMdPais = $lBdPais -> buscar($lMdPais); 
    	$lBdPais -> excluir($lMdPais); 
    	
    	return $this->_redirect('/default/paises/index');
    }
    
    
}
?>