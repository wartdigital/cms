<?php 

class Default_CidadesController extends Zend_Controller_Action{
	
	/** Filtro para busca de um combustível
	 * @author João Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function indexAction(){	    
		$lBdCidades = new Default_Model_CidadesMapper();
		$lListaCidades = $lBdCidades -> listar();
		$this->view -> pListaCidades = $lListaCidades;
		
	}
	
	/** Adição de um combustível
	 * @author João Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function addAction(){
		$this->getHelper('viewRenderer')->setRender('cadastro');
		
		$lForm = new Default_Form_InsereCidade();
		$this->view -> pForm = $lForm;
		
		$lFormPais = new Default_Form_SelecaoPais();
		$this->view -> pFormPais = $lFormPais;
		
		$this -> view -> cabecalho = 'Inserir';
		
		if($this->getRequest()->isPost()){
			$lPost = $this->getRequest()->getPost();
			
			$lMdCidade = new Default_Model_Cidade();
 			$lMdCidade -> setNmcidade($lPost['nmcidade']);
 			$lMdCidade -> setFgativo($lPost['fgativo']);
 			
 			$lMdEstado = new Default_Model_Estado();
			$lMdEstado -> setCdestado($lPost['cdestado']);
 			$lMdCidade -> setEstado($lMdEstado);
		
			$lBdCidades = new Default_Model_CidadesMapper();
			$lBdCidades -> inserir($lMdCidade);
			return $this->_redirect('/default/cidades/index');
		}
		
	}
	
 	/** Edi��o de um combust�vel
	 * @author João Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function editAction(){
		$this->getHelper('viewRenderer')->setRender(cadastro);

		$this -> view -> cabecalho = 'Alterar';
		
		$lBdCidades = new Default_Model_CidadesMapper();
		$lMdCidade = new Default_Model_Cidade();
		$lMdCidade -> setCdcidade($this->getRequest()->cd);
		
		if($this->getRequest()->isPost()){
			$lPost = $this->getRequest()->getPost();
			
 			$lMdCidade -> setNmcidade($lPost['nmcidade']);
 			$lMdCidade -> setFgativo($lPost['fgativo']);
 			
 			$lMdEstado = new Default_Model_Estado();
			$lMdEstado -> setCdestado($lPost['cdestado']);
 			$lMdCidade -> setEstado($lMdEstado);
		
			
			$lBdCidades -> alterar($lMdCidade);
			return $this->_redirect('/default/cidades/index');
		}else{
			$lMdCidade = $lBdCidades -> buscar($lMdCidade);
		}
		
		$lForm = new Default_Form_InsereCidade();
		$lForm -> popFromModel($lMdCidade);
		$this->view -> pForm = $lForm;
		
		$lFormPais = new Default_Form_SelecaoPais();
		$lFormPais -> popFromModel($lMdCidade -> getEstado() -> getPais());
		$this->view -> pFormPais = $lFormPais;
		
		$lBdEstado = new Default_Model_EstadosMapper();
		$lListaEstado = $lBdEstado -> listarPais($lMdCidade -> getEstado() -> getPais());
		
		$lFormEstado = new Default_Form_SelecaoEstado($lListaEstado);
		$lFormEstado -> popFromModel($lMdCidade -> getEstado());
		$this->view -> pFormEstado = $lFormEstado;
		
	}
	
	/** Exclusão
	 * @author João Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function deleteAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$lMdCidade = new Default_Model_Cidade();
		$lMdCidade -> setCdcidade($this->getRequest()->cd);
		
		$lBdCidades = new Default_Model_CidadesMapper();
		$lMdCidade = $lBdCidades -> buscar($lMdCidade); 
    	$lBdCidades -> excluir($lMdCidade); 
    	
    	return $this->_redirect('/default/cidades/index');
    }
    
	/** Carregamento do campo de seleção de estado
	 * @author João Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function loadcidadeAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

    	$lMdEstado = new Default_Model_Estado();
		$lMdEstado -> setCdestado($this->getRequest()->cdestado);
		
		$lBdCidade = new Default_Model_CidadesMapper();
		$lListaCidade = $lBdCidade -> listarEstado($lMdEstado);
	
		$lForm = new Default_Form_SelecaoCidade($lListaCidade);
		echo $lForm -> cdcidade;
    }
    
    
}
?>