<?php

class Default_LogController extends Zend_Controller_Action{

    public function filtroAction(){
        // filtro
        $form = new Default_Form_FiltroLog();
		$this->view->form = $form;
		
		$this->view->cabecalho = "Consulta";
    }

    public function listagemAction(){
		if(!isset($this->_banco)){
			$this->_banco = new Default_Model_Log();
		}
	
		$form = new Default_Form_FiltroLog ();
		$this->view->form = $form;
		
		if($this->getRequest()->isPost()){
			$lPost = $this->getRequest()->getPost();
			
			$this->_banco->setFiltro($lPost[cdpessoa],
									 $lPost[dtinicio],
									 $lPost[dtfim],
									 $lPost[nmmodule],
									 $lPost[nmcontroller],
									 $lPost[nmaction]
									 );
			$logs = $this->_banco->BuscaComFiltro();
			
			$this->view->logs = $logs;
		}else {
			$this->_banco->resetFiltro();
		}
    }
}
