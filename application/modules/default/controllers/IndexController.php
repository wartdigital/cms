<?php

class Default_IndexController extends Zend_Controller_Action{
	
	public function init(){}
	
	/**
	 * Action index padr�o - ir� redirecionar conforme o acesso utilizado
	 * 
	 * @author Jo�o Pedro Grasselli
	 * @version 27/07/2013
	 */
	public function indexAction(){
		$this->getHelper('layout')->setLayout('login');
		$lCdAcesso = Zend_Auth::getInstance()->getStorage()->read()->cdacesso;

		$lBdParametros = new Parametros_Model_Parametros();
		$lCdAcessoInterno = $lBdParametros -> getparametro('ACESSO_INTERNO');

		//Se o acesso for 1- configurador redirecionamoes para /default/index/configurador
		if($lCdAcesso == $lCdAcessoInterno){
			return $this -> _forward('configurador', 'index', 'default');
		//Sen�o redirecionamoes para /default/index/usuario
		}else{
			return $this -> _forward('usuario', 'index', 'default');
		}
	}
	
	/**
	 * Action com mensagem de recusa dos termos de uso
	 * 
	 * @author Jo�o Pedro Grasselli
	 * @version 06/12/2014
	 */
	public function recusatermosAction(){
		$lBdParametros = new Parametros_Model_Parametros();
		$lTextoRecusaTermos = $lBdParametros -> getparametro('RECUSA_TERMOS');
		$this -> view -> pRecusaTermos = $lTextoRecusaTermos;
	}

    public function configuradorAction(){
		$this->getHelper('layout')->setLayout('layout');
    }
    
    public function usuarioAction(){
		$this->getHelper('layout')->setLayout('layout');

		$lBdLicenca = new Clientes_Model_LicencaMapper();
		$lBdClientesGrupos = new Clientes_Model_ClienteGrupoMapper();
		$lBdParametro = new Parametros_Model_Parametros();
		
		$lSessionHandler = new Zend_Session_Namespace('coonsultaconteudos', true);
		
		$lFormFiltro = new Conteudos_Form_FiltroConsultaConteudos();
		if($lSessionHandler->filtro != NULL){
			$lFormFiltro -> populate($lSessionHandler->filtro);
		}
		$this -> view -> pFormFiltroConteudos = $lFormFiltro;
		
		//Buscando termos de uso
		$lTermosDeUso = $lBdParametro -> getparametro('TERMOS_DE_USO');
		$this -> view -> pTermosDeUso = $lTermosDeUso;
		
		//Buscando dias de anteced�ncia para aviso de vencimento
		$lDiasVencimento = $lBdParametro -> getparametro('DIAS_ANTECEDENCIA_AVISO');
		$this -> view -> pDiasAntecedencia = $lDiasVencimento;
		
		//Buscando mensagem padr�o de n�o permiss�o de grupo
		$lMensagemPermissao = $lBdParametro -> getparametro('MENSAGEM_BUSCA_GRUPOS');
		$this -> view -> pMensagemPermissao = $lMensagemPermissao;
		
		//Montando objeto do usu�rio logado
		$lCdPessoa = Zend_Auth::getInstance()->getStorage()->read()->cdpessoa;
        $lUsuario = new Clientes_Model_Usuario();
		$lUsuario -> setCdpessoa($lCdPessoa);
		
		//Montando objeto da licen�a desse usu�rio
		$lLicenca = new Clientes_Model_Licenca();
		$lLicenca -> setUsuario($lUsuario);
		$lLicenca = $lBdLicenca -> buscar($lLicenca);
		
		$lCliente = $lLicenca -> getCliente();
		$lListaGruposDisponiveis = $lBdClientesGrupos -> listar($lCliente);
		$this -> view -> pListaGruposDisponiveis = $lListaGruposDisponiveis;
		
		if($this -> getRequest() -> alerta != NULL){
			$this -> view -> pAlerta = $this -> getRequest() -> alerta;
		}
    }
	
	public function logtermosAction(){
		$this->view->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
		
		$lCdPessoa = Zend_Auth::getInstance()->getStorage()->read()->cdpessoa;
		
		$lBdUsuario = new Usuarios_Model_UsuarioMapper();
		
		$lUsuario = new Usuarios_Model_Usuario();
		$lUsuario -> setCdpessoa($lCdPessoa);
		
		$lBdUsuario -> inserirAceiteTermos($lUsuario);
		
		$lSessionHandler = new Zend_Session_Namespace('aceitetermos', true);
		$lSessionHandler->aceiteTermos = true;
	}

    public function suporteAction(){
        $this->view->layout()->setLayout('load_layout');
        
        $lFormContato = new Default_Form_Suporte();
        $this->view->form = $lFormContato;
        
        if($this->getRequest()->isPost()){
        	if($lFormContato->isValid($this->getRequest()->getPost())){
        		
        		$gConfig = Zend_Registry::get('gConfig');
        		
	        	$lPost = $this->getRequest()->getPost();
	        	$lBdParam = new Parametros_Model_Parametros();
	        	
	        	$lMensagem = "<h3>Suporte T�cnico - ".$gConfig['application']['title']."</h3>";
	        	$lMensagem.= "<p>Usu�rio: ".$lPost['nmpessoa']."</p>";
	        	$lMensagem.= "<p>Email: ".$lPost['nmemail']."</p>";
	        	$lMensagem.= "<p>".$lPost['nmtitulo']."</p>";
	        	$lMensagem.= "<p>".$lPost['dsmensagem']."</p>";
	        	
	        	$lMailHelper = $this->_helper->getHelper('EnviaEmail');
	        	
				$lMailHelper -> settexto(nl2br($lMensagem));
				$lMailHelper -> setrem($lBdParam->getparametro('REMETENTE_ESQUECISENHA'), $lPost['nmpessoa']);
				$lMailHelper -> setdest($lBdParam->getparametro('EMAIL_SUPORTE'));
				$lMailHelper -> addBcc('testes@w3informatica.com.br');
				$lMailHelper -> setassunto("Suporte T�cnico - ".$gConfig['application']['title']." - ".$lPost['nmtitulo']);
				
				$lMailHelper -> enviar();
				
				echo "<script>alert('Mensagem enviada com sucesso!');self.close();</script>";
	        }else{
	        	$lFormContato->populate($lPost);
	        }
        }
    }
    
    public function downloadAction(){
	    $this->view->layout()->disableLayout();
	    $this->_helper->viewRenderer->setNoRender(true);
    
	    $lFileExtension 	= $this->getRequest()->extensao;
	    $lFileName 			= $this->getRequest()->nmarquivo;
	    $lFileRealName 		= $this->getRequest()->nmfisico;
	    $lFolder			= $this->getRequest()->nmpasta;
	    
	    switch($file_extension){
	    	case "pdf":
	    		header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header('Content-disposition: attachment; filename="'.$lFileName.'.'.$lFileExtension.'"');
				header("Content-Type: application/pdf");
				header("Content-Transfer-Encoding: binary");
	    		break;
	    	default:
	    		header('Content-Type: application/octet-stream');
	    		header('Content-Disposition: attachment; filename="'.$lFileName.'.'.$lFileExtension.'"');
	    		break;
	    } 
  
	    readfile(APPLICATION_PATH . "/../public/upload/".$lFolder."/".$lFileRealName);
    }
}
