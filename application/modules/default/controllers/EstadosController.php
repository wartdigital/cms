<?php 

class Default_EstadosController extends Zend_Controller_Action{
	
	/** Filtro para busca de um combust�vel
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function indexAction(){	    
		$lBdEstados = new Default_Model_EstadosMapper();
		$lListaEstados = $lBdEstados -> listar();
		$this->view -> pListaEstados = $lListaEstados;
	}
	
	/** Adi��o de um combust�vel
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function addAction(){
		$this->getHelper('viewRenderer')->setRender(cadastro);
		
		$lForm = new Default_Form_InsereEstado();
		$this->view -> pForm = $lForm;
		
		$lFormPais = new Default_Form_SelecaoPais();
		$this->view -> pFormPais = $lFormPais;
		
		$this -> view -> cabecalho = 'Inserir';
		
		if($this->getRequest()->isPost()){
			$lPost = $this->getRequest()->getPost();
			
			$lPais = new Default_Model_Pais();
			$lPais -> setCdPais($lPost['cdpais']);
			
			$lMdEstado = new Default_Model_Estado();
 			$lMdEstado -> setNmestado($lPost['nmestado']);
 			$lMdEstado -> setNmsigla($lPost['nmsigla']);
 			$lMdEstado -> setFgativo($lPost['fgativo']);
			$lMdEstado -> setPais($lPais);
			
			$lBdEstado = new Default_Model_EstadosMapper();
			$lBdEstado -> inserir($lMdEstado);
			return $this->_redirect('/default/estados/index');
		}
		
	}
	
 	/** Edi��o de um combust�vel
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function editAction(){
		$this->getHelper('viewRenderer')->setRender(cadastro);

		$this -> view -> cabecalho = 'Alterar';
		
		$lBdEstado = new Default_Model_EstadosMapper();
		$lMdEstado = new Default_Model_Estado();
		
		$lMdEstado -> setCdestado($this->getRequest()->cd);
				
		if($this->getRequest()->isPost()){
			$lPost = $this->getRequest()->getPost();
			
			$lPais = new Default_Model_Pais();
			$lPais -> setCdPais($lPost['cdpais']);
			
 			$lMdEstado -> setNmestado($lPost['nmestado']);
 			$lMdEstado -> setNmsigla($lPost['nmsigla']);
 			$lMdEstado -> setFgativo($lPost['fgativo']);
			$lMdEstado -> setPais($lPais);
			
			$lBdEstado -> alterar($lMdEstado);
			
			return $this->_redirect('/default/estados/index');
		}else{
			$lMdEstado = $lBdEstado -> buscar($lMdEstado);
		}
		
		$lForm = new Default_Form_InsereEstado();
		$lForm -> popFromModel($lMdEstado);
		$this->view -> pForm = $lForm;
		
		$lFormPais = new Default_Form_SelecaoPais();
		$lFormPais -> popFromModel($lMdEstado -> getPais());
		$this->view -> pFormPais = $lFormPais;
	}
	
	/** Exclus�o de um combust�vel
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function deleteAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

    	$lMdEstado = new Default_Model_Estado();
		$lMdEstado -> setCdestado($this->getRequest()->cd);
		
		$lBdEstado = new Default_Model_EstadosMapper();
		$lMdEstado = $lBdEstado -> buscar($lMdEstado); 
    	$lBdEstado -> excluir($lMdEstado); 
    	
    	return $this->_redirect('/default/estados/index');
    }
    
	/** Carregamento do campo de sele��o de estado
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return void
	 * @version 02/05/2014
	 */
	public function loadestadoAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

    	$lMdPais = new Default_Model_Pais();
		$lMdPais -> setCdPais($this->getRequest()->cdpais);
		
		$lBdEstado = new Default_Model_EstadosMapper();
		$lListaEstado = $lBdEstado -> listarPais($lMdPais);
		
		$lForm = new Default_Form_SelecaoEstado($lListaEstado);
		echo $lForm -> cdestado;
    }
    
    
}
?>