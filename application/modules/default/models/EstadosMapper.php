<?php

class Default_Model_EstadosMapper{

	private $dbTable;

	public function __construct(){
		$this->dbTable = new Default_Model_DbTable_Estado();
	}

	/** Busca todos os estados
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return
	 * @version 04/05/2014
	 */
	public function listar(){
		try{
			return $this->dbTable -> listar();
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Busca todos os estados de determinado pa�s
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return
	 * @version 04/05/2014
	 */
	public function listarPais(Default_Model_Pais $pPais){
		try{
			return $this->dbTable -> listarPais($pPais);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Busca todos os estados
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Estado $pEstado
	 * @return Default_Model_Estado $pEstado
	 * @version 04/05/2014
	 */
	public function buscar(Default_Model_Estado $pEstado){
		try{
			return $this->dbTable -> buscar($pEstado);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Busca todos os estados
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Estado $pEstado
	 * @return
	 * @version 04/05/2014
	 */
	public function inserir(Default_Model_Estado $pEstado){
		try{
			return $this->dbTable -> inserir($pEstado);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Busca todos os estados
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Estado $pEstado
	 * @return
	 * @version 04/05/2014
	 */
	public function alterar(Default_Model_Estado $pEstado){
		try{
			return $this->dbTable -> alterar($pEstado);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Busca todos os estados
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Estado $pEstado
	 * @return
	 * @version 04/05/2014
	 */
	public function excluir(Default_Model_Estado $pEstado){
		try{
			$pEstado -> setFgativo('9') ;
			return $this->dbTable -> alterar($pEstado);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
}
