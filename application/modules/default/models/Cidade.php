<?php
/** Classe que representa uma cidade
 * @author Jo�o Pedro Grasselli
 * @param
 * @return
 * @version 27/11/2014
 */
class Default_Model_Cidade extends Comum_Estruturadados_ModelComum{

	protected $cdcidade;
	protected $nmcidade;
	
	protected $estado;

    public function setCdCidade($pCdcidade) {
	   $this->cdcidade = $pCdcidade;
	}

	public function getCdCidade() {
	    return $this->cdcidade;
	}
	
	public function setNmCidade($pNmcidade) {
	   $this->nmcidade = $pNmcidade;
	}

	public function getNmCidade() {
	    return $this->nmcidade;
	}
	
 	public function setEstado(Default_Model_Estado $pEstado) {
	   $this->estado = $pEstado;
	}

	public function getEstado() {
	    return $this->estado;
	}
	
}
