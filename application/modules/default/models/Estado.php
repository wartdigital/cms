<?php
/** Classe que representa um estado
 * @author Jo�o Pedro Grasselli
 * @param
 * @return
 * @version 04/05/2014
 */
class Default_Model_Estado extends Comum_Estruturadados_ModelComum{

	protected $cdestado;
	protected $nmestado;
	protected $nmsigla;
	
	protected $pais;

    public function setCdestado($pCdEstado) {
	   $this->cdestado = $pCdEstado;
	}

	public function getCdestado() {
	    return $this->cdestado;
	}
	
	public function setNmestado($pNmEstado) {
	   $this->nmestado = $pNmEstado;
	}

	public function getNmestado() {
	    return $this->nmestado;
	}
	
	public function setNmsigla($pNmSigla) {
	   $this->nmsigla = $pNmSigla;
	}

	public function getNmsigla() {
	    return $this->nmsigla;
	}
	
 	public function setPais(Default_Model_Pais $pPais) {
	   $this->pais = $pPais;
	}

	public function getPais() {
	    return $this->pais;
	}
	
}
