<?php

class Default_Model_Auth extends Zend_Db_Table {
	
	protected $_name = 'itr_pessoa';
	protected $_primary = 'cdpessoa';
		
	
		/** Fun��o para trocar a senha do usu�rio
	  * @author Jo�o Pedro Grasselli 
	  * @param $cdPessoa codigo do usu�rio a ser alterada a senha
	  * @param $senha senha atual do usuario
	  * @param $novasenha nova senha do usuario
	  * @return bool 1 se verdadeiro, 2 se falso
	  * @version 26/09/2011
	  */
	public function trocasenha($cdPessoa, $senha, $novasenha){
		$where = '1';
		if(!empty($cdPessoa)){
			$where = $this->getAdapter()->quoteInto("cdpessoa = ?", $cdPessoa);					
		}
		$pessoa = $this->fetchRow($where)->toArray();
		if($pessoa[nmsenha]==$senha){
			try {		
				$dados = array (
					"nmsenha" => $novasenha
				);
				parent::update($dados, $where);
			}catch (Zend_Db_Exception $e){
				echo "Erro na consulta ao banco de dados";
			}
		}else{
			return "<font color=#FF0000>Erro!</font>";
		}
		return '<script>alert("Senha alterada com sucesso!")</script>';
	}

		/** Fun��o para buscar Usu�rio atrav�s do seu e-mail para gravar nova senha
	  * @author Gustavo Pistore 
	  * @param $email email do usu�rio que sera buscado
	  * @return Object contendo consulta do usu�rio
	  * @version 26/09/2011
	  */
	
		public function busca($apelido = null){
			try {
				$where = '1';
				if(!empty($apelido)){
					$where = $this->getAdapter()->quoteInto("nmlogin = ?", $apelido);
				 }
				return $this->fetchRow($where);
			}catch (Zend_Db_Exception $e){
					echo "Erro na consulta ao banco de dados". $e->getMessage();;
			}
		}

	/** Fun��o para listar usu�rios
	  * @author Gustavo Pistore 
	  * @param null
	  * @return Object contendo consulta do usu�rio
	  * @version 26/09/2011
	  */		
		
		public function roles(){
			try {
				$where = '1';
				return $this->fetchRow($where);
			}catch (Zend_Db_Exception $e){
					echo "Erro na consulta ao banco de dados". $e->getMessage();;
			}
		}
		
		
}

