<?php

class Default_Model_Log extends Zend_Db_Table
{
	protected $_name = 'itr_log';
	protected $_primary ='cdlog';
	
	protected $_pessoa;
	protected $_dtinicio;
	protected $_dtfim;
	protected $_module;
	protected $_controller;
	protected $_action;
	
	 
	 /** Adiciona um novo log
	 * @author Jo�o Pedro Grasselli 
	 * @param string $dados dados do log a serem inseridos
	 * @return 
	 * @version 10/10/2011
	 */
		public function addLog($dados){
			try{
				$dados = self::colunas($dados);
				parent::insert($dados);
			}catch(Zend_Db_Adapter_Exception $e){
				echo $e->getMessage();
			}
		}
/*
Fun��es de busca de dados
*/
     public function listarModules(){
			try{
				$select = $this ->getAdapter()->select()
				->distinct()
		        ->from(array('l'=> 'itr_log'), array('nmmodule'))
		        ->order("nmmodule ASC");
				$stmt = $this->getAdapter()->query($select);
				//$result = $stmt->fetch();
				$result = $stmt->fetchAll();
				return $result;
			}catch (Zend_Db_Exception $e){
					echo "Erro na consulta ao banco de dados";
			}
      }
      
      public function listarController(){
			try{
				$select = $this ->getAdapter()->select()
		        ->distinct()
		        ->from(array('l'=> 'itr_log'), array('nmcontroller'))
		        ->order("nmcontroller ASC");
				$stmt = $this->getAdapter()->query($select);
				$result = $stmt->fetchAll();
				return $result;
			}catch (Zend_Db_Exception $e){
					echo "Erro na consulta ao banco de dados";
			}
      }
      
      public function listarAction(){
			try{
				$select = $this ->getAdapter()->select()
		        ->distinct()
		        ->from(array('l'=> 'itr_log'), array('nmaction'))
		        ->order("nmaction ASC");
				$stmt = $this->getAdapter()->query($select);
				$result = $stmt->fetchAll();
				return $result;
			}catch (Zend_Db_Exception $e){
					echo "Erro na consulta ao banco de dados";
			}
      }

      /** Indica as op��es a serem utilizados no filtro para a busca de logs
	 * @author Cristiano Felippetti
	 * @param string $pPessoa
	 * @param string $pDataInicio
	 * @param string $pDataFim
	 * @param string $pModule
	 * @param string $pController
	 * @param string $pAction
	 * @return
	 * @version 29/02/2012
	 */
	public function setFiltro($pPessoa, $pDataInicio, $pDataFim, $pModule, $pController, $pAction){

		if(!empty($pPessoa)){
			$this->_pessoa=$pPessoa;
		}
		if(!empty($pDataInicio)){
			$this->_dtinicio=$pDataInicio;
		}
		if(!empty($pDataFim)){
			$this->_dtfim=$pDataFim;
		}
		if(!empty($pModule)){
			$this->_module=$pModule;
		}
		if(!empty($pController)){
			$this->_controller=$pController;
		}
		if(!empty($pAction)){
			$this->_action=$pAction;
		}
	}
	
	/** Busca logs com filtro
	 * @author Cristiano Felippetti
	 * @param
	 * @return array
	 * @version 29/02/2012
	 */
	public function BuscaComFiltro()
	{
		$select = $this ->getAdapter()->select()
		         		->from(array('l'=> 'itr_log'))
		         		->joinInner(array('p'=>'itr_pessoa'), 'l.cdusuario=p.cdpessoa', 'p.nmpessoa')
                         ;
		if(!empty($this->_pessoa)){
			$select ->where ('l.cdusuario = '.$this->_pessoa);
		}
		
        if (!empty($this->_dtinicio) AND !empty($this->_dtfim)){
				$select ->where ("l.dtlog >= '".parent::data($this->_dtinicio, "-")." 00:00:00' AND l.dtlog <= '".parent::data($this->_dtfim, "-")." 23:59:59'");
		} else {
				if (!empty($this->dtinicio)){
					$select ->where ("l.dtlog >= '".parent::data($this->_dtinicio, "-")." 00:00:00'");
				}
				if (!empty($this->dtfim)){
					$select ->where ("l.dtlog <= '".parent::data($this->_dtfim, "-")." 23:59:59'");
				}
		}
		if(!empty($this->_module)){
			$select ->where ("l.nmmodule = '".$this->_module."'");
		}
		if(!empty($this->_controller)){
			$select ->where ("l.nmcontroller = '".$this->_controller."'");
		}
		if(!empty($this->_action)){
			$select ->where ("l.nmaction = '".$this->_action."'");
		}
		//echo $select;
		//exit;
		$stmt = $this->getAdapter()->query($select);
		$result = $stmt->fetchAll();	
		return $result;
	}
	

}
