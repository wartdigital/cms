<?php

class Default_Model_DbTable_Estado extends Comum_Db_Table{

	protected $_name = 'itr_estado';
	protected $_primary = 'cdestado';

	/** Busca todas os estados
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return Default_Model_ListaEstado
	 * @version 04/05/2014
	 */
	public function listar(){
		try {
            $select = $this->select()->where('fgativo <> "9"')->order('nmestado ASC');
 			$lRowset = $this->fetchAll($select);

 			$lListaEstado = new Default_Model_ListaEstado();
			$lBdPais = new Default_Model_PaisesMapper();
			
 			foreach ($lRowset as $key => $lRow){
 				$lEstado = new Default_Model_Estado();

	 			$lEstado-> setCdestado($lRow['cdestado']);
	 			$lEstado-> setNmestado($lRow['nmestado']);
	 			$lEstado-> setNmsigla($lRow['nmsigla']);
	 			$lEstado-> setFgativo($lRow['fgativo']);
	 			
	 			$lPais = new Default_Model_Pais();
	 			$lPais-> setCdPais($lRow['cdpais']);
				$lPais = $lBdPais -> buscar($lPais);
				
				$lEstado -> setPais($lPais);
				
 				$lListaEstado -> add($lEstado);
 			}
 			
 			return $lListaEstado;
 		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados";
		}
	}
	
	/** Busca todas os estados de um pais
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return Default_Model_ListaEstado
	 * @version 04/05/2014
	 */
	public function listarPais(Default_Model_Pais $pPais){
		try {
            $select = $this->select()->where('fgativo = "1"')->where('cdpais = '.$pPais -> getCdPais())->order('nmestado ASC');
 			$lRowset = $this->fetchAll($select);
 			
			$lListaEstado = new Default_Model_ListaEstado();
			
 			foreach ($lRowset as $key => $lRow){
 				$lEstado = new Default_Model_Estado();

	 			$lEstado-> setCdestado($lRow['cdestado']);
	 			$lEstado-> setNmestado($lRow['nmestado']);
	 			$lEstado-> setNmsigla($lRow['nmsigla']);
	 			$lEstado-> setFgativo($lRow['fgativo']);
	 			
	 			$lPais = new Default_Model_Pais();
	 			$lPais-> setCdPais($lRow['cdpais']);
				
				$lEstado -> setPais($lPais);
				
 				$lListaEstado -> add($lEstado);
 			}
 			
 			return $lListaEstado;
 		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados";
		}
	}
	
	/** Busca um pais
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return Default_Model_Estado
	 * @version 04/05/2014
	 */
 	public function buscar(Default_Model_Estado $pEstado){
 		try{
 			$lResult = $this->find($pEstado -> getCdestado());
 		
 			if($lResult->count() > 0){
 				$lRow =  $lResult -> current() -> toArray() ;
 			}else{
 				throw new Zend_Exception("ESTADO INV�LIDO");
 			}
 			
 			$lEstado = new Default_Model_Estado();
 			$lBdPais = new Default_Model_PaisesMapper();

	 		$lEstado-> setCdestado($lRow['cdestado']);
	 		$lEstado-> setNmestado($lRow['nmestado']);
	 		$lEstado-> setNmsigla($lRow['nmsigla']);
			$lEstado-> setFgAtivo($lRow['fgativo']);
			
			$lPais = new Default_Model_Pais();
 			$lPais-> setCdPais($lRow['cdpais']);
			$lPais = $lBdPais -> buscar($lPais);
			
			$lEstado -> setPais($lPais);

 			return $lEstado;
 		}catch (Zend_Db_Exception $e){
 			die ($e);
 		}
 	}
 	
 	/** Adiciona um pa�s
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Pais $pEstado
	 * @return 
	 * @version 02/05/2014
	 */
 	public function inserir(Default_Model_Estado $pEstado){
 		try{
 			$lInsert = array('nmestado' 	=> $pEstado->getNmestado(),
 							 'nmsigla'		=> $pEstado->getNmsigla(),
 							 'cdpais'		=> $pEstado->getPais()->getCdPais(),
 			                 'fgativo' 		=> $pEstado->getFgativo());
 						
 			$this->insert($lInsert);
 		}catch (Zend_Db_Exception $e){
 			die ($e);
 		}
 	}
 	
 	/** Altera os dados de um pa�s
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Estado $pEstado
	 * @return
	 * @version 02/05/2014
	 */
	public function alterar(Default_Model_Estado $pEstado){
 		try{			
 			$lUpdate = array('nmestado' 	=> $pEstado->getNmestado(),
 							 'nmsigla'		=> $pEstado->getNmsigla(),
 							 'cdpais'		=> $pEstado->getPais()->getCdPais(),
 			                 'fgativo' 		=> $pEstado->getFgativo());
 				
 			$lWhere = 'cdestado = '.$pEstado->getCdestado();
 			$this->update($lUpdate, $lWhere);
 		}catch (Zend_Db_Exception $e){
 			die ($e);
 		}
 	}
	
}
