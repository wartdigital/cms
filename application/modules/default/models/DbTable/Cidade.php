<?php

class Default_Model_DbTable_Cidade extends Comum_Db_Table{

	protected $_name = 'itr_cidade';
	protected $_primary = 'cdcidade';

	/** Busca todas os estados
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return Default_Model_ListaEstado
	 * @version 04/05/2014
	 */
	public function listar(){
		try {
            $select = $this->select()->where('fgativo <> 9')->order('nmcidade ASC');
 			$lRowset = $this->fetchAll($select);

 			$lListaCidade = new Default_Model_ListaCidade();
			$lBdEstado = new Default_Model_EstadosMapper();
			
 			foreach ($lRowset as $key => $lRow){
 				$lCidade = new Default_Model_Cidade();

	 			$lCidade-> setCdcidade($lRow['cdcidade']);
	 			$lCidade-> setNmcidade($lRow['nmcidade']);
	 			$lCidade-> setFgativo($lRow['fgativo']);
	 			
	 			$lEstado = new Default_Model_Estado();
	 			$lEstado-> setCdestado($lRow['cdestado']);
				$lEstado = $lBdEstado -> buscar($lEstado);
				
				$lCidade -> setEstado($lEstado);
				
 				$lListaCidade -> add($lCidade);
 			}
 			
 			return $lListaCidade;
 		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados";
		}
	}
	
	/** Busca todas os estados de um pais
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Estado $pEstado
	 * @return Default_Model_ListaCidade
	 * @version 04/05/2014
	 */
	public function listarEstado(Default_Model_Estado $pEstado){
		try {
            $select = $this->select()->where('fgativo = "1"')->where('cdestado = '.$pEstado -> getCdestado())->order('nmcidade ASC');
 			$lRowset = $this->fetchAll($select);
 			
			$lListaCidade = new Default_Model_ListaCidade();
			$lBdEstado = new Default_Model_EstadosMapper();
			
 			foreach ($lRowset as $key => $lRow){
 				$lCidade = new Default_Model_Cidade();

	 			$lCidade-> setCdcidade($lRow['cdcidade']);
	 			$lCidade-> setNmcidade($lRow['nmcidade']);
	 			$lCidade-> setFgativo($lRow['fgativo']);
	 			
	 			$lEstado = new Default_Model_Estado();
	 			$lEstado-> setCdestado($lRow['cdestado']);
				$lEstado = $lBdEstado -> buscar($lEstado);
				
				$lCidade -> setEstado($lEstado);
				
 				$lListaCidade -> add($lCidade);
 			}
 			
 			return $lListaCidade;
 		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados";
		}
	}
	
	/** Busca um pais
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Cidade $pCidade
	 * @return Default_Model_Cidade $pCidade
	 * @version 04/05/2014
	 */
 	public function buscar(Default_Model_Cidade $pCidade){
 		try{
 			$lResult = $this->find($pCidade -> getCdcidade());
 		
 			if($lResult->count() > 0){
 				$lRow =  $lResult -> current() -> toArray() ;
 			}else{
 				throw new Zend_Exception("CIDADE INV�LIDA");
 			}
 			
 			$lListaCidade = new Default_Model_ListaCidade();
			$lBdEstado = new Default_Model_EstadosMapper();

	 		$lCidade = new Default_Model_Cidade();

 			$lCidade-> setCdcidade($lRow['cdcidade']);
 			$lCidade-> setNmcidade($lRow['nmcidade']);
 			$lCidade-> setFgativo($lRow['fgativo']);
 			
 			$lEstado = new Default_Model_Estado();
 			$lEstado-> setCdestado($lRow['cdestado']);
			$lEstado = $lBdEstado -> buscar($lEstado);
			
			$lCidade -> setEstado($lEstado);

 			return $lCidade;
 		}catch (Zend_Db_Exception $e){
 			die ($e);
 		}
 	}
 	
 	/** Adiciona um pa�s
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Cidade $pCidade
	 * @return 
	 * @version 02/05/2014
	 */
 	public function inserir(Default_Model_Cidade $pCidade){
 		try{
 			$lInsert = array('nmcidade' 	=> $pCidade->getNmcidade(),
 							 'cdestado'		=> $pCidade->getEstado()->getCdestado(),
 			                 'fgativo' 		=> $pCidade->getFgativo());
 						
 			$this->insert($lInsert);
 		}catch (Zend_Db_Exception $e){
 			die ($e);
 		}
 	}
 	
 	/** Altera os dados de um pa�s
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Cidade $pCidade
	 * @return
	 * @version 02/05/2014
	 */
	public function alterar(Default_Model_Cidade $pCidade){
 		try{			
 			$lUpdate = array('nmcidade' 	=> $pCidade->getNmcidade(),
 							 'cdestado'		=> $pCidade->getEstado()->getCdestado(),
 			                 'fgativo' 		=> $pCidade->getFgativo());
 				
 			$lWhere = 'cdcidade = '.$pCidade->getCdcidade();
 			$this->update($lUpdate, $lWhere);
 		}catch (Zend_Db_Exception $e){
 			die ($e);
 		}
 	}
	
}
