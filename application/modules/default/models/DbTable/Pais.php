<?php

class Default_Model_DbTable_Pais extends Comum_Db_Table{

	protected $_name = 'itr_pais';
	protected $_primary = 'cdpais';

	/** Busca todas os paises
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return Default_Model_ListaPais
	 * @version 04/05/2014
	 */
	public function listar(){
		try {
            $select = $this->select()->where('fgativo <> 9')->order('nmpais ASC');
 			$lRowset = $this->fetchAll($select);

 			$lListaPais = new Default_Model_ListaPais();

 			foreach ($lRowset as $key => $lRow){
 				$lPais = new Default_Model_Pais();

	 			$lPais-> setCdPais($lRow['cdpais']);
	 			$lPais-> setNmPais($lRow['nmpais']);
	 			$lPais-> setNmSigla($lRow['nmsigla']);
				$lPais-> setFgAtivo($lRow['fgativo']);
				
 				$lListaPais -> add($lPais);
 			}
 			
 			return $lListaPais;
 		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados";
		}
	}
	
	/** Busca um pais
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return Default_Model_Pais
	 * @version 04/05/2014
	 */
 	public function buscar(Default_Model_Pais $pPais){
 		try{
 			$lResult = $this->find($pPais -> getCdPais());
 		
 			if($lResult->count() > 0){
 				$lRow =  $lResult -> current() -> toArray() ;
 			}else{
 				throw new Zend_Exception("PA�S INV�LIDO");
 			}
 			
 			$lPais = new Default_Model_Pais();

	 		$lPais-> setCdPais($lRow['cdpais']);
	 		$lPais-> setNmPais($lRow['nmpais']);
	 		$lPais-> setNmSigla($lRow['nmsigla']);
			$lPais-> setFgAtivo($lRow['fgativo']);

 			return $lPais;
 		}catch (Zend_Db_Exception $e){
 			die ($e);
 		}
 	}
 	
 	/** Adiciona um pa�s
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Pais $pPais
	 * @return 
	 * @version 02/05/2014
	 */
 	public function inserir(Default_Model_Pais $pPais){
 		try{
 			$lInsert = array('nmpais' 		=> $pPais->getNmPais(),
 							 'nmsigla' 		=> $pPais->getNmSigla(),
 			                 'fgativo' 		=> $pPais->getFgativo());
 						
 			$this->insert($lInsert);
 		}catch (Zend_Db_Exception $e){
 			die ($e);
 		}
 	}
 	
 	/** Altera os dados de um pa�s
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Pais $pPais
	 * @return
	 * @version 02/05/2014
	 */
	public function alterar(Default_Model_Pais $pPais){
 		try{			
 			$lUpdate = array('nmpais' 		=> $pPais->getNmPais(),
 							 'nmsigla' 		=> $pPais->getNmSigla(),
 			                 'fgativo' 		=> $pPais->getFgativo());
 			
 			$lWhere = 'cdpais = '.$pPais->getCdPais();
 			$this->update($lUpdate, $lWhere);
 		}catch (Zend_Db_Exception $e){
 			die ($e);
 		}
 	}
	
}
