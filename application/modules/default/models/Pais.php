<?php
/** Classe que representa um pa�s
 * @author Jo�o Pedro Grasselli
 * @param
 * @return
 * @version 27/11/2014
 */
class Default_Model_Pais extends Comum_Estruturadados_ModelComum{

	protected $cdpais;
	protected $nmpais;
	protected $nmsigla;

    public function setCdpais($pCdpais) {
	   $this->cdpais = $pCdpais;
	}

	public function getCdpais() {
	    return $this->cdpais;
	}
	
	public function setNmpais($pNmpais) {
	   $this->nmpais = $pNmpais;
	}

	public function getNmpais() {
	    return $this->nmpais;
	}
	
	public function setNmsigla($pNmSigla) {
	   $this->nmsigla = $pNmSigla;
	}

	public function getNmsigla() {
	    return $this->nmsigla;
	}
	
}
