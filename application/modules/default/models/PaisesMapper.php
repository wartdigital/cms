<?php 

class Default_Model_PaisesMapper{
	
	private $dbTable;
	
	public function __construct(){
		$this->dbTable = new Default_Model_DbTable_Pais();
	}
	
	/** Busca todas pa�ses cadastradas
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return Default_Model_ListaPais
	 * @version 02/05/2014
	 */
	public function listar(){
		try{
			return $this->dbTable -> listar();
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Busca um pa�s
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Pais $pPais
	 * @return void
	 * @version  02/05/2014
	 */
	public function buscar(Default_Model_Pais $pPais){
		try{
			return $this->dbTable -> buscar($pPais);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Adi��o de um pa�s
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Pais $pPais
	 * @return void
	 * @version  02/05/2014
	 */
	public function inserir(Default_Model_Pais $pPais){
		try{
			return $this->dbTable -> inserir($pPais);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Altera os dados de um pa�s
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Pais $pPais
	 * @return void
	 * @version 02/05/2014
	 */
	public function alterar(Default_Model_Pais $pPais){
		try{
			return $this->dbTable -> alterar($pPais);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Exclus�o um pa�s
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Pais $pPais
	 * @return void
	 * @version 02/05/2014
	 */
	public function excluir(Default_Model_Pais $pPais){
		try{
			$pPais -> setFgativo('9') ;
			return $this->dbTable -> alterar($pPais);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
}
?>