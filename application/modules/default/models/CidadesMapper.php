<?php

class Default_Model_CidadesMapper{

	private $dbTable;

	public function __construct(){
		$this->dbTable = new Default_Model_DbTable_Cidade();
	}

	/** Busca todos as cidades
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return
	 * @version 04/05/2014
	 */
	public function listar(){
		try{
			return $this->dbTable -> listar();
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Busca todos as cidades de determinado estado
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Estado $pEstado
	 * @return
	 * @version 04/05/2014
	 */
	public function listarEstado(Default_Model_Estado $pEstado){
		try{
			return $this->dbTable -> listarEstado($pEstado);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Busca uma cidades
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Cidade $pCidade
	 * @return Default_Model_Cidade $pCidade
	 * @version 04/05/2014
	 */
	public function buscar(Default_Model_Cidade $pCidade){
		try{
			return $this->dbTable -> buscar($pCidade);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Insere uma cidade
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Cidade $pCidade
	 * @return
	 * @version 04/05/2014
	 */
	public function inserir(Default_Model_Cidade $pCidade){
		try{
			return $this->dbTable -> inserir($pCidade);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Altera uma cidade
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Cidade $pCidade
	 * @return
	 * @version 04/05/2014
	 */
	public function alterar(Default_Model_Cidade $pCidade){
		try{
			return $this->dbTable -> alterar($pCidade);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Exclui uma cidade
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Cidade $pCidade
	 * @return
	 * @version 04/05/2014
	 */
	public function excluir(Default_Model_Cidade $pCidade){
		try{
			$pCidade -> setFgativo('9') ;
			return $this->dbTable -> alterar($pCidade);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
}
