<?php
/**
 * Classe de lista para Estado
 *
 * @author Jo�o Pedro Grasselli
 */
class Default_Model_ListaEstado extends Comum_Estruturadados_ListaComum{

	/**
	 * M�todo respons�vel pela adi��o de um elemento a lista
	 *
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Estado $pEstado
	 */
	public function add(Default_Model_Estado $pEstado) {
		$this->lLista[$this->getTam()] = $pEstado;
	}

}

?>
