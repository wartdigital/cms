<?php
/**
 * Classe de lista para Cidade
 *
 * @author Jo�o Pedro Grasselli
 */
class Default_Model_ListaCidade extends Comum_Estruturadados_ListaComum{

	/**
	 * M�todo respons�vel pela adi��o de um elemento a lista
	 *
	 * @author Jo�o Pedro Grasselli
	 * @param Default_Model_Cidade $pCidade
	 */
	public function add(Default_Model_Cidade $pCidade) {
		$this->lLista[$this->getTam()] = $pCidade;
	}

}

?>
