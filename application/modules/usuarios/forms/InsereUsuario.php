<?php
    /**
 	 * Formul�rio para adi��o e edi��o de usu�rios
	 * @author Jo�o Pedro Grasselli
	 * @version 12/05/2014
	 */
class Usuarios_Form_InsereUsuario extends Zend_Form{

	public function init(){
	    $nome = new Comum_Form_Element_Text('nmpessoa');
		$nome ->setRequired(true)
			  ->setDescription('Nome');
		$this->addElement($nome);
		
		$senha = new Comum_Form_Element_Text('nmsenha');
		$senha ->setRequired(true)
			  ->setDescription('Senha');
		$this->addElement($senha);

        $apelido = new Comum_Form_Element_Text('nmapelido');
		$apelido ->setRequired(true)
			  ->setDescription('Login');
		$this->addElement($apelido);
		
		//contato
		$nmemail = new Comum_Form_Element_Text('nmemail');
		$nmemail ->setRequired(false)
			  ->setDescription('E-mail');
		$this->addElement($nmemail);

		$nrtelefone = new Comum_Form_Element_Telefone('nrtelefone');
		$nrtelefone ->setRequired(false)
			  ->setDescription('Telefone');
		$this->addElement($nrtelefone);
		
	 	//Bot�o de envio
	 	$submit = new Comum_Form_Element_Gravar('Gravar');
	 	$this->addElement($submit);

		//Bot�o Voltar
		$voltar = new Comum_Form_Element_Button('Voltar');
		$voltar -> setAttrib('OnClick', "history.go(-1)");
		$this->addElement($voltar);
	}
	
	public function popFromModel(Usuarios_Model_Usuario $pUsuario){
        if($pUsuario != NULL){
            $this -> nmpessoa -> setValue($pUsuario -> getNmpessoa());
            $this -> nmapelido -> setValue($pUsuario -> getNmapelido());
            $this -> nmemail -> setValue($pUsuario -> getNmemail());
            $this -> nrtelefone -> setValue($pUsuario -> getNrtelefone());
        }
	}
}
