<?php

class Usuarios_Model_DbTable_Usuario extends Comum_Db_Table{

    protected $_schema = 'security';
    protected $_name = 'zapp_usuarios';
    protected $_primary = 'cdusuario';

	public function listar(){
		try{
			$select = $this ->getAdapter()->select()
			                ->from(array('p'=> 'security.zapp_usuarios'))
							->joinInner(array('pp' => 'security.zapp_perfilusuario'), 'p.cdusuario = pp.cdusuario AND pp.cdacesso = 1', array())
		         	        ->where('p.fgativo <> 9')
		         	        ->order('nmusuario ASC');
            $stmt = $this->getAdapter()->query($select);
            $lResult = $stmt->fetchAll();
            
 			$lListaUsuario = new Usuarios_Model_ListaUsuario();

            foreach($lResult as $lKey => $lRow){
               $lUsuario = new Usuarios_Model_Usuario();

                $lUsuario -> setCdpessoa($lRow['cdusuario']);
                $lUsuario -> setNmpessoa($lRow['nmusuario']);
                $lUsuario -> setNmemail($lRow['nmemail']);
                $lUsuario -> setNmapelido($lRow['nmlogin']);
                $lUsuario -> setNmsenha($lRow['nmsenha']);
                $lUsuario -> setFgativo($lRow['fgativo']);
            
               $lListaUsuario -> add($lUsuario);
            }
            return $lListaUsuario;
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}

	public function buscar(Usuarios_Model_Usuario $pUsuario){
		try{
            $lResult = $this->find($pUsuario -> getCdpessoa());
            
            if($lResult->count() > 0){
 				$lRow =  $lResult -> current() -> toArray() ;
 			}else{
 				throw new Zend_Exception("USUÁRIO INVÁLIDO");
 			}

            $lUsuario = new Usuarios_Model_Usuario();
            $lUsuario -> setCdpessoa($lRow['cdusuario']);
            $lUsuario -> setNmpessoa($lRow['nmusuario']);
            $lUsuario -> setNmemail($lRow['nmemail']);
            $lUsuario -> setNmapelido($lRow['nmlogin']);
            $lUsuario -> setNmsenha($lRow['nmsenha']);
            $lUsuario -> setFgativo($lRow['fgativo']);
            
            $lUsuario -> setSessionid($lRow['nmsession']);

            return $lUsuario;
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}

	public function getByLogin(Usuarios_Model_Usuario $pUsuario){
		try{
            $lSelect = $this ->getAdapter()->select()
			                ->from(array('p'=> 'security.zapp_usuarios'))
		         	        ->where('p.fgativo <> 9')
							->where('p.nmlogin = ?', $pUsuario -> getNmapelido())
		         	        ->order('nmusuario ASC');
            $stmt = $this->getAdapter()->query($lSelect);
            $lRow = $stmt->fetch();

			if($lRow){
				$lUsuario = new Usuarios_Model_Usuario();
                $lUsuario -> setCdpessoa($lRow['cdusuario']);
                $lUsuario -> setNmpessoa($lRow['nmusuario']);
                $lUsuario -> setNmemail($lRow['nmemail']);
                $lUsuario -> setNmapelido($lRow['nmlogin']);
                $lUsuario -> setNmsenha($lRow['nmsenha']);
                $lUsuario -> setFgativo($lRow['fgativo']);
	
				return $lUsuario;
			}
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}

	public function getByMail(Usuarios_Model_Usuario $pUsuario){
		try{
            $lSelect = $this ->getAdapter()->select()
			                ->from(array('p'=> 'security.zapp_usuarios'))
		         	        ->where('p.fgativo <> 9')
							->where('p.nmemail = ?', $pUsuario -> getNmemail())
		         	        ->order('nmpessoa ASC');
            $stmt = $this->getAdapter()->query($lSelect);
            $lRow = $stmt->fetch();

			if($lRow){
				$lUsuario = new Usuarios_Model_Usuario();
				$lUsuario -> setCdpessoa($lRow['cdpessoa']);
				$lUsuario -> setNmpessoa($lRow['nmpessoa']);
				$lUsuario -> setNmemail($lRow['nmemail']);
				$lUsuario -> setNrtelefone($lRow['nrtelefone_residencial']);
				$lUsuario -> setNmapelido($lRow['nmapelido']);
				$lUsuario -> setNmsenha($lRow['nmsenha']);
				$lUsuario -> setFgativo($lRow['fgativo']);
	
				return $lUsuario;
			}
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}

	public function inserir(Usuarios_Model_Usuario $pUsuario){
		try{
            $lCriptografia = Zend_Controller_Action_HelperBroker::getStaticHelper('Criptografa');
            $lSenha = $lCriptografia-> criptografa($pUsuario -> getNmsenha());
            
            $lUsuario = array('nmusuario' => $pUsuario -> getNmpessoa(),
                              'nmemail' => $pUsuario -> getNmemail(),
                              'nmlogin' => $pUsuario -> getNmapelido(),
                              'nmsenha' => $lSenha,
                              'fgativo' => '1');

            $lUsuarioInserido = parent::insert($lUsuario);
            $lCdUsuario = $this -> lastID();
            
            $pUsuario -> setCdpessoa($lCdUsuario);
            $this ->inserirPerfilDefault($pUsuario);
            
			return $lCdUsuario;
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}

	public function alterar(Usuarios_Model_Usuario $pUsuario){
		try{
			$lWhere = $this->getAdapter()->quoteinto("cdpessoa = ?", $pUsuario -> getCdpessoa());

            $lUsuario = array('nmpessoa' => $pUsuario -> getNmpessoa(),
                              'nmemail' => $pUsuario -> getNmemail(),
                              'nmapelido' => $pUsuario -> getNmapelido(),
                              'nrtelefone_residencial' => $pUsuario -> getNrtelefone(),
                              'fgativo' => $pUsuario -> getFgativo(),
                              'nmsession' => $pUsuario -> getSessionid());
                              
            if($pUsuario -> getNmsenha() != NULL){
                $lUsuario['nmsenha'] = $pUsuario -> getNmsenha();
            }
			
			//Sempre que um usuário for excluído limpamos seu login
			if($pUsuario -> getFgativo() == '9'){
                $lUsuario['nmlogin'] = '';
            }

            return  parent::update($lUsuario, $lWhere);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}

	public function inserirPerfilDefault(Usuarios_Model_Usuario $pUsuario){
        $lBdParametro = new Parametros_Model_Parametros();
        $lCdAcesso = $lBdParametro -> getparametro('ACESSO_INTERNO');
        $lCdPerfil = $lBdParametro -> getparametro('PERFIL_USUARIO_INTERNO');
        
        $lArrayInsert = array('cdusuario' => $pUsuario -> getCdpessoa(),
                              'cdperfil' => $lCdPerfil,
                              'cdacesso' => $lCdAcesso);
        
        $lTable = new Comum_Db_Table('security.zapp_perfilusuario');
        $lTable -> insert($lArrayInsert);
	}

	public function inserirAceiteTermos(Usuarios_Model_Usuario $pUsuario){
		try{
			$lWhere = $this->getAdapter()->quoteinto("cdpessoa = ?", $pUsuario -> getCdpessoa());

            $lUsuario = array('fgaceitetermos' => '1',
                              'dtaceitetermos' => date('Y-m-d H:i:s'),
                              'nripaceitetermos' => $_SERVER['REMOTE_ADDR']);

            return  parent::update($lUsuario, $lWhere);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}
}
