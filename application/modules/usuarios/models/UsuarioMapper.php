<?php

class Usuarios_Model_UsuarioMapper extends Comum_Estruturadados_MapperComum{

	public function __construct(){
		$this->dbTable = new Usuarios_Model_DbTable_Usuario();
	}

	/** Lista todos os usu�rios internos
	 * @author Jo�o Pedro Grasselli
	 * @param void
	 * @return Usuarios_Model_ListaUsuario
	 * @version 12/05/2014
	 */
	public function listar(){
		try{
			return $this->dbTable -> listar();
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}

	/** Busca um usu�rio de cliente
	 * @author Jo�o Pedro Grasselli
	 * @param Usuarios_Model_Usuario $pUsuario
	 * @return Usuarios_Model_Usuario
	 * @version 12/05/2014
	 */
	public function buscar(Usuarios_Model_Usuario $pUsuario){
		try{
			return $this->dbTable -> buscar($pUsuario);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}

	/** Busca um usu�rio de cliente
	 * @author Jo�o Pedro Grasselli
	 * @param Usuarios_Model_Usuario $pUsuario
	 * @return Usuarios_Model_Usuario
	 * @version 12/05/2014
	 */
	public function getByLogin(Usuarios_Model_Usuario $pUsuario){
		try{
			return $this->dbTable -> getByLogin($pUsuario);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Busca um usu�rio de cliente
	 * @author Jo�o Pedro Grasselli
	 * @param Usuarios_Model_Usuario $pUsuario
	 * @return Usuarios_Model_Usuario
	 * @version 12/05/2014
	 */
	public function getByMail(Usuarios_Model_Usuario $pUsuario){
		try{
			return $this->dbTable -> getByMail($pUsuario);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Insere um usu�rio de cliente
	 * @author Jo�o Pedro Grasselli
	 * @param Usuarios_Model_Usuario $pUsuario
	 * @return void
	 * @version 12/05/2014
	 */
	public function inserir(Usuarios_Model_Usuario $pUsuario){
		try{
			return $this->dbTable -> inserir($pUsuario);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Altera um usu�rio de cliente
	 * @author Jo�o Pedro Grasselli
	 * @param Usuarios_Model_Usuario $pUsuario
	 * @return void
	 * @version 12/05/2014
	 */
	public function alterar(Usuarios_Model_Usuario $pUsuario){
		try{
			return $this->dbTable -> alterar($pUsuario);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Exclui um usu�rio de cliente
	 * @author Jo�o Pedro Grasselli
	 * @param Usuarios_Model_Usuario $pUsuario
	 * @return void
	 * @version 12/05/2014
	 */
	public function excluir(Usuarios_Model_Usuario $pUsuario){
		try{
			$lUsuario = self::buscar($pUsuario);
			$lUsuario -> setFgativo('9');
			return $this->dbTable -> alterar($lUsuario);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Insere o log de aceite de termos de uso
	 * @author Jo�o Pedro Grasselli
	 * @param Usuarios_Model_Usuario $pUsuario
	 * @return void
	 * @version 10/05/2014
	 */
	public function inserirAceiteTermos(Usuarios_Model_Usuario $pUsuario){
		try{
			return $this->dbTable -> inserirAceiteTermos($pUsuario);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
}
