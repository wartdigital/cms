<?php
class Usuarios_Model_Usuario extends Comum_Estruturadados_ModelComum{

    protected $cdpessoa;
    protected $nmpessoa;
    protected $nrtelefone;
    protected $nmemail;
    protected $nmapelido;
    protected $nmsenha;

    protected $sessionid;

    public function setCdpessoa($pCdPessoa) {
	   $this->cdpessoa = $pCdPessoa;
	}

    public function getCdpessoa() {
	    return $this->cdpessoa;
	}

    public function setNmpessoa($pNmPessoa) {
	   $this->nmpessoa = $pNmPessoa;
	}

    public function getNmpessoa() {
	    return $this->nmpessoa;
	}

    public function setNmemail($pNmEmail) {
	   $this->nmemail = $pNmEmail;
	}

	public function getNmemail() {
	    return $this->nmemail;
	}

    //Login
    public function setNmapelido($pNmApelido) {
	   $this->nmapelido = $pNmApelido;
	}

    public function getNmapelido() {
	    return $this->nmapelido;
	}

    public function setNmsenha($pNmSenha) {
	   $this->nmsenha = $pNmSenha;
	}

    public function getNmsenha() {
	    return $this->nmsenha;
	}
	
	public function setSessionid($pSessionId){
	    $this -> sessionid = $pSessionId;
	}
	
	public function getSessionid(){
	    return $this -> sessionid;
	}
}

