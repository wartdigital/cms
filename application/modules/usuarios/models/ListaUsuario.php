<?php
/**
 * Classe de lista para Usuario
 *
 * @author Jo�o Pedro Grasselli
 */
class Usuarios_Model_ListaUsuario extends Comum_Estruturadados_ListaComum{

	/**
	 * M�todo respons�vel pela adi��o de um elemento a lista
	 *
	 * @author Jo�o Pedro Grasselli
	 * @param Usuarios_Model_Usuario $pUsuario
	 */
	public function add(Usuarios_Model_Usuario $pUsuario) {
		$this->lLista[$this->getTam()] = $pUsuario;
	}

}

?>
