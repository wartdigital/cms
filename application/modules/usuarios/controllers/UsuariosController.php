<?php
class Usuarios_UsuariosController extends Zend_Controller_Action {

	public function init() {
	}

	/**
	 *  Listagem de usu�rios
	 */
	public function indexAction() {
		$lBdUsuarios = new Usuarios_Model_UsuarioMapper();
		$lListaUsuarios = $lBdUsuarios -> listar();
		$this -> view -> pListaUsuarios = $lListaUsuarios;
	}

	/**
	 *  Inclus�o de usu�rio
	 */
	public function addAction() {
		$this -> getHelper('viewRenderer') -> setRender('cadastro');

		$lBdUsuarios = new Usuarios_Model_UsuarioMapper();

		$lFormUsuario = new Usuarios_Form_InsereUsuario();
		$this -> view -> pForm = $lFormUsuario;

		$this -> view -> cabecalho = 'Inclusão';

		if ($this -> getRequest() -> isPost()) {
			$lPost = $this -> getRequest() -> getPost();

			$lUsuario = new Usuarios_Model_Usuario();
			$lUsuario -> setNmpessoa($lPost['nmpessoa']);
			$lUsuario -> setNmapelido($lPost['nmapelido']);
			$lUsuario -> setNmemail($lPost['nmemail']);
			$lUsuario -> setNrtelefone($lPost['nrtelefone']);
			$lUsuario -> setNmsenha($lPost['nmsenha']);

			$lBdUsuarios -> inserir($lUsuario);

			return $this -> _redirect('/usuarios/usuarios/index');
		}
	}

	/**
	 *  Altera��o de usu�rio
	 */
	public function editAction() {
		$this -> getHelper('viewRenderer') -> setRender('cadastro');

		$lCdUsuario = $this -> getRequest() -> cdusuario;

		$lBdUsuarios = new Usuarios_Model_UsuarioMapper();

		$lUsuario = new Usuarios_Model_Usuario();
		$lUsuario -> setCdpessoa($lCdUsuario);
		$lUsuario = $lBdUsuarios -> buscar($lUsuario);

		$lFormUsuario = new Usuarios_Form_InsereUsuario();
		$lFormUsuario -> popFromModel($lUsuario);
		$this -> view -> pForm = $lFormUsuario;

		$this -> view -> cabecalho = 'Alteração';
		$this -> view -> pDesabilitaSenha = true;

		if ($this -> getRequest() -> isPost()) {
			$lPost = $this -> getRequest() -> getPost();

			$lUsuario -> setNmpessoa($lPost['nmpessoa']);
			$lUsuario -> setNmapelido($lPost['nmapelido']);
			$lUsuario -> setNmemail($lPost['nmemail']);
			$lUsuario -> setNrtelefone($lPost['nrtelefone']);

			$lBdUsuarios -> alterar($lUsuario);

			return $this -> _redirect('/usuarios/usuarios/index');
		}
	}

	/**
	 *  Exclus�o de usu�rio
	 */
	public function deleteAction() {
		$this -> _helper -> layout -> disableLayout();
		$this -> _helper -> viewRenderer -> setNoRender();

		$lCdUsuario = $this -> getRequest() -> cdusuario;

		if ($lCdUsuario != "") {
			$lBdUsuarios = new Usuarios_Model_UsuarioMapper();

			$lUsuario = new Usuarios_Model_Usuario();
			$lUsuario -> setCdpessoa($lCdUsuario);
			$lBdUsuarios -> excluir($lUsuario);

			return $this -> _redirect('/usuarios/usuarios/index');
		}
	}

	/**
	 * Valida��o de login/apelido
	 */
	public function validacaoAction() {
		$this -> _helper -> layout -> disableLayout();
		$this -> _helper -> viewRenderer -> setNoRender();

		$lNmApelido = $this -> getRequest() -> getPost("nmapelido");
		$lCdUsuario = $this -> getRequest() -> getPost("cdusuario");

		if ($lNmApelido != "") {
			$lBdUsuarios = new Usuarios_Model_UsuarioMapper();

			$lUsuario = new Usuarios_Model_Usuario();
			$lUsuario -> setNmapelido($lNmApelido);

			$lVOUsuario = $lBdUsuarios -> getByLogin($lUsuario);

			if ($lVOUsuario != NULL) {
				if($lCdUsuario != NULL){
					if($lVOUsuario -> getCdpessoa() == $lCdUsuario){
						$lReturn = json_encode(array("SUCESSO"));
					}else{
						$lReturn = json_encode(array("FALHA"));
					}
				}else{
					$lReturn = json_encode(array("FALHA"));
				}
			}else{
				$lReturn = json_encode(array("SUCESSO"));	
			}
		}else{
			$lReturn = json_encode(array("FALHA"));	
		}
		echo $lReturn;
	}

}
