<?php
/**
 * Login_Model_Users
 *  
 * @author Enrico Zimuel (enrico@zimuel.it)
 */
class Login_Model_Users extends Zend_Db_Table_Abstract
{
    protected $_schema = 'security';
    protected $_name = 'zapp_perfilusuario';
    protected $_primary = array('cdusuario', 'cdperfil');
        
    /**
     * Get the role Id of a user
     * 
     * @param string $userID
     * @param string $cdacesso
     * @return integer|boolean
     */
    public function getRoleId($userID, $cdAcesso) {
    	if (empty($username) or empty($cdAcesso)) {
    		return false;
    	}
    	$select = $this->getAdapter()->select()
				->from(array('pp'=>'security.zapp_perfilusuario'))
				->where("cdusuario = ?",$userID)
				->where("cdacesso = ?",$cdAcesso);
		$stmt = $this->getAdapter()->query($select);
		$result = $stmt->fetchAll();	
    	if (!empty($result)) {
    		return $result[0]['cdperfil'];
    	}
    	return false;
    }
}