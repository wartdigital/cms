<?php class Login_Model_Menu extends Zend_Db_Table {

    protected $_schema = 'security';
	protected $_name = 'zapp_menu';
	protected $_primary = 'cdmenu';

    public function montaMenu($role){
           $select = $this->getAdapter()->select();
	       $select->from(array('p' => 'security.zapp_perfilrelacao'), array('mod.cdmenu', 'menu.nmmenu'))
                  ->distinct()
                  ->joinInner(array('mod' => 'security.zapp_modulos'), 'p.cdmodulo=mod.cdmodulo', '')
                  ->joinInner(array('menu'=>'security.zapp_menu'), 'mod.cdmenu=menu.cdmenu', '')
                  ->where('p.cdperfil=?', $role)
                  ->order('nmmenu ASC');

           $stmt = $this->getAdapter()->query($select);
           return $menu = $stmt ->fetchAll();
	}
}
