<?php
/**
 * Login_Model_Resources
 *  
 * @author Enrico Zimuel (enrico@zimuel.it)
 */
class Login_Model_Resources extends Zend_Db_Table_Abstract
{
    protected $_schema = 'security';
    protected $_name = 'zapp_perfilrelacao';
    protected $_primary = array('cdperfil', 'cdmodulo');

    /**
     * getResources
     *
     * @param integer $role
     * @return array
     */
    public function getResources($role) 
    {
		$select = $this->getAdapter()->select();
      	$select->from(array('p' => 'security.zapp_perfilrelacao'), array('cdnivel'))
               ->joinInner(array('m' => 'security.zapp_modulos'), 'p.cdmodulo=m.cdmodulo')
               ->where('p.cdperfil=?', $role);
		       $stmt = $this->getAdapter()->query($select);
		       $results = $stmt->fetchAll();
		       $retorno = array();
		       $cont=0;
		       foreach($results as $modulo){
        			$select = $this->getAdapter()->select();
        			$select->from(array('a' => 'security.zapp_action'))
            	   	   ->where('a.cdmodulo=?', $modulo[cdmodulo])
            	   	   ->where('a.cdnivel <=?', $modulo[cdnivel]);
            		$stmt = $this->getAdapter()->query($select);
            		$modulo = $stmt->fetchAll();
            		$cont++;
            		$retorno[$cont]= $modulo;
		        }
		      
		       return $retorno;
    }
    
}