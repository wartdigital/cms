
<?php class Login_Model_Auth extends Zend_Db_Table {

    protected $_schema = 'security';
	protected $_name = 'zapp_usuarios';
	protected $_primary = 'cdusuario';

	
	 /** Fun��o para trocar a senha do usu�rio
	  * @author Jo�o Pedro Grasselli 
	  * @param $cdPessoa codigo do usu�rio a ser alterada a senha
	  * @param $senha senha atual do usuario
	  * @param $novasenha nova senha do usuario
	  * @return bool 1 se verdadeiro, 2 se falso
	  * @version 26/09/2011
	  */
	public function trocasenha($cdPessoa, $senha, $novasenha){
		$where = '1';
		if(!empty($cdPessoa)){
			$where = $this->getAdapter()->quoteInto("cdusuario = ?", $cdPessoa);
		}
		$pessoa = $this->fetchRow($where)->toArray();
		if($pessoa[nmsenha]==$senha){
			try {		
				$dados = array (
					"nmsenha" => $novasenha
				);
				parent::update($dados, $where);
			}catch (Zend_Db_Exception $e){
				echo "Erro na consulta ao banco de dados";
			}
		}else{
			return "<font color=#FF0000>Erro!</font>";
		}
		return '<script>alert("Senha alterada com sucesso!")</script>';
	}

	 /** Função para buscar Usuário através do seu e-mail para gravar nova senha
	  * @author Gustavo Pistore 
	  * @param $email email do usuário que sera buscado
	  * @return Object contendo consulta do usu�rio
	  * @version 26/09/2011
	  */
	public function busca($apelido = null){
		try {
			//$where = '1';
			if(!empty($apelido)){
				$where = $this->getAdapter()->quoteInto("nmlogin = ?", $apelido);
			}
			return $this->fetchRow($where);
		}catch (Zend_Db_Exception $e){
				echo "Erro na consulta ao banco de dados: ". $e->getMessage();;
		}
	}

	public function roles(){
		try {
			$where = '1';
			return $this->fetchRow($where);
		}catch (Zend_Db_Exception $e){
				echo "Erro na consulta ao banco de dados". $e->getMessage();;
		}
	
	}
	public function buscaroles($pessoa,$acesso){
		try {
			$select = $this ->getAdapter() -> select()
							->from(array('a'=>'security.zapp_perfilusuario'))
							->joinInner(array('c'=>'security.zapp_acesso'), 'a.cdacesso=c.cdacesso', 'nmacesso')
							->where("a.cdusuario = ?",$pessoa)
							->where("a.cdacesso= ?", $acesso);

			$stmt = $this->getAdapter()->query($select);
			$result = $stmt->fetch();	
			return $result;
		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados: ". $e->getMessage();;
		}	
	}
}
