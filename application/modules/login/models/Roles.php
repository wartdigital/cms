<?php
/**
 * Login_Model_Roles
 *  
 * @author Enrico Zimuel (enrico@zimuel.it)
 */
class Login_Model_Roles extends Zend_Db_Table_Abstract
{
    protected $_schema = 'security';
    protected $_name = 'zapp_perfil';
    protected $_primary = 'cdperfil';

    /**
     * getRoles
     * 
     * @return object
     */
    public function getRoles() {
    	return $this->fetchAll(null,'cdperfil');
    }
}