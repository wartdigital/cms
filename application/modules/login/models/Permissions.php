<?php

/**
 * Login_Model_Permissions
 *  
 * @author Enrico Zimuel (enrico@zimuel.it)
 */
class Login_Model_Permissions extends Zend_Db_Table_Abstract
{
    protected $_schema = 'security';
    protected $_name = 'zapp_perfilrelacao';
    protected $_primary = array('cdperfil', 'cdmodulo');


    /**
     * getPermissions
     * 
     * @param integer $role
     * @return array 
     */
    public function getPermissions($role){
		$select = $this->getAdapter()->select();
      	$select->from(array('p' => 'security.zapp_perfilrelacao'), array('cdnivel'))
               ->joinleft(array('m' => 'security.zapp_modulos'), 'p.cdmodulo=m.cdmodulo')
               ->where('p.cdperfil=?', $role);
		
        $stmt = $this->getAdapter()->query($select);
		$results = $stmt->fetchAll();
		$retorno = array();
		
		$cont=0;
		foreach($results as $modulo){
        			$select = $this->getAdapter()->select();
        			$select->from(array('a' => 'security.zapp_action'))
            	   	   ->where('a.cdmodulo=?', $modulo[cdmodulo])
            	   	   ->where('a.cdnivel <=?', $modulo[cdnivel]);
            		$stmt = $this->getAdapter()->query($select);
            		$modulo = $stmt->fetchAll();
            		$cont++;
            		$retorno[$cont]= $modulo;
		}
		return $retorno;
	}

}