<?php
/**
 * Login_Acl
 *
 * @author Enrico Zimuel (enrico@zimuel.it)
 */
class Login_Model_Acl extends Zend_Acl {
    /**
     * __construct
     *
     * @param Zend_Db_Adapter $db
     * @param integer $role
     */
    public function __construct($db,$role) {
        $this->loadRoles($db);
        //$roles = new Login_Model_Roles($db);
        if(!empty($role)) {
            $this->loadResources($db,$role);
        	$this->loadPermissions($db,$role);
        }
    }
    /**
     * Load all the roles from the DB
     *
     * @param Zend_Db_Adapter $db
     * @return boolean
     */
    public function loadRoles($db) {
    	if (empty($db)) {
    		return false;
    	}
        $roles = new Login_Model_Roles($db);
        $allRoles = $roles->getRoles();
        foreach ($allRoles as $role) {
                $this->addRole(new Zend_Acl_Role($role->cdperfil));
        }
        return true;
    }
    /**
     * Load all the resources for the specified role
     *
     * @param Zend_Db_Adapter $db
     * @param integer $role
     * @return boolean
     */
    public function loadResources($db,$role) {
    	if (empty($db)) {
    		return false;
    	}
    	$resources= new Login_Model_Resources($db);
    	$allResources= $resources->getResources($role);
    	foreach ($allResources as $res) {
    			foreach($res as $modulo){
	                if (!$this->has($modulo['nmaction'])) {
	                    $this->addResource(new Zend_Acl_Resource(str_replace("-","", $modulo['nmaction'])));
	                }
    			}
    	}
        return true;
    }
    /**
     * Load all the permission for the specified role
     *
     * @param Zend_Db_Adapter $db
     * @param integer $role
     * @return boolean
     */
    public function loadPermissions($db,$role) {
    	if (empty($db)) {
    		return false;
    	}
    	$permissions= new Login_Model_Permissions($db);
    	$allPermissions= $this->getResources();
    	foreach ($allPermissions as $resource) {
    			$this->allow($role,$resource);
    	}
		return true;
	}
}