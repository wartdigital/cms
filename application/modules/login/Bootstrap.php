<?php

class Login_Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{    
    public function _initNewsAutoload()  
    {  
        $moduleLoader = new Zend_Application_Module_Autoloader(  
                                array(  
                                    'namespace' => 'Login',  
                                    'basePath' => APPLICATION_PATH . '/modules/login'  
                                )  
                            );  
  
        // adding model resources to the autoloader  
        $moduleLoader->addResourceTypes(  
                array(  
                    'plugins' => array(  
                        'path' => 'plugins',  
                        'namespace' => 'Plugin'  
                    )  
                )  
            );  
  
        return $moduleLoader;  
    }  
  
    public function _initPlugins()  
    {  
        $this->bootstrap('frontcontroller');  
        $fc = $this->getResource('frontcontroller');  
  
        $fc->registerPlugin(new Login_Plugin_AccessControl());
    }  
}