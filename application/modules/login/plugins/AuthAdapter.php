<?php

class Login_Plugin_AuthAdapter extends Zend_Auth_Adapter_DbTable
{
 public function __construct()
   {
      $registry = Zend_Registry::getInstance();
      parent::__construct($registry->dbAdapter);
      

      $this->setTableName('security.zapp_usuarios');
      $this->setIdentityColumn('nmlogin');
      $this->setCredentialColumn('nmsenha');

      $this->setCredentialTreatment('$crip->criptografa(?)');
	  
      
   }
}