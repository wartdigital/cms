<?php


class Login_Plugin_AccessControl extends Zend_Controller_Plugin_Abstract{
   /**
    * @var Zend_Auth
    */
   private $_auth;
   
   /**
    * @var Zend_Acl
    */
   private $_acl;
   
   /**
    * @var string
    */
   private $_message;
   
   private $MODULE_NO_AUTH = array('ws');
   
   private $ACTIONS_NO_AUTH = array('login/auth/login',
   									'login/auth/logout',
								    'login/auth/esquecisenha',
   									'default/index/suporte');
	
   private $ACTIONS_NO_PERMISSION = array('login/auth/trocasenha',
   										  'default/error/index',
   										  'default/error/error',
   										  'default/index/download',
   										  'default/index/index',
   										  'default/index/recusatermos',
   										  'pessoa/pessoaobj/valida',
   										  'default/estado/loadestado',
   										  'default/cidade/loadcidade',
   										  'modelos/modelos/validacao',
   										  'usuarios/usuarios/validacao'
   					);
   private $_controller;
   private $_module;
   private $_action;
   private $_role;
   
   public function __construct(){}
   
   public function preDispatch(Zend_Controller_Request_Abstract $request){
   		$this->_module		= $this->getRequest()->getModuleName();
		$this->_controller 	= $this->getRequest()->getControllerName();
        $this->_action 		= $this->getRequest()->getActionName();

        $auth = Zend_Auth::getInstance();
        
        $lCdPessoa = Zend_Auth::getInstance()->getStorage()->read()->cdpessoa;
        if($lCdPessoa != NULL && Zend_Auth::getInstance()->getStorage()->read()->cdacesso == 2){
            $lBdUsuario = new Usuarios_Model_UsuarioMapper();
            $lUsuario = new Usuarios_Model_Usuario();
            $lUsuario -> setCdpessoa($lCdPessoa);
            $lUsuario = $lBdUsuario -> buscar($lUsuario);
            
            if($lUsuario -> getSessionid() != session_id()){
                $request->setModuleName('login');
        		$request->setControllerName('auth');
        		$request->setActionName('logout');
            }
        }
        
        $redirect=true;
        if (!in_array($this->_module.'/'.$this->_controller.'/'.$this->_action, $this->ACTIONS_NO_AUTH)
        		&& !in_array($this->_module, $this->MODULE_NO_AUTH)
        	){
            if ($this->_isAuth($auth)) {
            	if (!in_array($this->_module.'/'.$this->_controller.'/'.$this->_action, $this->ACTIONS_NO_PERMISSION)) {
            		
	                $user= $auth->getStorage()->read();
	                
		        	$this->_role= $user->cdperfil;
		        	$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		        	$db= $bootstrap->getResource('db');
	
					$manager = $bootstrap->getResource('cachemanager');
					$cache = $manager->getCache('acl');

					
					if (($acl= $cache->load('ACL_'.$this->_role))===false) {
						$acl= new Login_Model_Acl($db,$this->_role);
			            $cache->save($acl,'ACL_'.$this->_role);
					}
					
	            	if($this->_controller == 'admin'){
	            		$lRedirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
			        	$lRedirector -> gotoSimpleAndExit('index', 'index', 'default');
			        }
					
			        if ($this->_isAllowed($auth,$acl)) {
			            $redirect=false;
			        }else{
			        	$request->setModuleName('default');
			        	$request->setControllerName('error');
			        	$request->setActionName('negado');
			        }
            	}	        
        	}else{
        		$request->setModuleName('login');
        		$request->setControllerName('auth');
        		$request->setActionName('login');
        	}
        } else {
            $redirect=false;
        }
    }

    /**
     * Check user identity using Zend_Auth
     * 
     * @param Zend_Auth $auth
     * @return boolean
     */
    private function _isAuth (Zend_Auth $auth)
    {
    	if (!empty($auth) && ($auth instanceof Zend_Auth)) {
        	return $auth->hasIdentity();
    	} 
    	return false;	
    }
    
    /**
     * Check permission using Zend_Auth and Zend_Acl
     * 
     * @param Zend_Auth $auth
     * @param Zend_Acl $acl
     * @return boolean
     */
    private function _isAllowed(Zend_Auth $auth, Zend_Acl $acl) {
    	if (empty($auth) || empty($acl) ||
    		!($auth instanceof Zend_Auth) ||
    		 !($acl instanceof Zend_Acl)) {
    			return false;
    	}
    	$resources= '/'.$this->_module.'/'.$this->_controller.'/'.$this->_action;  
    	$resources = str_replace("-","", $resources);
    	
    	$result = false;

    	if ($acl->has($resources)) {
    		$result = $acl->isAllowed($this->_role,$resources);
    	}

    	return $result;
    }  
    
    /**
     * Fun��o para verificar se o usu�rio possui permiss�o para acesso conforme permiss�es do sistema
     * 
     * @author Jo�o Pedro Grasselli
     * @param $pResource - string de identifica��o da action/permiss�o
     * @return boolean
     * @throws Zend_Exception 
     */
    public function LiberaAcesso($pResource, $pRole = NULL){
    	try{
    		$lAuth = Zend_Auth::getInstance();

    		//dividimos a string de permiss�o para identificar o module/controller/action
    		if(strpos($pResource, '/') !== FALSE){
    			$lResourceArray = explode('/', $pResource);
    		}elseif(strpos($pResource, '\\') !== FALSE){
    			$lResourceArray = explode('\\', $pResource);
    		}else{
    			throw new Zend_Exception('Favor verificar o formato da permissão solicitada: '.$pResource);
    		}
    		
    		//Se a URL come�ar por / a primeira posi��o do array estar� vazia, e assim deveremos desconsider�-la
    		if(empty($lResourceArray[0])){
    			$lModule = $lResourceArray[1];
    			$lController = $lResourceArray[2];
    			$lAction = $lResourceArray[3];
    		}else{
    			$lModule = $lResourceArray[0];
    			$lController = $lResourceArray[1];
    			$lAction = $lResourceArray[2];
    		}
    		
    		//Verificamos se � necess�ria permiss�o de login
    		if (!in_array($lModule.'/'.$lController.'/'.$lAction, $this->ACTIONS_NO_AUTH)
        		&& !in_array($lModule, $this->MODULE_NO_AUTH)
        		){
        			//Testamos se a pessoa est� logada
	            	if ($this->_isAuth($lAuth)) {
	            		//Testamos se � necess�ria permiss�o para este recurso
		            	if (!in_array($lModule.'/'.$lController.'/'.$lAction, $this->ACTIONS_NO_PERMISSION)){
		            		if($pRole == NULL){
				    			$lRole = Zend_Auth::getInstance()->getIdentity()->cdperfil;	
		            		}else{
		            			$lRole = $pRole;
		            		}
							$lBootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
							
							$lCacheManager = $lBootstrap->getResource('cachemanager');
							$lCache = $lCacheManager->getCache('acl');

							if (($lAcl = $lCache->load('ACL_'.$lRole)) === false) {
								$lDb = $lBootstrap -> getResource('db');
								
								$lAcl= new Login_Model_Acl($lDb, $lRole);
								$lCache->save($lAcl, 'ACL_'.$lRole);
							}
							
							if ($lAcl->has($pResource)) {
								return $lAcl->isAllowed($lRole, $pResource);
					    	}else{
					    		return false;
					    		//throw new Zend_Exception('Recurso/Permiss�o n�o encontrado ou n�o existente: '.$pResource);
					    	}
		            	}
		            	//caso n�o seja necess�ria permiss�o retornamos verdadeiro
		            	return true;
	            	}
	            	//Caso ela n�o esteja logada devemos retornar falso
	            	return false;
	            }
	            //Caso n�o seja necess�ria permiss�o de login retornamos verdadeiro
	            return true;
    	}catch(Zend_Exception $ze){
    		die ($ze);
    	}
    }
}
