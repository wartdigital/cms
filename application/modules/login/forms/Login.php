<?php
	/** 
 	 * Formul�rio de login
 	 * @param $pURL a URL atrav�s da qual a p�gina est� sendo acessada
	 * @author Jo�o Pedro Grasselli 
	 * @version 11/01/2013
	 */
class Login_Form_Login extends Zend_Form{

    /**
     *
     */
    public function init(){

        $this->setAction('')
         ->setMethod('post');

        $user = new Comum_Form_Element_Text('login_user');
        $user -> setDescription('Usuário ou e-mail')
          -> setAttrib('autocomplete', 'off')
          -> setRequired(true)
          -> setAttrib('class', 'wart-textfield__input');

        $password = new Comum_Form_Element_Password('login_password');

        $password -> setDescription('Senha')
              -> setAttrib('autocomplete', 'off')
              -> setRequired(true)
              -> setAttrib('class', 'wart-textfield__input');


        $acesso = self::getCampoAcesso();

        $submit = new Zend_Form_Element_Submit('submit', array(
                                                        'label' => 'Entrar'
                                                        ));
        $submit ->setDecorators(array(
                            array('ViewHelper'),
                            array('Errors'),
                            ))
             ->setAttrib('class', 'mdl-button mdl-js-button mdl-button--raised mdl-button--colored wart-button-login mdl-js-ripple-effect');

        $this->addElements(array(
            $user,
            $password,
            $acesso,
            $submit
        ));
	}
	
	private function getCampoAcesso(){
        $acesso = new Zend_Form_Element_Hidden('login_acesso');
        $acesso->setValue('1')
            ->setDecorators(array(
                array('ViewHelper'),
                array('Errors'),
            ));
      
      return $acesso;
	}
}
