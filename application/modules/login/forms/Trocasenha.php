<?php
	/** 
 	 * Formulário de login
	 * @author João Pedro Grasselli 
	 * @version 15/06/2013
	 */
class Login_Form_Trocasenha extends Zend_Form{

	public function init(){
		$this->setAction('')
			 ->setMethod('post');
			 
		$senha = new Comum_Form_Element_Password('nmsenha');
        $senha->setDescription('title', 'Senha')
			  ->setLabel('Senha atual')
			  ->setRequired(true);
        
        $novasenha = new Comum_Form_Element_Password('novasenha');
        $novasenha->setDescription('title', 'Nova senha')
				  ->setLabel('Nova senha')
        		  ->setRequired(true);
        		  
        $novasenhaconfirm = new Comum_Form_Element_Password('novasenhaconfirm');
        $novasenhaconfirm->setDescription('title', 'Confirmar nova senha')
						 ->setLabel('Confirmar nova senha')
        				 ->setRequired(true);
        				 
        $submit = new Comum_Form_Element_Gravar('Trocar');
        
        $this->addElements(array(
					            $senha, 
            					$novasenha, 
            					$novasenhaconfirm,
            					$submit
        						));

	}
}