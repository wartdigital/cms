<?php
	/** 
 	 * Formulário de login
	 * @author João Pedro Grasselli 
	 * @version 11/01/2013
	 */
class Login_Form_Esquecisenha extends Zend_Form{

	public function init(){
		$this->setAction('')
			 ->setMethod('post');
			 
		$user = new Comum_Form_Element_Text('nmapelido');
		$user->setDescription('Usuário')
			 ->setAttrib('autocomplete', 'off')
			 ->setRequired(true)
			 ->setAttrib('class', 'campopadrao');
		 $this -> addElement($user);
		 	
		 $submit = new Comum_Form_Element_Gravar('Enviar');
		 $this -> addElement($submit);

	}
}