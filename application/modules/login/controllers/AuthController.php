<?php 
class Login_AuthController extends Zend_Controller_Action
{
	private $_auth;
	
	/**
	 * @var Zend_Acl
	 */
	private $_acl;
	
	/**
	 * @var string
	 */
	private $_message;

    public function init()
    {
        /* Initialize action controller here */
    }
    
	/** Action de logout
	 * @author João Pedro Grasselli
	 * @param 
	 * @return mixed
	 */
    public function logoutAction()
    {
      $this->getHelper('layout')->disablelayout();
      $this->_helper->viewRenderer->setNoRender(true);	
      
      $lCdAcesso = Zend_Auth::getInstance()->getStorage()->read()->cdacesso;
      
      Zend_Auth::getInstance()->clearIdentity();
      Zend_Session::destroy();
      Zend_Registry::_unsetInstance();
      
      if($lCdAcesso == '1'){
         $this->_redirect('/adminprofile');
      }elseif($lCdAcesso == '3'){
         $this->_redirect('/demo');
      }else{
         $this->_redirect('/login/auth/login');
      }

    }
    
	/** Action de login
	 * @author João Pedro Grasselli
	 * @param 
	 * @return mixed
	 */
    public function loginAction(){
	   	$this->getHelper('layout')->setLayout('login');
	   	$this->_auth = Zend_Auth::getInstance();
	   	
	    if($this->getRequest()->isPost()){		
			$data = $this->_request->getPost();

			if (!$this->_auth->hasIdentity() 
					&& null !== $this->getrequest()->getPost('login_user') 
							&& null !== $this->getrequest()->getPost('login_password')
				){

				$lFilter 	= new Zend_Filter_StripTags();
				$username 	= $lFilter->filter($this->getrequest()->getPost('login_user'));
				$password 	= $lFilter->filter($this->getrequest()->getPost('login_password'));
				$acesso 	= $lFilter->filter($this->getrequest()->getPost('login_acesso'));

				if (empty($username)){
					$message = 'Insira o nome do usuário';
				}elseif (empty($password)){
					$message = 'Insira a senha';
				}else{
					try{
						$authAdapter = new Login_Plugin_AuthAdapter();
						$authAdapter->setIdentity($username);
						$authAdapter->setCredential($password);

						$ozf_AuthResult = $this->_auth->authenticate($authAdapter);

						switch ($ozf_AuthResult->getCode()){
							case Zend_Auth_Result::SUCCESS :
								$storage = $this->_auth->getStorage();
								$obj_authResult = $authAdapter->getResultRowObject(null, 'nmsenha');
								
								$perfil = new Login_Model_Auth();
								$perfil = $perfil->buscaroles($obj_authResult->cdusuario, $acesso);

								$obj_authResult->cdperfil = $perfil[cdperfil];
								$obj_authResult->cdacesso = $acesso;
								$obj_authResult->nmacesso = $perfil[nmacesso];

                                //var_dump($obj_authResult);exit;

								if (!$obj_authResult->cdperfil){
									$message = 'Você não possui perfil para este acesso.';
									Zend_Auth::getInstance()->clearIdentity();
									Zend_Session::destroy();
								}else{
								    Zend_Auth::setStorage($storage);
									$storage->write($obj_authResult);
								}
								break;
							case Zend_Auth_Result::FAILURE :
								$message = "Erro desconhecido." . nl2br(print_r($ozf_AuthResult->getMessages(), true));
								break;
							case Zend_Auth_Result::FAILURE_IDENTITY_AMBIGUOUS :
								$message = "Usuário ambíguo. Entre em contato com o Administrador.";
								break;
							case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND :
							case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID :
								$message = "Usúario ou senha está incorreto.";
								break;
							case Zend_Auth_Result::FAILURE_UNCATEGORIZED :
								$message = "Erro sem categoria." . nl2br(print_r($ozf_AuthResult->getMessages(), true));
								break;
							default :
								$message = "Erro desconhecido." . nl2br(print_r($ozf_AuthResult->getMessages(), true));
							break;
						}
					}catch(Zend_Exception $ze){
						$message = $ze -> getMessage();
					}
				}
			
				if (isset($message)){
					$this->_message = $message;
				}else{
					$this->_message = "";
				}
			}
		}

		if ($this->_auth->hasIdentity()){
			$lCdPerfil = Zend_Auth::getInstance()->getStorage()->read()->cdperfil;
			//var_dump(Zend_Auth::getInstance()->getStorage()->read()); exit;
			$lBdMenu = new Modulos_Model_Menu();
			$lMenu = $lBdMenu->montaMenu($lCdPerfil);

			$lSessionNamespace = new Zend_Session_Namespace('menu');
			$lSessionNamespace -> menus = $lMenu;
		  
			$lHelperLog = $this->_helper->getHelper('Geralog');
			$lHelperLog -> Geralog($this->_request->getModuleName(), 
		  						   $this->_request->getControllerName(), 
		  						   $this->_request->getActionName(), 
		  						   $acesso);
			  
			return $this->_redirect('/default/index/index');
		}
      
		$message = NULL;
		$lForm = new Login_Form_Login();
      
		$this->view->form = $lForm;
		$this->view->retorno = $this->_message;  
	}
    
  
	/** Action de troca de senha
	 * @author Jo�o Pedro Grasselli 
	 * @param 
	 * @return mixed
	 */
    public function trocasenhaAction(){
    	$this->getHelper('layout')->setLayout('clear');
    	
    	$lForm = new Login_Form_Trocasenha();
      	$this -> view -> pForm = $lForm;
    	
        if($this->getRequest()->isPost()){
    		$lPost = $this->_request->getPost();
			
    		if($lPost["novasenha"] == $lPost["novasenhaconfirm"]){
	    		$lHelperSenha 	= $this->_helper->getHelper('Criptografa');
				
				$lBdUsuario = new Usuarios_Model_UsuarioMapper();
			
				$lUsuario = new Usuarios_Model_Usuario();
				$lUsuario -> setCdpessoa(Zend_Auth::getInstance()->getIdentity()->cdpessoa);
				$lUsuario = $lBdUsuario -> buscar($lUsuario);
				
				$lSenhaAtual = $lHelperSenha -> criptografa($lPost["nmsenha"]);
				
				if($lSenhaAtual == $lUsuario -> getNmsenha()){
					$lSenha 		= $lHelperSenha -> criptografa($lPost["novasenha"]);
					$lUsuario 		-> setNmsenha($lSenha);
					
					$lBdUsuario		-> alterar($lUsuario);
				}else{
					$this->view->retorno = "Senha atual não confere!";
				}
	    	}else{
	            $this->view->retorno = "Erro na validação da nova senha!";
	    	}
       } 
    }
	
    /** Action de gera��o de nova de senha
	 * @author Jo�o Pedro Grasselli 
	 * @param 
	 * @return mixed
	 */
    public function esquecisenhaAction(){
    	$this->getHelper('layout')->setLayout('clear');
    	
		$lForm = new Login_Form_Esquecisenha();
		$this -> view -> pForm = $lForm;
    	
		$this->view->retorno= "";
		
        if($this->getRequest()->isPost()){
    		$lPost = $this->_request->getPost();
			
			$lBdUsuario = new Usuarios_Model_UsuarioMapper();
			
			$lUsuario = new Usuarios_Model_Usuario();
			$lUsuario -> setNmemail($lPost["nmapelido"]);
			
			$lUsuario = $lBdUsuario -> getByMail($lUsuario);
		
			if ($lUsuario != NULL){
				$lBdParam = new Parametros_Model_Parametros();

				$lHelperRandom	= $this->_helper->getHelper('GenerateRandom');
				$lHelperSenha 	= $this->_helper->getHelper('Criptografa');
				$lMail 			= $this->_helper->getHelper('EnviaEmail');

				$lNovaSenha		= $lHelperRandom -> generateCustom(8,false,true,false);
				$lSenha 		= $lHelperSenha -> criptografa($lNovaSenha);

				$lUsuario 		-> setNmsenha($lSenha);
				$lBdUsuario		-> alterar($lUsuario);
				
				$lCabecalho 	= $lBdParam->getparametro('cabecalho_esquecisenha');
				$lRodape 		= $lBdParam->getparametro('rodape_esquecisenha');
				$lRemetente 	= $lBdParam->getparametro('REMETENTE_PADRAO');
				
				$lConteudoEmail = $lCabecalho
								 .'<br><br> Usuário: '.$lUsuario -> getNmapelido()
								 .'<br> Senha : '.$lNovaSenha.'<br><br>'
								 .$lRodape;
				try{
					$lMail->settexto($lConteudoEmail);
					$lMail->setrem($lRemetente, $lRemetente);
					$lMail->setdest($lUsuario -> getNmemail());
					$lMail->setassunto('Lembrete de senha');
					$lMail->enviar();
					
					$this -> view -> retorno 	= "Uma nova senha foi enviada para o email cadastrado!";  
					$this -> view -> positivo 	= true;
				}catch(Zend_Exception $pError){
					var_dump($pError);
					$this->view->retorno = "Erro no envio do e-mail!";
					$this -> view -> positivo 	= false;
				}
				
			}else{
				$this->view->retorno = "Login não cadastrado!";
				$this -> view -> positivo 	= false;
			}
    	}
 	}
 	
	public function novasenhaAction(){
		$this->getHelper('layout')->setLayout('load_layout');
		$form = new Zend_Form();
      	$form->setMethod('post');
      	$texto = new Zend_Form_Element_Textarea('texto', array(
        								    'label' => 'Digite uma mensagem para ser adicionada ao email', 
            								'required' => false
        									));
        $texto->setAttrib('rows','5');
		$texto->addDecorators(array(
							   array('ViewHelper'),
							   array('Errors'),
							   array('Description', array('tag' => 'br')),
							   array('HtmlTag', array('tag' => 'br')),
							   array('Label', array('tag' => 'br')),
								));
        $texto->class="campomenu";
        $submit = new Zend_Form_Element_Submit('submit', array(
        									    'label' => 'Enviar nova senha'
           									   ));
	    $submit->setDecorators(array(
							   array('ViewHelper'),
							   array('Errors'),
							   array('HtmlTag', array('tag' => 'br')),
							   array('Description'),
							   array('HtmlTag'),
								));
        $submit->class="botaopadrao";
        $submit->setAttrib("style","width:70%");
        $form->addElement($texto);
        $form->addElement($submit);
    	$this->view->form= $form;
    	
	   	if($this->getRequest()->isPost()){
    		$data = $this->_request->getPost();
    		if($form->isValid($data)){
				$cdpessoa = $this->getRequest()->cdpessoa;
    			$helper = $this->_helper->getHelper('GenerateRandom');
		       	$novasenha = $helper->generateCustom(8,false,true,false);
				$helpersenha = $this->_helper->getHelper('Criptografa');
				$senha = $helpersenha->criptografa($novasenha);
			   	$pessoa = new Pessoa_Model_Pessoa();
		       	$user = $pessoa->buscar($cdpessoa);
    		   	$pessoa-> editar(array(nmsenha=>($senha)),$cdpessoa);
	       		$param = new Parametros_Model_Parametros();
				$cabecalho = $param->getparametro('cabecalho_esquecisenha');
	    		$rodape = $param->getparametro('rodape_esquecisenha');
		        $email = $this->_helper->getHelper('EnviaEmail');
				$email->settexto($cabecalho.'<br><br> Usuário: '.$user[nmapelido].'<br> Senha : '.$novasenha.'<br><br>'.$data[texto].'<br><br>'.$rodape);
				$email->setrem($param->getparametro('remetente_esquecisenha'),'Aeroclube');
				$email->setdest($user[nmemail]);
				$email->setassunto('Nova Senha');
				$email->enviar();
				echo "<script>alert('Nova Senha enviada com sucesso');window.close();</script>";
								
	    	}
	   	}
	}
}
