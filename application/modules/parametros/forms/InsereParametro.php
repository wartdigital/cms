<?php

class Parametros_Form_InsereParametro extends Zend_Form{

	public  function init(){
		
		$this->setMethod('post');

		//Nome do Par�metro
		$nomeparam = new Zend_Form_Element_Text('nmparametro');
		$nomeparam->addValidator(new Zend_Validate_StringLength(1,100))
	             ->setRequired(true)
	             ->setAttrib('alt', 'Nome do Par�metro')
	           	->setAttrib('title', 'Nome do Par�metro')
	           	->setAttrib('style', 'width:90.3%')
	           	->setValue('Nome do Par�metro')
	             ->setDecorators(array(
						   		array('ViewHelper'),
						   		array('Errors'),
						   		array('Label'),
						   		))
      			 ->class='campomenu';
      	$this->addElement($nomeparam);

      	//Valor do Par�metro
		$valorparam = new Zend_Form_Element_Textarea('vlparametro');
		$valorparam	->setRequired(true)
	             	->setAttrib('alt', 'Valor do Par�metro')
	             	->setAttrib('title', 'Valor do Par�metro')
	           		->setValue('Valor do Par�metro')
	             	->setDecorators(array(
							   		array('ViewHelper'),
							   		array('Errors'),
							   		array('Label'),
							   		))
      				->class='campomenu';
      	$this->addElement($valorparam);
      	
	 	//Fgativo
	 	$ativo = new Zend_Form_Element_FgAtivo();
	 	$this->addElement($ativo);
		
		//Hidden com c�digo da Pergunta
	 	$cod = new Zend_Form_Element_Hidden('cdparametro');
		$cod->setDecorators(array(
						   	array('ViewHelper'),
						   	array('Errors'),
						   	array('HtmlTag'),
						   	));
	 	$this->addElement($cod);
		//Botao gravar
		$submit = new Zend_Form_Element_Submit('Gravar');
	 	$submit->class='botaopadrao';
		$submit->setDecorators(array(
						   	array('ViewHelper'),
						   	array('Errors'),
						   	array('HtmlTag', array('tag' => '&nbsp')),
						   	));
		$this->addElement($submit);
		//Botao Voltar
		$voltar = new Zend_Form_Element_Button('Voltar');
	 	$voltar->class='botaopadrao';
		$voltar->setAttrib('OnClick',"window.location.href='/parametros/parametros'");
		$voltar->setDecorators(array(
						   	array('ViewHelper'),
						   	array('Errors'),
						   	array('HtmlTag', array('tag' => '&nbsp')),
						   	));
		$this->addElement($voltar);
		
       
	}
}
	