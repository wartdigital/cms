<?php

class Parametros_Form_InsereConteudo extends Zend_Form{

	public  function init(){
		//Nome do Par�metro
		$nomecont = new Comum_Form_Element_Text('nmconteudo');
		$nomecont ->setRequired(true)
	           	  ->setDescription('Nome do Par�metro');
      	$this->addElement($nomecont);

      	//Valor do Par�metro
		$dsconteudo = new Comum_Form_Element_Textarea('dsconteudo');
		$dsconteudo	->setRequired(true)
	           		->setDescription('Valor do Par�metro');
      	$this->addElement($dsconteudo);
      	
	 	//Fgativo
	 	$ativo = new Zend_Form_Element_FgAtivo();
	 	$this->addElement($ativo);
		
		//Botao gravar
		$submit = new Comum_Form_Element_Gravar('Gravar');
		$this->addElement($submit);
		
		//Botao Voltar
		$voltar = new Comum_Form_Element_Button('Voltar');
		$voltar->setAttrib('OnClick',"window.location.href='/parametros/conteudos/index'");
		$this->addElement($voltar);
	}
	
	public function popFromModel(Parametros_Model_Conteudo $pConteudo){
	   if($pConteudo != NULL){
         $this -> nmconteudo -> setValue($pConteudo -> getNmconteudo() );
         $this -> dsconteudo -> setValue($pConteudo -> getDsconteudo() );
	   }
	}
}
	
