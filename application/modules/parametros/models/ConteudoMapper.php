<?php

class Parametros_Model_ConteudoMapper extends Comum_Estruturadados_MapperComum{

	public function __construct(){
		$this->dbTable = new Parametros_Model_DbTable_Conteudo();
	}
	
	/** Busca todos os conte�dos
	 * @author Jo�o Pedro Grasselli
	 * @param
	 * @return Parametros_Model_ListaConteudo
	 * @version 04/05/2014
	 */
	public function listar(){
		try{
			return $this->dbTable -> listar();
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}

    /** Busca um conte�dos
	 * @author Jo�o Pedro Grasselli
	 * @param Parametros_Model_Conteudo $pConteudo
	 * @return Parametros_Model_Conteudo
	 * @version 04/05/2014
	 */
	public function buscar(Parametros_Model_Conteudo $pConteudo){
		try{
			return $this->dbTable -> buscar($pConteudo);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
	/** Busca todos os conte�dos
	 * @author Jo�o Pedro Grasselli
	 * @param Parametros_Model_Conteudo $pConteudo
	 * @return  void
	 * @version 04/05/2014
	 */
	public function alterar(Parametros_Model_Conteudo $pConteudo){
		try{
			return $this->dbTable -> alterar($pConteudo);
		}catch(Zend_Db_Exception $e){
			die($e);
		}
	}
	
}
