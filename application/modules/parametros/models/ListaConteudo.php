<?php
/**
 * Classe de lista para Conte�dos
 *
 * @author Jo�o Pedro Grasselli
 */
class Parametros_Model_ListaConteudo extends Comum_Estruturadados_ListaComum{

	/**
	 * M�todo respons�vel pela adi��o de um elemento a lista
	 *
	 * @author Jo�o Pedro Grasselli
	 * @param Parametros_Model_Conteudo $pConteudo
	 */
	public function add(Parametros_Model_Conteudo $pConteudo) {
		$this->lLista[$this->getTam()] = $pConteudo;
	}

}

?>
