<?php

class Parametros_Model_DbTable_Conteudo extends Comum_Db_Table {

	protected $_name = 'itr_conteudosite';
	protected $_primary = 'cdconteudo';
		
	/** Lista todos os conte�dos
	  * @author Jo�o Pedro Grasselli
	  * @param void
	  * @return Parametros_Model_ListaConteudo
	  * @version 04/05/2014
	  */
	public function listar(){
		try{
             $select = $this->select()->where('fgativo <> 9')
 									  ->order('nmconteudo ASC');
             $lRowset = $this->fetchAll($select);

             $lListaConteudo = new Parametros_Model_ListaConteudo();
             
             foreach($lRowset as $lKey => $lRow){
                 $lConteudo = new Parametros_Model_Conteudo();
                 
                 $lConteudo -> setCdconteudo($lRow['cdconteudo']);
                 $lConteudo -> setNmconteudo($lRow['nmconteudo']);
                 $lConteudo -> setNmsigla($lRow['nmsigla']);
                 $lConteudo -> setDsconteudo($lRow['dsconteudo']);
                 $lConteudo -> setFgativo($lRow['fgativo']);
                 
                 $lListaConteudo -> add($lConteudo);
             }

             return $lListaConteudo;
		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados!".$e->getMessage();	
		}
	}

	/** Busca um conte�do
	 * @author Jo�o Pedro Grasselli
	 * @param Parametros_Model_Conteudo $pConteudo
	 * @return Parametros_Model_Conteudo
	 * @version 04/05/2014
	 */
	public function buscar(Parametros_Model_Conteudo $pConteudo){
		try {
            $lResult = $this->find($pConteudo -> getCdconteudo());

 			if($lResult->count() > 0){
 				$lRow =  $lResult -> current() -> toArray() ;
 			}else{
 				throw new Zend_Exception("CONTE�DO INV�LIDO");
 			}
 			
 			$lConteudo = new Parametros_Model_Conteudo();

            $lConteudo -> setCdconteudo($lRow['cdconteudo']);
            $lConteudo -> setNmconteudo($lRow['nmconteudo']);
            $lConteudo -> setNmsigla($lRow['nmsigla']);
            $lConteudo -> setDsconteudo($lRow['dsconteudo']);
            $lConteudo -> setFgativo($lRow['fgativo']);

            return $lConteudo;
		}catch (Zend_Db_Exception $e){
               echo "Erro na consulta ao banco de dados";
		}
	}

	/** Altera um conte�do
     * @author Jo�o Pedro Grasselli
	 * @param Parametros_Model_Conteudo $pConteudo
	 * @return void
	 * @version 04/05/2014
	 */
    public function alterar(Parametros_Model_Conteudo $pConteudo){
		try{
            $lUpdate = array('nmconteudo' => $pConteudo -> getNmconteudo(),
                             'dsconteudo' => $pConteudo -> getDsconteudo());
 			$lWhere = 'cdconteudo = '.$pConteudo->getCdConteudo();
 			$this->update($lUpdate, $lWhere);
		}catch(Zend_Db_Adapter_Exception $e){
               echo $e->getMessage();
		}
   }


}

