<?php
/** Classe que representa um conte�do
 * @author Jo�o Pedro Grasselli
 * @param
 * @return
 * @version 04/05/2014
 */
class Parametros_Model_Conteudo extends Comum_Estruturadados_ModelComum{

	protected $cdconteudo;
	protected $nmconteudo;
	protected $nmsigla;
	protected $dsconteudo;

    public function setCdconteudo($pCdConteudo) {
	   $this->cdconteudo = $pCdConteudo;
	}

	public function getCdconteudo() {
	    return $this->cdconteudo;
	}

	public function setNmconteudo($pNmConteudo) {
	   $this->nmconteudo = $pNmConteudo;
	}

	public function getNmconteudo() {
	    return $this->nmconteudo;
	}

	public function setNmsigla($pNmSigla) {
	   $this->nmsigla = $pNmSigla;
	}

	public function getNmsigla() {
	    return $this->nmsigla;
	}
	
	public function setDsconteudo($pDsConteudo) {
	   $this->dsconteudo = $pDsConteudo;
	}

	public function getDsconteudo() {
	    return $this->dsconteudo;
	}
}
?>
