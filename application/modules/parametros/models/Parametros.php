<?php

class Parametros_Model_Parametros extends Zend_Db_Table {

	protected $_name = 'itr_parametros';
	protected $_primary = 'cdparametro';
		
	/** Lista os tipos de Parametros
	  * @author Jo�o P. Grasselli 
	  * @param 
	  * @return 
	  * @version 13/09/2011
	  */
	public function listar(){
		try {
			$where = $this->getAdapter()->quoteInto("fgvisivel = 1 AND fgativo <> ?", 9);	
			 return $this->fetchAll($where) -> toArray();
		}catch (Zend_Db_Exception $e){
			echo "Erro na consulta ao banco de dados!";	
		}
	}

	/** Busca um Par�metro
	 * @author Jo�o P. Grasselli
	 * @param string $cert
	 * @return array contendo o resultado da busca
	 * @version 13/09/2011
	 */
	public function buscar($param = null){
		try {			
			$select = $this ->getAdapter()->select()
		        		->from(array('l'=> 'itr_parametros'))
		        		->where("cdparametro = ?", $param);
			$stmt = $this->getAdapter()->query($select);
			$result = $stmt->fetch();
			return $result;	
		}catch (Zend_Db_Exception $e){
				echo "Erro na consulta ao banco de dados";
		}
	}
	
	/** Adiciona novo Par�metro
	  * @author Jo�o P. Grasselli
	  * @param string docs nome a ser adicionado 
	  * @return -
	  * @version 13/09/2011
	  */
	public function inserir($pParametro){
		try{
			parent::insert($pParametro);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}
	
	/** Adiciona novo Par�metro
	  * @author Jo�o P. Grasselli
	  * @param $pParametro
	  * @param $pCdParametro
	  * @return -
	  * @version 13/09/2011
	  */
	public function editar($pParametro, $pCdParametro){
		try{
			$lWhere = $this->getAdapter()->quoteinto("cdparametro = ?", $pCdParametro);
			parent::update($pParametro, $lWhere);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}
	
	/** 
	 * Exclui um Par�metro
	 * 
	 * @author Jo�o P. Grasselli
	 * @param $pCdParametro
	 * @return 
	 * @version 15/09/2011
	 */
	public function excluir($pCdParametro){
		try{
			$lWhere = $this->getAdapter()->quoteInto("cdparametro = ?", $pCdParametro);
			$lUpdate = array ("fgativo" => '9');
			parent::update($lUpdate, $lWhere);
		}catch(Zend_Db_Adapter_Exception $e){
			echo $e->getMessage();
		}
	}

	/** 
	 * Busca um par�metro pelo nome
	 * 
	 * @author Jo�o P. Grasselli
	 * @param string $pParamName nome do par�metro
	 * @return 
	 * @version 15/09/2011
	 */
	public function getparametro($pParamName){
		try {
			//colocando nome para 'caixa alta'
			$pParamName = strtoupper($pParamName);
			
			$select = $this->getAdapter()->select()
						   ->from(array('a'=>'itr_parametros'))
						   ->where("fgativo = '1' AND nmparametro = '".$pParamName."'");
			$stmt = $this->getAdapter()->query($select);
			$lResult = $stmt->fetch();
			
			return ($lResult['vlparametro']);
		}catch (Zend_Db_Exception $e){
				echo "Erro na consulta ao banco de dados";
		}
	}

}

