<?php

class Parametros_ParametrosController extends Zend_Controller_Action
{
	
	public function init(){}
	
	/** Action da listagem dos par�metros
	 * @author Jo�o P. Grasselli
	 * @param
	 * @return mixed
	 */
	public function indexAction(){
		$lBdParametro = new Parametros_Model_Parametros();
		$lListaParametros = $lBdParametro->listar();
		$this->view->pListaParametro = $lListaParametros;
	}
	
	/** Action da inclus�o de par�metros
	 * @author Jo�o P. Grasselli
	 * @param
	 * @return mixed
	 */
	public function addAction(){
		$this->getHelper('viewRenderer')->setRender('cadastro');
		
		$lForm = new Parametros_Form_InsereParametro();
		$this->view->pForm = $lForm;
		
		$this->view->cabecalho = "Inclus�o";
	
		if($this->getRequest()->isPost()){
			$lPost = $this -> getRequest() -> getPost();
			
			$lBdParametro = new Parametros_Model_Parametros();
			$lBdParametro->inserir($lPost);
			
			return $this->_redirect('/parametros/parametros/index');
		}
	}
	
	/** Action da edi��o de par�metros
	 * @author Jo�o P. Grasselli
	 * @param
	 * @return mixed
	 */	
	public function editAction(){
		$this->getHelper('viewRenderer')->setRender('cadastro');

		$lBdParametro = new Parametros_Model_Parametros();

		$lCdParametro = $this -> getRequest() -> cdparametro;
		$lParametro = $lBdParametro -> buscar($lCdParametro);
	
		$lForm = new Parametros_Form_InsereParametro();
		$lForm->populate($lParametro);
		$this->view->pForm = $lForm;

		$this->view->cabecalho = "Altera��o";
	
		if($this->getRequest()->isPost()){
			$lPost = $this -> getRequest() -> getPost();
			
			$lParametro['fgativo']		= $lPost['fgativo'];
			$lParametro['nmparametro']	= $lPost['nmparametro'];
			$lParametro['vlparametro']	= $lPost['vlparametro'];
			
			$lBdParametro -> editar($lParametro, $lCdParametro);
			
			return $this->_redirect('/parametros/parametros/index');
		}
	}
	
	/** Action da exclus�o de par�metros
	 * @author Jo�o P. Grasselli
	 * @param
	 * @return
	 */	
	public function deleteAction(){
		$lBdParametro = new Parametros_Model_Parametros();
		$lCdParametro = $this -> getRequest() -> cdparametro;
		
		if($lCdParametro != NULL){
			$lBdParametro -> excluir($lCdParametro);
		}
		return $this->_redirect('/parametros/parametros/index');
	}
	
}
