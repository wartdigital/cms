<?php

class Parametros_ConteudosController extends Zend_Controller_Action
{
	
	public function init(){
		/* Initialize action controller here */
	}
	/** Action da listagem dos Conte�dos do site
	 */
	public function indexAction(){
        $lBdConteudo = new Parametros_Model_ConteudoMapper();
		$lListaConteudos = $lBdConteudo->listar();
		$this->view->pListaConteudos = $lListaConteudos;
	}
	
	/** Action da altera��o de Conte�dos do site
	 */
	public function editAction(){
		$this->getHelper('viewRenderer')->setRender('cadastro');
		
		$lCdConteudo = $this -> getRequest() -> cdconteudo;
        $lConteudo = new Parametros_Model_Conteudo();
        $lConteudo -> setCdconteudo($lCdConteudo);
        
        $lBdConteudo = new Parametros_Model_ConteudoMapper();
		$lConteudo = $lBdConteudo -> buscar($lConteudo);
		
		$lForm = new Parametros_Form_InsereConteudo();
		$lForm->popFromModel($lConteudo);
		$this->view->pForm = $lForm;
		
		$this->view->cabecalho = "Altera��o";
	
		if($this->getRequest()->isPost()){
			$lPost = $this->getRequest()->getPost();
			
            $lConteudo -> setNmconteudo($lPost['nmconteudo']);
            $lConteudo -> setDsconteudo($lPost['dsconteudo']);
            
			$lBdConteudo->alterar($lConteudo);

			return $this->_redirect('/parametros/conteudos/index');
		}
	}
	
	/** Action da exclus�o de Conte�dos do site
	 */
	public function deleteAction(){
		$banco = new Parametros_Model_ConteudoFixo();
		$cod = $this->getRequest();
		if($cod->cd!=""){
			$banco->excluir($cod->cd);
			//$helper = $this->_helper->getHelper('Geralog');
			//$helper -> Geralog($this->_request->getControllerName(), $this->_request->getActionName(), $cod->cd);
			return $this->_redirect('/parametros/conteudo-fixo/index');
		}
	}
	
	}
