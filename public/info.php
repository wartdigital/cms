<!Doctype html>
<html>
<head>
    <title>Configurações para o Zend</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <h2>Configurações do Servidor</h2>
        <p>Configurações essenciais para a aplicação em Zend</p>
        <table class="table">
            <thead>
            <tr>
                <th>Configuração</th>
                <th>Valor</th>
            </tr>
            </thead>
            <tbody>
            <tr class="<?php if(ini_get('display_errors')){echo 'warning';} ?>">
                <td>display_errors</td>
                <td><?php if(ini_get('display_errors')){echo 'true';}else{echo 'false';} ?></td>
            </tr>
            <tr>
                <td>post_max_size</td>
                <td><?php echo ini_get('post_max_size'); ?></td>
            </tr>
            <tr>
                <td>memory_limit</td>
                <td><?php echo ini_get('memory_limit'); ?></td>
            </tr>
            <tr class="<?php if(ini_get('short_open_tag')){echo 'success';}else{echo 'danger';} ?>">
                <td>short_open_tag</td>
                <td><?php if(ini_get('short_open_tag')){echo 'true';}else{echo 'false';} ?></td>
            </tr>
            <tr>
                <td>PDO</td>
                <td>
                    <?php foreach(PDO::getAvailableDrivers() as $driver)
                    {
                        echo $driver.'<br />';
                    } ?>
                </td>
            </tr>
            <tr class="<?php if (in_array("mod_rewrite",apache_get_modules())) {echo 'success';}else{echo 'danger';} ?>">
                <td>mod_rewrite</td>
                <td>
                    <?php
                    if (in_array("mod_rewrite",apache_get_modules())) {
                        echo "true";
                    } else {
                        echo 'false';
                    }
                    ?>
                </td>
            </tr>
            <tr class="<?php if(phpversion() < 5.6){echo 'danger';}else{echo 'success';} ?>">
                <td>PHP</td>
                <td>
                    <?php
                   echo phpversion();
                    ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
